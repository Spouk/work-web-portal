package main

import (
	"gitlab.com/Spouk/work-web-portal/library"
)

const (
	info = "usage: %s\n"
)

func main() {

	////production
	//var (
	//	configPath  string
	//	debugBackup bool
	//)
	//flag.StringVar(&configPath, "config", "", "файл конфигурации с полным путем к нему")
	//flag.BoolVar(&debugBackup, "debugbackup", false, "отладка бэкапирования `debugbackup=false/true`")
	//
	//flag.Parse()
	//flag.Usage = func() {
	//	fmt.Printf(info, os.Args[0])
	//	flag.PrintDefaults()
	//}
	//switch len(os.Args) {
	//case 1:
	//	flag.Usage()
	//	os.Exit(1)
	//case 3: //config
	//	fmt.Printf("Config only, simple execute\n")
	//case 4: //config+debug
	//	if debugBackup {
	//		//fmt.Printf("Good params `%v`\n-----------------\n%3s%s: %3d\n", debugBackup,"","* result", 1203)
	//		s := library.NewServer(configPath)
	//		s.BackupPlugin.DebuggerFindFiles()
	//		os.Exit(1)
	//	}
	//	if debugBackup == false {
	//		flag.Usage()
	//		os.Exit(-1)
	//	}
	//
	//default:
	//	fmt.Printf("Wrong count params, exit application %d: %v\n", len(os.Args), debugBackup)
	//	flag.Usage()
	//	os.Exit(-1)
	//}
	//
	//fmt.Printf("Ok")
	//
	//s := library.NewServer(configPath)

	//developing
	cfg := "/home/spouk/stock/s.develop/go/src/gitlab.com/Spouk/work-web-portal/config.yaml"
	s := library.NewServer(cfg)

	//root
	s.Mux.Get("/", s.HandlerRoot)

	//ajax
	s.Mux.Post("/ajax/{command}", s.HandlerAjax)
	s.Mux.Post("/ajax/{command}/{id}", s.HandlerAjax)
	//websocket :: обработка проверки серверов
	s.Mux.Get("/admin/tester/ws", s.WebSocket2)
	s.Mux.Post("/admin/tester/ws", s.WebSocket2)

	//config
	s.Mux.Get("/config", s.HandlerConfig)
	s.Mux.Post("/config", s.HandlerConfig)
	s.Mux.Get("/config/{command}", s.HandlerConfig)
	s.Mux.Post("/config/{command}", s.HandlerConfig)
	s.Mux.Get("/config/{command}/{id}", s.HandlerConfig)
	s.Mux.Post("/config/{command}/{id}", s.HandlerConfig)

	//single check
	s.Mux.Get("/checkdbsingle", s.HandlerCheck)
	s.Mux.Post("/checkdbsingle", s.HandlerCheck)
	s.Mux.Get("/checkdbsingle/{command}", s.HandlerCheck)
	s.Mux.Post("/checkdbsingle/{command}", s.HandlerCheck)
	s.Mux.Get("/checkdbsingle/{command}/{id}", s.HandlerCheck)
	s.Mux.Post("/checkdbsingle/{command}/{id}", s.HandlerCheck)

	//timer check
	s.Mux.Get("/checkdbtimer", s.HandlerCheckTimer)
	s.Mux.Post("/checkdbtimer", s.HandlerCheckTimer)
	s.Mux.Get("/checkdbtimer/{command}", s.HandlerCheckTimer)
	s.Mux.Post("/checkdbtimer/{command}", s.HandlerCheckTimer)
	s.Mux.Get("/checkdbtimer/{command}/{id}", s.HandlerCheckTimer)
	s.Mux.Post("/checkdbtimer/{command}/{id}", s.HandlerCheckTimer)

	//wrapper python
	s.Mux.Get("/wrappython", s.Handlerwrappython)
	s.Mux.Post("/wrappython", s.Handlerwrappython)
	s.Mux.Get("/wrappython/{command}", s.Handlerwrappython)
	s.Mux.Post("/wrappython/{command}", s.Handlerwrappython)
	s.Mux.Get("/wrappython/{command}/{id}", s.Handlerwrappython)
	s.Mux.Post("/wrappython/{command}/{id}", s.Handlerwrappython)

	//wrapper dbs
	s.Mux.Get("/wrapdbs", s.Handlerwrapdbs)
	s.Mux.Post("/wrapdbs", s.Handlerwrapdbs)
	s.Mux.Get("/wrapdbs/{command}", s.Handlerwrapdbs)
	s.Mux.Post("/wrapdbs/{command}", s.Handlerwrapdbs)
	s.Mux.Get("/wrapdbs/{command}/{id}", s.Handlerwrapdbs)
	s.Mux.Post("/wrapdbs/{command}/{id}", s.Handlerwrapdbs)

	//spider bd-net (smb/cifs)
	s.Mux.Get("/spidersmb", s.Handlerspidersmb)
	s.Mux.Post("/spidersmb", s.Handlerspidersmb)
	s.Mux.Get("/spidersmb/{command}", s.Handlerspidersmb)
	s.Mux.Post("/spidersmb/{command}", s.Handlerspidersmb)
	s.Mux.Get("/spidersmb/{command}/{id}", s.Handlerspidersmb)
	s.Mux.Post("/spidersmb/{command}/{id}", s.Handlerspidersmb)

	//spider bd-net (ftp)
	s.Mux.Get("/spiderftp", s.Handlerspiderftp)
	s.Mux.Post("/spiderftp", s.Handlerspiderftp)
	s.Mux.Get("/spiderftp/{command}", s.Handlerspiderftp)
	s.Mux.Post("/spiderftp/{command}", s.Handlerspiderftp)
	s.Mux.Get("/spiderftp/{command}/{id}", s.Handlerspiderftp)
	s.Mux.Post("/spiderftp/{command}/{id}", s.Handlerspiderftp)

	//multicheck
	s.Mux.Get("/multicheck", s.HandlerCheck)
	s.Mux.Post("/multicheck", s.HandlerCheck)
	s.Mux.Get("/multicheck/{command}", s.HandlerCheck)
	s.Mux.Post("/multicheck/{command}", s.HandlerCheck)
	s.Mux.Get("/multicheck/{command}/{id}", s.HandlerCheck)
	s.Mux.Post("/multicheck/{command}/{id}", s.HandlerCheck)

	//checkbackupSingle
	s.Mux.Get("/checkbackupSingle", s.HandlerCheck)
	s.Mux.Post("/checkbackupSingle", s.HandlerCheck)
	s.Mux.Get("/checkbackupSingle/{command}", s.HandlerCheck)
	s.Mux.Post("/checkbackupSingle/{command}", s.HandlerCheck)
	s.Mux.Get("/checkbackupSingle/{command}/{id}", s.HandlerCheck)
	s.Mux.Post("/checkbackupSingle/{command}/{id}", s.HandlerCheck)
	//websocket :: проверка бэкапов
	s.Mux.Get("/ws/backupsingle", s.WebsocketBackupSingle)
	s.Mux.Post("/ws/backupsingle", s.WebsocketBackupSingle)

	//checkbackupTimer
	s.Mux.Get("/checkbackupTimer", s.HandlerCheck)
	s.Mux.Post("/checkbackupTimer", s.HandlerCheck)
	s.Mux.Get("/checkbackupTimer/{command}", s.HandlerCheck)
	s.Mux.Post("/checkbackupTimer/{command}", s.HandlerCheck)
	s.Mux.Get("/checkbackupTimer/{command}/{id}", s.HandlerCheck)
	s.Mux.Post("/checkbackupTimer/{command}/{id}", s.HandlerCheck)

	//timer-single-config
	s.Mux.Get("/timersingleconfig", s.HandlerTimerSingleConfig)
	s.Mux.Post("/timersingleconfig", s.HandlerTimerSingleConfig)
	s.Mux.Get("/timersingleconfig/{command}", s.HandlerTimerSingleConfig)
	s.Mux.Post("/timersingleconfig/{command}", s.HandlerTimerSingleConfig)
	s.Mux.Get("/timersingleconfig/{command}/{id}", s.HandlerTimerSingleConfig)
	s.Mux.Post("/timersingleconfig/{command}/{id}", s.HandlerTimerSingleConfig)

	//websocket :: timer-single-config
	s.Mux.Get("/ws/timersingleconfig", s.WebsocketTimerSingleConfig)
	s.Mux.Post("/ws/timersingleconfig", s.WebsocketTimerSingleConfig)

	//timer-single-result
	s.Mux.Get("/timersingleresult", s.HandlerTimerSingleResult)
	s.Mux.Post("/timersingleresult", s.HandlerTimerSingleResult)
	s.Mux.Get("/timersingleresult/{command}", s.HandlerTimerSingleResult)
	s.Mux.Post("/timersingleresult/{command}", s.HandlerTimerSingleResult)
	s.Mux.Get("/timersingleresult/{command}/{id}", s.HandlerTimerSingleResult)
	s.Mux.Post("/timersingleresult/{command}/{id}", s.HandlerTimerSingleResult)

	//websocket :: timer-single-result
	s.Mux.Get("/ws/timersingleresult", s.WebsocketTimerSingleResult)
	s.Mux.Post("/ws/timersingleresult", s.WebsocketTimerSingleResult)

	//websocket :: обработка проверки  серверов - РАЗОВАЯ ПРОВЕРКА ::> checkdbsingle
	s.Mux.Get("/ws/singlecheck", s.HandlerWebsocketSingleCheck)
	s.Mux.Post("/ws/singlecheck", s.HandlerWebsocketSingleCheck)

	//websocket :: обработка проверки серверов :: проверка через форму на сайте
	s.Mux.Get("/ws/singleconfigcheck", s.HandlerWebsocketSingleFormChecker)
	s.Mux.Post("/ws/singleconfigcheck", s.HandlerWebsocketSingleFormChecker)

	//websocket :: постоянные проверки с логированием в базу данных ::
	s.Mux.Get("/ws/multicheck", s.HandlerWebsocketMultiCheck)
	s.Mux.Post("/ws/multicheck", s.HandlerWebsocketMultiCheck)

	//dbs:interactiv
	s.Mux.Get("/dbsinteract", s.HandlerCheck)
	s.Mux.Post("/dbsinteract", s.HandlerCheck)
	s.Mux.Get("/dbsinteract/{command}", s.HandlerCheck)
	s.Mux.Post("/dbsinteract/{command}", s.HandlerCheck)
	s.Mux.Get("/dbsinteract/{command}/{id}", s.HandlerCheck)
	s.Mux.Post("/dbsinteract/{command}/{id}", s.HandlerCheck)

	//----------------------------------------------------------------------
	// remd + imek + users crud + rpn
	//-----------------------------------------------------------------------
	s.Mux.Get("/functions/{command}", s.HandlerCheck)
	s.Mux.Post("/functions/{command}", s.HandlerCheck)
	s.Mux.Get("/functions/{command}/{id}", s.HandlerCheck)
	s.Mux.Post("/functions/{command}/{id}", s.HandlerCheck)

	//websocket :: remd + imek + users crud
	s.Mux.Get("/ws/functions", s.WebsocketFu)
	s.Mux.Post("/ws/functions", s.WebsocketFu)

	//start server
	s.Run()
}

module gitlab.com/Spouk/work-web-portal

go 1.16

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-yaml/yaml v2.1.0+incompatible // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/mozillazg/go-unidecode v0.1.1 // indirect
	github.com/nakagami/firebirdsql v0.9.1
	gitlab.com/Spouk/datastruct v0.0.0-20210810190200-1b2a4892e111
	gitlab.com/Spouk/gotool v0.0.0-20200529055947-6593d2f8460f
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

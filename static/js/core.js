CORE = {};

//AJAX with FETCH
CORE.testerEvents = async function (idblock) {
    var d = document.getElementById(idblock);
    try {
        let response = await fetch('/admin/ajax/testerevent', {
            method: 'POST',
            cache: 'no-cache',
        });
        let ja = await response.json();
        switch (ja.stage) {
            case "start":
                console.log(ja.stage);
                break;
            case "process":
                console.log(ja.stage);
                break;
            case "end":
                console.log(ja.stage);
                break;
            default:
                console.log("не определенное значение блока событийно модели");
                return;
        }
        console.log(ja, ja.t.length);
        for (let i = 0; i < ja.t.length; i++) {
            let newdiv = document.createElement('div');
            d.appendChild(newdiv);
        }
        // let text = await response.text();
        // d.outerHTML = text;
        // console.log(text);
    } catch (e) {
        console.log(e);
    }
};

//задумка - при нажатии кнопки на форме, запускается событийная модель
//1 - создание вебсокета на стороне фронта
//2 - отправка запроса на запуск горутин для работы
//3 - получение ответа от сервера о успешности запуска и блока со списком горутин
//4 - ожидание работы горутин
//5 - получение результата горутин
CORE.sock = function (idblock) {
    block = document.getElementById(idblock);
    let socket = new WebSocket("ws://localhost:4001/admin/ws")

    //установка соединения с сервером и отправка первичной порции данных
    socket.onopen = function (e) {
        console.log('[socket] connection established');
        console.log('[socket] send data to server');
        startStruct = {
            Status: "enable",
            Message: "start"
        };
        socket.send(JSON.stringify(startStruct));
    };

    //получение данных с сервера
    socket.onmessage = function (e) {
        //парсинг блока данных
        let res = JSON.parse(e.data);
        // //поиск в block имеющегося дивана с ID
        // let b =  document.getElementById(e.data.id);
        // if (b === null) {
        //     //элемент не найден, добавляю новый
        //     let newdiv = document.createElement("div");
        //     newdiv.classList.add("box");
        //     newdiv.id = res.ids;
        //     //id
        //     let tagID = document.createElement("span");
        //     tagID.id = 'ids'+res.ids;
        //     tagID.innerText = res.ids;
        //     newdiv.appendChild(tagID);
        //     //status
        //     let tagStatus = document.createElement("span");
        //     tagStatus.id = 'status'+res.ids;
        //     tagStatus.innerText = res.status;
        //     newdiv.appendChild(tagStatus);
        //     //result
        //     // let tagResult = document.createElement("span");
        //     // tagResult.id = 'result'+res.ids;
        //     // tagResult.classList.add('span');
        //     // tagresult.innerText = res.result;
        //     // newdiv.appendChild(tagResult);
        //     block.appendChild(newdiv);
        //
        // } else {
        //     //блок найден, надо обновить по нему данные
        //     console.log(b, res);
        // }

        console.log(res);

        console.log('[socket] получение данных с сервера', e.data, JSON.stringify(e.data).status, e.data.status);
        // console.log(res.status, res.message);

        // console.lof(`${e.data.stringify()}`);
    };

    //соединеие закрыто
    socket.onclose = function (e) {
        if (e.wasClean) {
            console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
        } else {
            console.log('[socket] соединение закрыто со стороны сервера ', e.code);
        }
    };

    //ошибка
    socket.onerror = function (e) {
        console.log(`[socket] websocket error ${e.message}`);
    }
}

//
class WS {
    constructor() {
        this.socket = new WebSocket("ws://localhost:4001/ws");

        //event->open::ws
        this.socket.onopen = function (e) {
            console.log('[socket] connection established');
            console.log('[socket] send data to server');
        };

        //event->read::ws
        this.socket.onmessage = function (e) {

            //парсинг блока данных
            let res = JSON.parse(e.data);
            console.log(res);
        };

        //соединеие закрыто
        this.socket.onclose = function (e) {
            if (e.wasClean) {
                console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
            } else {
                console.log('[socket] соединение закрыто со стороны сервера ', e.code);
            }
        };

        //ошибка
        this.socket.onerror = function (e) {
            console.log(`[socket] websocket error ${e.message}`);
        };
    }
}


function testering(t) {
    console.log(t, t.id, t.innerText);
    switch (t.innerText) {
        case "testing":
            t.innerText = "simple";
            break;
        case 'simple':
            t.innerText = "testing";
            break;
        default:
            t.innerText = "testing";

            break;
    }
}


//класс вебсокеты под опрос серверов firebirds
class Sockets {
    constructor(idblock, idend, msgbox) {
        // this.msgbox = document.getElementById(msgbox);
        this.idblock = document.getElementById(idblock);
        this.idend = document.getElementById(idend);
        this.socket = new WebSocket("ws://localhost:4001/admin/ws");

        //установка соединения с сервером и отправка первичной порции данных
        this.socket.onopen = function (e) {
            console.log('[socket] connection established');
            console.log('[socket] send data to server');
        };
        console.log("socket: ", this.socket);
        //получение данных с сервера
        this.socket.onmessage = function (e) {
            //парсинг блока данных
            let res = JSON.parse(e.data);
            //проверка на статус
            switch (res.from_server) {

                //количество отработавших горутин
                case "count":
                    console.log("COUNT---- ", res);

                case "end":
                    this.msgbox = document.getElementById(msgbox);
                    //окончания работы воркеров, надо вывести сообщение об этом
                    this.msgbox.innerText = "Воркеры закончили работу";

                    //вычищаю боксы
                    this.idblock = document.getElementById(idblock);
                    this.idblock.innerHTML = "";

                    break;
                default:
                    //ветка по умолчанию =- обработка всех входящих пакетов
                    //поиск в block имеющегося дивана с ID
                    let b = document.getElementById(res.ids);
                    console.log("B==========", b, res.ids);
                    if (b === null) {
                        //элемент не найден, добавляю новый
                        let newdiv = document.createElement("div");
                        newdiv.classList.add("box");
                        newdiv.id = res.ids;
                        //id
                        let tagID = document.createElement("span");
                        tagID.id = 'ids' + res.ids;
                        tagID.innerText = res.ids;
                        newdiv.appendChild(tagID);
                        //status
                        let tagStatus = document.createElement("span");
                        tagStatus.id = 'status' + res.ids;
                        tagStatus.innerText = res.status;
                        newdiv.appendChild(tagStatus);
                        //result
                        let tagResult = document.createElement("span");
                        tagResult.id = 'result' + res.ids;
                        tagResult.classList.add('span');
                        tagResult.innerText = res.result;
                        newdiv.appendChild(tagResult);
                        //добавляю бокс к общему боксу
                        this.idblock = document.getElementById(idblock);
                        this.idblock.appendChild(newdiv);
                    } else {
                        //блок найден, надо обновить по нему данные
                        console.log("Блок найден ");
                        let tagResult = document.getElementById('result' + res.ids);
                        console.log("RESULT BOX:  ", tagResult);
                        tagResult.innerText = res.result;
                        console.log(b, res);
                    }
            }

            console.log(res);
            console.log('[socket] получение данных с сервера', e.data, JSON.stringify(e.data).status, e.data.status);
            // console.log(res.status, res.message);

            // console.lof(`${e.data.stringify()}`);
        };
        //соединеие закрыто
        this.socket.onclose = function (e) {
            if (e.wasClean) {
                console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
            } else {
                console.log('[socket] соединение закрыто со стороны сервера ', e.code);
            }
        };

        //ошибка
        this.socket.onerror = function (e) {
            console.log(`[socket] websocket error ${e.message}`);
        };

    }

    //вешать на кнопку отмены
    endEvent() {
        //отсылаю сообщение о закрытии горутин
        let startStruct = {
            Status: "enable",
            Command: "end"
        };
        this.socket.send(JSON.stringify(startStruct));
    }

    //вешать на кнопку старт
    startEvent() {
        let startStruct = {
            Status: "enable",
            Command: "start"
        };
        //очищаю мессадже бокс
        let mbox = document.getElementById(msgbox);
        console.log("MsgBOX: ", mbox, msgbox);
        msgbox.innerHTML = "";
        // if (mbox  != null) {
        //     msgbox.innerHTML === "";
        //     msgbox.innerHTML = "";
        // } else {
        //     console.log('msgbox not found in DOM, strange');
        // };

        this.socket.send(JSON.stringify(startStruct));
        console.log("SENDING COMMAND");

        console.log("socket: ", this.socket);
        console.log("before SENDING COMMAND");
    }
}

class WSTest {
    constructor() {
        this.socket = new WebSocket("ws://localhost:4001/ws");

        //event->open::ws
        this.socket.onopen = function (e) {
            console.log('[socket] connection established');
            console.log('[socket] send data to server');
        };

        //event->read::ws
        this.socket.onmessage = function (e) {

            //парсинг блока данных
            let res = JSON.parse(e.data);
            console.log(res);
        };

        //соединеие закрыто
        this.socket.onclose = function (e) {
            if (e.wasClean) {
                console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
            } else {
                console.log('[socket] соединение закрыто со стороны сервера ', e.code);
            }
        };

        //ошибка
        this.socket.onerror = function (e) {
            console.log(`[socket] websocket error ${e.message}`);
        };
    }
}


/////////////////////////////////////////////////////////
//тестовый класс для проверки websockets
///////////////////////////////////////////////////////////
// class SocketTester {
//     constructor(idblock, list, checked, single, btn, btnStop, serverAddr) {
//         this.block = document.getElementById(idblock);
//         // this.socket = new WebSocket("ws://localhost:4001/ws/singlecheck");
//         this.socket = new WebSocket(serverAddr);
//         // this.socket = new WebSocket("wss://localhost:4001/admin/tester/ws");
//         this.checked = checked
//         this.btn = btn
//         this.single = single
//         this.list = list
//         this.btnstop = btnStop
//
//         //OPEN->WS
//         this.socket.onopen = function (e) {
//             console.log('[socket] connection established');
//             console.log('[socket] send data to server');
//         };
//
//         //READ->WS
//         this.socket.onmessage = function (e) {
//
//             //парсинг блока данных
//             let res = JSON.parse(e.data);
//
//             console.log("------> ", res);
//
//             //выстраиваю логическое дерево
//             switch (res.client.command) {
//                 //установки информационного значения
//                 case "infoset":
//                     let block = document.getElementById(idblock);
//                     block.innerText = res.client.nodes;
//                     console.log("FROM CLIENT: ", res);
//                     break;
//
//                 //количество отработавших горутин
//                 case "count":
//                     console.log("COUNT---- ", res);
//                     //добавляю инфу о количестве проверенных серверов
//                     document.getElementById(checked).innerText = res.client.count_check;
//                     break;
//
//                 //одиночная проверка
//                 case "work":
//                     if (res.worker.result === false) {
//                         //сервер не доступен
//
//                         let tbody = document.getElementById('list');
//                         console.log(tbody);
//                         let tr = document.createElement("tr");
//
//                         //host
//                         let td = document.createElement("td");
//                         td.innerHTML = res.worker.node;
//                         tr.appendChild(td);
//
//                         //error
//                         let td1 = document.createElement("td");
//                         td1.innerHTML = res.worker.error;
//                         tr.appendChild(td1);
//
//                         //time
//                         let td2 = document.createElement("td");
//                         td2.innerHTML = res.worker.time_run;
//                         tr.appendChild(td2);
//
//                         tbody.appendChild(tr);
//
//                         // let tr1 = document.createElement("tr");
//                         // let tr2 = document.createElement("tr");
//                         //
//                         //
//                         // let tr1 = box.insertRow(0);
//                         // let td0 = tr1.insertCell(0);
//                         // let td1 = tr1.insertCell(1);
//                         // let td2 = tr1.insertCell(2);
//                         //
//                         // td0.innerHTML = res.worker.node;
//                         // td1.innerHTML = res.worker.error;
//                         // td2.innerHTML = res.worker.time_run;
//                         //
//                         // tr.appendChild(td1);
//                         // tr.appendChild(td2);
//                         // tr.appendChild(td3);
//                         //
//                         // box.appendChild(tr);
//
//                         //обновляю информацию о количестве  отработанных воркеров
//                         document.getElementById(checked).innerText = res.client.count_check;
//                     }
//                     break;
//
//                 //постоянная проверка, с заданной периодичностью
//                 case "end":
//                     //изменяю статус кнопки и делаю ее доступной
//                     let bb = document.getElementById(btn);
//                     bb.disabled = false;
//                     bb.innerText = "проверить сервера";
//                     //изменяю статус кнопки останов
//                     let b = document.getElementById(btnStop);
//                     b.disabled = true;
//
//                     console.log("проверка закончена ", res);
//
//                     //добавляю инфу о количестве проверенных серверов
//                     document.getElementById(checked).innerText = res.client.count_check;
//
//                     break;
//                 default:
//                     console.log("wrong  res.client.command", res.client.command);
//                     break;
//             }
//         };
//
//         //соединеие закрыто
//         this.socket.onclose = function (e) {
//             if (e.wasClean) {
//                 console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
//             } else {
//                 console.log('[socket] соединение закрыто со стороны сервера ', e.code);
//             }
//         };
//
//         //ошибка
//         this.socket.onerror = function (e) {
//             console.log(`[socket] websocket error ${e.message}`);
//         };
//
//     }
//     //обработка нажатия на обновление списка серверов
//     updateServerList(t) {
//         // получаю объект кнопки
//         let bb  = document.getElementById(t.id);
//         if (bb === null) {
//             console.log("ERROR не найден объект кнопка обновления");
//             return null;
//         }
//         //изменяю статус изменением цвета кнопки
//         t.classList.add('is-warning');
//
//     }
//     //выбрать все элементы
//     selectAll(t) {
//         let allNodes = document.getElementsByName('node');
//         console.log("List: ", allNodes);
//         for(var i=0, n=allNodes.length;i<n;i++) {
//             allNodes[i].checked = t.checked;
//         }
//     }
//
//     //вешать на кнопку старт
//     startEvent(t) {
//         //получаю кнопку-останов
//         let bb = document.getElementById(this.btnstop);
//
//         //очищаю предыдущий результат по списку
//         let elemList = document.getElementById(this.list);
//         console.log("Element list: ", elemList);
//         elemList.innerHTML = "";
//         elemList.innerText = "";
//         console.log("[after] Element list: ", elemList);
//
//         //добавляю инфу о количестве проверенных серверов
//         document.getElementById(this.checked).innerText = 0;
//
//         //получюа поле с чекбоксом
//         let box = document.getElementById(this.single);
//         console.log("BOX CHECKED : ", box.checked);
//         //при выставленном чекбоксом идет одиночная проверка, без - проверка по таймеру
//         let startStruct = {
//             Status: "enable",
//             Command: ""
//         };
//         if (box.checked) {
//             startStruct.Command = "start_single";
//         } else {
//             startStruct.Command = "start_period";
//         }
//
//         //отправляю на сервер команду на запуск проверки
//         this.socket.send(JSON.stringify(startStruct));
//
//         //получаю кнопку для изменения
//         t.innerText = "в работе...";
//         t.disabled = true;
//
//         //активирую кнопку-останов
//         bb.disabled = false;
//     }
//
//     //кнопка останов
//     stopEvent(e) {
//         let bb = document.getElementById(this.btnStop);
//         //проверка рабочей версии - одиночной или по таймеру
//         let btn = document.getElementById(this.single);
//         let ss = {
//             Status: "enable",
//             Command: ""
//         };
//         if (btn.checked) {
//             ss.command = "stop_single";
//
//         } else {
//             ss.command = "stop_period";
//         }
//         //отправляю на сервер команду на запуск проверки
//         this.socket.send(JSON.stringify(ss));
//     }
//
// }


/////////////////////////////////////////////////////////
//тестовый класс для проверки websockets
///////////////////////////////////////////////////////////
class SocketTester {
    constructor(idblock, list, checked, single, btn, btnStop, serverAddr) {
        this.block = document.getElementById(idblock);
        // this.socket = new WebSocket("ws://localhost:4001/ws/singlecheck");
        this.socket = new WebSocket(serverAddr);
        // this.socket = new WebSocket("wss://localhost:4001/admin/tester/ws");
        this.checked = checked
        this.btn = btn
        this.single = single
        this.list = list
        this.btnstop = btnStop

        //OPEN->WS
        this.socket.onopen = function (e) {
            console.log('[socket] connection established');
            console.log('[socket] send data to server');
        };

        //READ->WS
        this.socket.onmessage = function (e) {

            //парсинг блока данных
            let res = JSON.parse(e.data);
            console.log("------> ", res);

            //выстраиваю логическое дерево
            switch (res.client.command) {
                //установки информационного значения
                case "infoset":
                    //формирование списка серверов для возможности исключения
                    let listbox = document.getElementById('listbox');
                    //распределяю список серверов по боксам пропорционально (4 бокса)
                    console.log('Split list nodes: ', res.client.split_list_nodes);
                    for (let c = 0; c < res.client.split_list_nodes.length; c++) {
                        console.log("[", c, "]", ">>>>>>>>>iteration: ", res.client.split_list_nodes[c]);
                        //создаю сегмент из среза в массиве срезов поступивших по вебсокету
                        let d = document.createElement('div');
                        d.classList.add('column');
                        let dul = document.createElement('ul');
                        d.appendChild(dul);
                        listbox.appendChild(d);
                        //проход по циклу среза
                        for (let t = 0; t < res.client.split_list_nodes[c].length; t++) {
                            console.log('[', c, ']------------', 'iter: ', res.client.split_list_nodes[c][t]);
                            let li = document.createElement('li');
                            let label = document.createElement('label');
                            label.classList.add('checkbox');
                            let checkbox = document.createElement('input');
                            checkbox.type = "checkbox";

                            console.log("FILID  -----: ", res.client.split_list_nodes[c][t].filid);
                            console.log("ELEMENT  -----: ", res.client.split_list_nodes[c][t].element);
                            if (res.client.split_list_nodes[c][t].element.status) {
                                checkbox.checked = true;
                            }
                            checkbox.id = res.client.split_list_nodes[c][t].filid;
                            // checkbox.id = 'checkbox:'+c+t;
                            checkbox.classList.add('checkboxer');

                            ////добавить обработчик на каждый чекбокс
                            // checkbox.addEventListener('click', function(e){
                            //     console.log("Clicked: ", e.target.id, e.target.checked,"::: ", e.target.tester);
                            //     // //отправить сообщение для добавление в исключение
                            //     // let startStruct = {
                            //     //     Status: "",
                            //     //     Command: "disable_db",
                            //     // };
                            //     //
                            //     // console.log("check-lpu->server: ", startStruct);
                            //     // //отправляю на сервер команду на запуск проверки
                            //     // this.socket.send(JSON.stringify(startStruct));
                            // }, false);


                            let span = document.createElement('span');
                            span.classList.add('label_checkbox');
                            //поле чекбокса
                            // span.textContent = res.client.split_list_nodes[c][t].dbpath;
                            span.textContent = " " + res.client.split_list_nodes[c][t].filid;
                            label.appendChild(checkbox);
                            label.appendChild(span);
                            li.appendChild(label);
                            dul.appendChild(li);
                        }
                    }


                    //установка статуса вебсокета
                    document.getElementById('icon').classList.add("error_green");
                    let block = document.getElementById(idblock);
                    //установка количество серверов доступных для проверки
                    block.innerText = res.client.nodes;
                    console.log("FROM CLIENT: ", res);
                    //формирую список чекбоксов
                    let box = document.getElementById('listnodes');
                    console.log("-----> NODES: ", res.listnode.listdbnode);
                    //проверяю количество серверов для проверки, если 0 то все кнопки отключить
                    if (res.listnode.listdbnode === null) {
                        console.log("--- COUNT NODES = 0\n");
                        // enable `stop`
                        let bstop = document.getElementById("stop");
                        bstop.disabled = true;
                        // enable `btnzod`
                        let bzod = document.getElementById("btnzod");
                        bzod.disabled = true;
                        // enable `btnlpu`
                        let blpu = document.getElementById("btnlpu");
                        blpu.disabled = true;
                        // enable `btnall`
                        let ball = document.getElementById("btnall");
                        ball.disabled = true;

                    } else {
                        if (res.listnode.listdbnode.length === 0) {
                            console.log("--- COUNT NODES = 0\n");
                            // enable `stop`
                            let bstop = document.getElementById("stop");
                            bstop.disabled = true;
                            // enable `btnzod`
                            let bzod = document.getElementById("btnzod");
                            bzod.disabled = true;
                            // enable `btnlpu`
                            let blpu = document.getElementById("btnlpu");
                            blpu.disabled = true;
                            // enable `btnall`
                            let ball = document.getElementById("btnall");
                            ball.disabled = true;
                        } else {

                            // enable `stop`
                            let bstop = document.getElementById("stop");
                            bstop.disabled = true;
                            // enable `btnzod`
                            let bzod = document.getElementById("btnzod");
                            bzod.disabled = false;
                            // enable `btnlpu`
                            let blpu = document.getElementById("btnlpu");
                            blpu.disabled = false;
                            // enable `btnall`
                            let ball = document.getElementById("btnall");
                            ball.disabled = false;


                            let haveProcent = res.listnode.listdbnode.length % 3;
                            let median = 0;
                            if (haveProcent > 0) {
                                median = Math.floor(res.listnode.listdbnode.length / 3) + 1;
                            } else {
                                median = Math.floor(res.listnode.listdbnode.length / 3);
                            }


                            let timer1 = document.getElementById('timeupdate');
                            timer1.innerText = res.listnode.timeupdate;

                            let timeoutTCP1 = document.getElementById('timeoutTCP');
                            let timeoutCTX1 = document.getElementById('timeoutCTX');
                            let timeoutTCPZBD1 = document.getElementById('timeoutTCPZBD');
                            let timeoutCTXZBD1 = document.getElementById('timeoutCTXZBD');
                            timeoutTCP1.value = res.timertcp;
                            timeoutCTX1.value = res.timerctx;
                            timeoutTCPZBD1.value = res.timertcpzbd;
                            timeoutCTXZBD1.value = res.timerctxzbd;

                        }

                        break;

                    }
                    break;

                //обновление таймеров
                case "update_timers":
                    let timeoutTCP = document.getElementById('timeoutTCP');
                    let timeoutCTX = document.getElementById('timeoutCTX');
                    let timeoutTCPZBD = document.getElementById('timeoutTCPZBD');
                    let timeoutCTXZBD = document.getElementById('timeoutCTXZBD');
                    timeoutTCP.value = res.timertcp;
                    timeoutCTX.value = res.timerctx;
                    timeoutTCPZBD.value = res.timertcpzbd;
                    timeoutCTXZBD.value = res.timerctxzbd;
                    let spantimer = document.getElementById('updatetimerspan');
                    spantimer.innerText = "";

                    var currentdate = new Date();
                    var datetime = currentdate.getDate() + "/"
                        + (currentdate.getMonth() + 1) + "/"
                        + currentdate.getFullYear() + " @ "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();

                    spantimer.innerText = "таймеры успешно обновлены " + datetime;
                    // spantimer.innerText = "таймеры успешно обновлены " + dd.getFullYear()+"-"+dd.getMonth()+"-"+dd.getDay()+" "+dd.getHours()+":"+dd.getMinutes()+":"+dd.getMilliseconds();
                    break;

                //обновление списка серверов
                case "update_list":
                    console.log("--update_list---: ", res);
                    //изменяю статус изменением цвета кнопки
                    let t = document.getElementById('btnupdate');
                    t.classList.replace('is-warning', 'is-info');
                    let timer = document.getElementById('timeupdate');
                    timer.innerText = res.listnode.timeupdate;
                    //

                    let bl = document.getElementById(idblock);
                    bl.innerText = res.client.nodes;

                    break;

                case "end":
                    break;

                case "terminate":
                    console.log("Getting TERMINATE COMMAND: ", res);
                    switch (res.client.typecheck) {
                        case "start_zod":
                            let r = document.getElementById("btnzod");
                            r.disabled = false;
                            r.innerText = "проверить бд ЦОД";
                            let bbbstop = document.getElementById("stop");
                            bbbstop.disabled = true;
                            break;

                        case "start_lpu":
                            let r1 = document.getElementById("btnlpu");
                            r1.disabled = false;
                            r1.innerText = "проверить бд ЛПУ";
                            let bbb2 = document.getElementById("stop");
                            bbb2.disabled = true;
                            break;
                        case "start_all":
                            let r2 = document.getElementById("btnall");
                            r2.disabled = false;
                            r2.innerText = "проверить все сервера";
                            let bbb3 = document.getElementById("stop");
                            bbb3.disabled = true;
                            break;
                    }
                    console.log("--break + enable buttons");
                    // enable `stop`
                    // let bstop  = document.getElementById("stop");
                    // bstop.disabled = false;
                    // enable `btnzod`
                    let bzod = document.getElementById("btnzod");
                    bzod.disabled = false;
                    // enable `btnlpu`
                    let blpu = document.getElementById("btnlpu");
                    blpu.disabled = false;
                    // enable `btnall`
                    let ball = document.getElementById("btnall");
                    ball.disabled = false;

                    break;


                //количество отработавших горутин
                case "count":
                    console.log("COUNT---- ", res);
                    //добавляю инфу о количестве проверяемых
                    document.getElementById("infobd").innerText = res.client.nodes;
                    //добавляю инфу о количестве проверенных серверов
                    document.getElementById("checked").innerText = res.client.count_check;

                    break;

                //одиночная проверка
                case "work":
                    if (res.worker.result === false) {
                        //сервер не доступен

                        let tbody = document.getElementById('list');
                        console.log(tbody);
                        let tr = document.createElement("tr");


                        //filid
                        let td0 = document.createElement("td");
                        td0.innerHTML = res.worker.filid;
                        tr.appendChild(td0);

                        //host
                        let td = document.createElement("td");
                        td.innerHTML = res.worker.node;
                        tr.appendChild(td);

                        //error
                        let td1 = document.createElement("td");
                        td1.innerHTML = res.worker.error;
                        tr.appendChild(td1);

                        //time
                        let td2 = document.createElement("td");
                        td2.innerHTML = res.worker.time_run;
                        tr.appendChild(td2);

                        tbody.appendChild(tr);

                        // let tr1 = document.createElement("tr");
                        // let tr2 = document.createElement("tr");
                        //
                        //
                        // let tr1 = box.insertRow(0);
                        // let td0 = tr1.insertCell(0);
                        // let td1 = tr1.insertCell(1);
                        // let td2 = tr1.insertCell(2);
                        //
                        // td0.innerHTML = res.worker.node;
                        // td1.innerHTML = res.worker.error;
                        // td2.innerHTML = res.worker.time_run;
                        //
                        // tr.appendChild(td1);
                        // tr.appendChild(td2);
                        // tr.appendChild(td3);
                        //
                        // box.appendChild(tr);

                        //обновляю информацию о количестве  отработанных воркеров
                        document.getElementById(checked).innerText = res.client.count_check;
                    }
                    break;

                //обработка ответа о завершении проверки lpu
                case "endstart_lpu" :
                    //изменяю статус кнопки и делаю ее доступной
                    let bb2 = document.getElementById("btnlpu");
                    bb2.disabled = false;
                    bb2.innerText = "проверить бд ЛПУ";
                    //изменяю статус кнопки останов
                    document.getElementById("stop").disabled = true;


                    console.log("проверка закончена lpu ", res);

                    //добавляю инфу о количестве проверенных серверов
                    document.getElementById(checked).innerText = res.client.count_check;

                    break;

                //обработка ответа о завершении проверки zod
                case "endstart_zod" :
                    //изменяю статус кнопки и делаю ее доступной
                    let bb3 = document.getElementById("btnzod");
                    bb3.disabled = false;
                    bb3.innerText = "проверить бд ЦОД";
                    //изменяю статус кнопки останов
                    let b3 = document.getElementById("stop");
                    b3.disabled = true;

                    console.log("проверка закончена ", res);

                    //добавляю инфу о количестве проверенных серверов
                    document.getElementById(checked).innerText = res.client.count_check;

                    break;


                //обработка ответа о завершении проверки всех серверов
                case "endstart_all" :
                    //изменяю статус кнопки и делаю ее доступной
                    let bb = document.getElementById("btnall");
                    bb.disabled = false;
                    bb.innerText = "проверить сервера";
                    //изменяю статус кнопки останов
                    let b = document.getElementById("stop");
                    b.disabled = true;

                    console.log("проверка закончена ", res);

                    //добавляю инфу о количестве проверенных серверов
                    document.getElementById(checked).innerText = res.client.count_check;

                    break;
                default:
                    console.log("wrong  res.client.command", res.client.command);
                    break;
            }
            let stock = ["endstart_all", "endstart_zod", "endstart_lpu"];
            for (let index = 0; index < stock.length; index++) {
                if (res.client.command === stock[index]) {
                    // enable `stop`
                    // let bstop = document.getElementById("stop");
                    // bstop.disabled = f;
                    // enable `btnzod`
                    let bzod = document.getElementById("btnzod");
                    bzod.disabled = false;
                    // enable `btnlpu`
                    let blpu = document.getElementById("btnlpu");
                    blpu.disabled = false;
                    // enable `btnall`
                    let ball = document.getElementById("btnall");
                    ball.disabled = false;
                }

            }
        };

        //соединеие закрыто
        this.socket.onclose = function (e) {
            if (e.wasClean) {
                console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
            } else {
                console.log('[socket] соединение закрыто со стороны сервера ', e.code);
            }
            //установка статуса вебсокета
            document.getElementById('icon').classList.replace("error_green", "error_red");
        };

        //ошибка
        this.socket.onerror = function (e) {
            console.log(`[socket] websocket error ${e.message}`);
            //установка статуса вебсокета
            document.getElementById('icon').classList.replace("error_green", "error_red");
        };

    }


    //проверка LPU сегмента
    checkLPU() {
        //получаю установленные  таймауты
        let timeoutTCP = document.getElementById('timeoutTCP');
        let timeoutCTX = document.getElementById('timeoutCTX');
        let timeoutTCPZBD = document.getElementById('timeoutTCPZBD');
        let timeoutCTXZBD = document.getElementById('timeoutCTXZBD');
        let startStruct = {
            Status: "enable",
            Command: "check_lpu",
            TimerCTX: parseInt(timeoutCTX.value),
            TimerTCP: parseInt(timeoutTCP.value),
            TimerCTXZBD: parseInt(timeoutCTXZBD.value),
            TimerTCPZBD: parseInt(timeoutTCPZBD.value),
        };


        console.log("check-lpu->server: ", startStruct);
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(startStruct));
    }

    //проверка ZBD сегмента
    checkZBD() {
        let timeoutTCP = document.getElementById('timeoutTCP');
        let timeoutCTX = document.getElementById('timeoutCTX');
        let timeoutTCPZBD = document.getElementById('timeoutTCPZBD');
        let timeoutCTXZBD = document.getElementById('timeoutCTXZBD');
        let startStruct = {
            Status: "enable",
            Command: "check_zbd",
            TimerCTX: parseInt(timeoutCTX.value),
            TimerTCP: parseInt(timeoutTCP.value),
            TimerCTXZBD: parseInt(timeoutCTXZBD.value),
            TimerTCPZBD: parseInt(timeoutTCPZBD.value),
        };
        console.log("check-zbd->server: ", startStruct);
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(startStruct));
    }


    //проверка всех серверов
    checkALL() {
        let timeoutTCP = document.getElementById('timeoutTCP');
        let timeoutCTX = document.getElementById('timeoutCTX');
        let timeoutTCPZBD = document.getElementById('timeoutTCPZBD');
        let timeoutCTXZBD = document.getElementById('timeoutCTXZBD');
        let startStruct = {
            Status: "enable",
            Command: "check_all",
            TimerCTX: parseInt(timeoutCTX.value),
            TimerTCP: parseInt(timeoutTCP.value),
            TimerCTXZBD: parseInt(timeoutCTXZBD.value),
            TimerTCPZBD: parseInt(timeoutTCPZBD.value),
        };
        console.log("check-all->server: ", startStruct);
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(startStruct));
    }


    //обновление таймеров (btn) id = `updateTimers`
    updateTimers() {
        let timeoutTCP = document.getElementById('timeoutTCP');
        let timeoutCTX = document.getElementById('timeoutCTX');
        let timeoutTCPZBD = document.getElementById('timeoutTCPZBD');
        let timeoutCTXZBD = document.getElementById('timeoutCTXZBD');
        let startStruct = {
            Status: "enable",
            Command: "update_timers",
            TimerCTX: parseInt(timeoutCTX.value),
            TimerTCP: parseInt(timeoutTCP.value),
            TimerCTXZBD: parseInt(timeoutCTXZBD.value),
            TimerTCPZBD: parseInt(timeoutTCPZBD.value),
        };
        console.log("update-timers->server: ", startStruct);
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(startStruct));
    }

    //обработчик на открытие панели для исключения серверов
    checklistShow(t) {
        //получаю бокс со списком серверов
        let box = document.getElementById('listcheck');
        if (box.style.display === 'none') {
            box.style.display = 'block';
            //изменяю окраску
            t.style.backgroundColor = 'aqua';
        } else {
            //изменяю окраску
            t.style.backgroundColor = 'white';
            box.style.display = 'none';
        }
    }

    //обработка нажатия на обновление списка серверов
    updateServerList(t) {
        // получаю объект кнопки
        let bb = document.getElementById(t.id);
        if (bb === null) {
            console.log("ERROR не найден объект кнопка обновления");
            return null;
        }
        let startStruct = {
            Status: "enable",
            Command: "update_list",
        };
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(startStruct));

        //изменяю статус изменением цвета кнопки
        t.classList.replace('is-info', 'is-warning');

    }

    //выборка элементов по полю
    checkdByListID(e) {
        //снимаю все значения с полей
        let allNodes = document.getElementsByName('node');
        for (var i = 0, n = allNodes.length; i < n; i++) {
            allNodes[i].checked = false;
        }
        //ставлю введенные
        let b = document.getElementById('idnum');
        if (e.key === "Enter") {
            console.log("Enter: ", b.value);
            let res = b.value.split(",");
            console.log("==> ", res);
            for (let a = 0; a < res.length; a++) {

                let current = document.getElementById("ids" + res[a]);
                console.log("Found ID: ", "ids" + res[a], current, res);
                if (current != null) {
                    current.checked = true;
                }
            }
        }
    }

    //выбрать все элементы
    selectAll(t) {
        let allNodes = document.getElementsByName('node');
        console.log("List: ", allNodes);
        for (var i = 0, n = allNodes.length; i < n; i++) {
            allNodes[i].checked = t.checked;
        }
    }

    //запустить проверку бд ЛПУ
    startCheckLPU(t) {
        //получаю все отмеченные чекбоксы по исключенным серверам
        let listTotal = document.getElementsByClassName('checkboxer');
        let stocker = [];

        console.log('ListTotal: ', listTotal);
        for (let x = 0; x < listTotal.length; x++) {
            let elem = {
                IDS: listTotal[x].id,
                Status: listTotal[x].checked,
            };
            stocker.push(elem);
        }
        console.log('STOCKER: ', stocker);

        //получаю кнопку-останов
        let bb = document.getElementById(this.btnstop);

        //очищаю предыдущий результат по списку
        let elemList = document.getElementById(this.list);
        console.log("Element list: ", elemList);
        elemList.innerHTML = "";
        elemList.innerText = "";
        console.log("[after] Element list: ", elemList);

        //добавляю инфу о количестве проверенных серверов
        document.getElementById(this.checked).innerText = 0;

        // //получаю список чекбоксов с выделенными элементами
        // let allNodes = document.getElementsByName('node');
        // let listChecked = [];
        // for (var i = 0, n = allNodes.length; i < n; i++) {
        //     if (allNodes[i].checked === true) {
        //         listChecked.push(parseInt(allNodes[i].id.split("ids")[1]))
        //     }
        // }

        let startStruct = {
            Status: "enable",
            Command: "start_lpu",
            Stocker: stocker,
        };
        console.log("result package: ", startStruct);

        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(startStruct));

        //получаю кнопку для изменения
        t.innerText = "в работе...";
        t.disabled = true;

        // enable `stop`
        let bstop = document.getElementById("stop");
        bstop.disabled = false;
        // enable `btnzod`
        let bzod = document.getElementById("btnzod");
        bzod.disabled = true;
        // enable `btnlpu`
        let blpu = document.getElementById("btnlpu");
        blpu.disabled = true;
        // enable `btnall`
        let ball = document.getElementById("btnall");
        ball.disabled = true;

        //активирую кнопку-останов
        bb.disabled = false;
    }


    //запустить проверку бд ЦОД
    startCheckZOD(t) {
        //получаю все отмеченные чекбоксы по исключенным серверам
        let listTotal = document.getElementsByClassName('checkboxer');
        let stocker = [];

        console.log('ListTotal: ', listTotal);
        for (let x = 0; x < listTotal.length; x++) {
            let elem = {
                IDS: listTotal[x].id,
                Status: listTotal[x].checked,
            };
            stocker.push(elem);
        }
        console.log('STOCKER: ', stocker);

        //получаю кнопку-останов
        let bb = document.getElementById(this.btnstop);

        //очищаю предыдущий результат по списку
        let elemList = document.getElementById(this.list);
        console.log("Element list: ", elemList);
        elemList.innerHTML = "";
        elemList.innerText = "";
        console.log("[after] Element list: ", elemList);

        //добавляю инфу о количестве проверенных серверов
        document.getElementById(this.checked).innerText = 0;

        let startStruct = {
            Status: "enable",
            Command: "start_zod",
            Stocker: stocker,
        };
        console.log("result package: ", startStruct);

        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(startStruct));

        //получаю кнопку для изменения
        t.innerText = "в работе...";
        t.disabled = true;

        // enable `stop`
        let bstop = document.getElementById("stop");
        bstop.disabled = true;
        // enable `btnzod`
        let bzod = document.getElementById("btnzod");
        bzod.disabled = true;
        // enable `btnlpu`
        let blpu = document.getElementById("btnlpu");
        blpu.disabled = true;
        // enable `btnall`
        let ball = document.getElementById("btnall");
        ball.disabled = true;


        //активирую кнопку-останов
        bb.disabled = false;
    }


    //запуск проверки всех серверов
    startCheckALL(t) {
        //получаю все отмеченные чекбоксы по исключенным серверам
        let listTotal = document.getElementsByClassName('checkboxer');
        let stocker = [];

        console.log('ListTotal: ', listTotal);
        for (let x = 0; x < listTotal.length; x++) {
            let elem = {
                IDS: listTotal[x].id,
                Status: listTotal[x].checked,
            };
            stocker.push(elem);
        }
        console.log('STOCKER: ', stocker);

        //получаю кнопку-останов
        let bb = document.getElementById(this.btnstop);

        //очищаю предыдущий результат по списку
        let elemList = document.getElementById(this.list);
        console.log("Element list: ", elemList);
        elemList.innerHTML = "";
        elemList.innerText = "";
        console.log("[after] Element list: ", elemList);

        //добавляю инфу о количестве проверенных серверов
        document.getElementById(this.checked).innerText = 0;

        // //получаю список чекбоксов с выделенными элементами
        // let allNodes = document.getElementsByName('node');
        // let listChecked = [];
        // for (var i = 0, n = allNodes.length; i < n; i++) {
        //     if (allNodes[i].checked === true) {
        //         listChecked.push(parseInt(allNodes[i].id.split("ids")[1]))
        //     }
        // }

        let startStruct = {
            Status: "enable",
            Command: "start_all",
            Stocker: stocker,
            // ListNode: listChecked,
        };
        console.log("result package: ", startStruct);

        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(startStruct));

        //получаю кнопку для изменения
        t.innerText = "в работе...";
        t.disabled = true;

        // enable `stop`
        let bstop = document.getElementById("stop");
        bstop.disabled = false;
        // enable `btnzod`
        let bzod = document.getElementById("btnzod");
        bzod.disabled = true;
        // enable `btnlpu`
        let blpu = document.getElementById("btnlpu");
        blpu.disabled = true;
        // enable `btnall`
        let ball = document.getElementById("btnall");
        ball.disabled = true;


        //активирую кнопку-останов
        bb.disabled = false;
    }

    //кнопка останов
    stopEvent(e) {
        let bb = document.getElementById(this.btnStop);
        //проверка рабочей версии - одиночной или по таймеру
        let ss = {
            Status: "enable",
            Command: "stop_single",
        };
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(ss));
    }

}

/////////////////////////////////////////////////////////
//HandlerWebsocketSingleFormChecker:: `одиночная` проверка с вводом параметров через вебморду
///////////////////////////////////////////////////////////
class HandlerWebsocketSingleFormChecker {
    constructor(core, connIcon, serverAddr) {
        this.core = core;
        this.icon = connIcon;
        this.connectIcon = document.getElementById(connIcon);
        console.log('dfdfg=>', this.connectIcon, connIcon);
        this.socket = new WebSocket(serverAddr);
        //OPEN->WS
        this.socket.onopen = function (e) {
            console.log('[socket] connection established');
            console.log('[socket] send data to server');
            document.getElementById(connIcon).classList.add("error_green");
        };


        //READ->WS
        this.socket.onmessage = function (e) {
            //парсинг блока данных
            let res = JSON.parse(e.data);
            console.log("------> ", res);
            //выстраиваю логическое дерево
            switch (res.client.command) {
                // //установки информационного значения
                // case "infoset":
                //     let block = document.getElementById(idblock);
                //     block.innerText = res.client.nodes;
                //     console.log("FROM CLIENT: ", res);
                //     break;
                // //количество отработавших горутин
                // case "count":
                //     console.log("COUNT---- ",res);
                //     //добавляю инфу о количестве проверенных серверов
                //     document.getElementById(checked).innerText = res.client.count_check;
                //     break;

                //одиночная проверка
                case "work":
                    if (res.worker.result === false) {
                        //пробую получить блок с данными по IDS
                        let block = document.getElementById(res.worker.ids);

                        if (block === null) {
                            //блок не найден, создаю
                            let coreS = document.getElementById(core);
                            let newdiv = document.createElement('div');
                            newdiv.classList.add('box');
                            newdiv.id = res.worker.ids;

                            let button = document.createElement('div');
                            button.classList.add('button');
                            button.classList.add('is-primary');

                            //определить функцию
                            // let fu = function(res, sock, event) {
                            //     //отправить команду во вебсокету на прекращение работы этого обработчика
                            //     let MsgServer = {
                            //         Command: "kill",
                            //         Marked: res.worker.marked,
                            //     };
                            //     console.log("MSgServer=killaddeventlistener: ", MsgServer);
                            //     console.log("res: ", res);
                            //     console.log("sock: ", sock);
                            //     //отправляю на сервер команду на запуск проверки
                            //     // sock.socket.send(JSON.stringify(MsgServer));
                            //     // //убираем бокс по этой горутине
                            //     // document.getElementById(res.worker.ids).remove();
                            // };
                            let fu2 = function (a, b, e) {
                                console.log(a, b, e);
                                console.log("RESS --- > ", b);
                                let MsgServer = {
                                    Command: "kill",
                                    Marked: res.worker.marked,
                                };
                                a.send(JSON.stringify(MsgServer));
                                document.getElementById(b.worker.ids).remove();
                            }

                            //добавляю обработчик на кнопку для прекращения работы обработчика
                            // button.addEventListener("click", () => fu(res, this.socket, e));
                            // button.addEventListener("click", () => fu2(this.socket, res, e));
                            button.addEventListener("click", event => {
                                this.CloseWorker(event)
                            });


                            let infobox = document.createElement('div');
                            infobox.classList.add('box');
                            infobox.id = res.worker.ids + "infobox";

                            let logbox = document.createElement('div');
                            logbox.classList.add('box');
                            logbox.id = res.worker.ids + "logbox";

                            let text = document.createElement('textarea');
                            text.classList.add('textarea');
                            text.id = res.worker.ids + "text";

                            logbox.appendChild(text);
                            newdiv.appendChild(infobox);
                            newdiv.appendChild(button);
                            newdiv.appendChild(logbox);
                            coreS.appendChild(newdiv);


                        } else {
                            //блок найден, обновляю данные
                            let infobox = document.getElementById(res.worker.ids + "infobox");
                            let text = document.getElementById(res.worker.ids + "text");
                            infobox.innerHTML = "";
                            infobox.innerHTML = "=> " + res.worker.node + " run: " + res.worker.time_run;
                            text.value += "\n";
                            text.value += res.worker.result + " " + res.worker.error + " " + res.worker.timerun;
                        }


                        // //сервер не доступен
                        // let tbody = document.getElementById('list');
                        // console.log(tbody);
                        // let tr = document.createElement("tr");
                        //
                        // //host
                        // let td = document.createElement("td");
                        // td.innerHTML = res.worker.node;
                        // tr.appendChild(td);
                        //
                        // //error
                        // let td1 = document.createElement("td");
                        // td1.innerHTML = res.worker.error;
                        // tr.appendChild(td1);
                        //
                        // //time
                        // let td2 = document.createElement("td");
                        // td2.innerHTML = res.worker.time_run;
                        // tr.appendChild(td2);
                        //
                        // tbody.appendChild(tr);
                        //
                        // // let tr1 = document.createElement("tr");
                        // // let tr2 = document.createElement("tr");
                        // //
                        // //
                        // // let tr1 = box.insertRow(0);
                        // // let td0 = tr1.insertCell(0);
                        // // let td1 = tr1.insertCell(1);
                        // // let td2 = tr1.insertCell(2);
                        // //
                        // // td0.innerHTML = res.worker.node;
                        // // td1.innerHTML = res.worker.error;
                        // // td2.innerHTML = res.worker.time_run;
                        // //
                        // // tr.appendChild(td1);
                        // // tr.appendChild(td2);
                        // // tr.appendChild(td3);
                        // //
                        // // box.appendChild(tr);
                        //
                        // //обновляю информацию о количестве  отработанных воркеров
                        // document.getElementById(checked).innerText = res.client.count_check;
                    }
                    break;

                //постоянная проверка, с заданной периодичностью
                case "end":
                    //изменяю статус кнопки и делаю ее доступной
                    let bb = document.getElementById(btn);
                    bb.disabled = false;
                    bb.innerText = "проверить сервера";
                    //изменяю статус кнопки останов
                    let b = document.getElementById(btnStop);
                    b.disabled = true;

                    console.log("проверка закончена ", res);

                    //добавляю инфу о количестве проверенных серверов
                    document.getElementById(checked).innerText = res.client.count_check;

                    break;
                default:
                    console.log("wrong  res.client.command", res.client.command);
                    break;
            }
        };

        //соединеие закрыто
        this.socket.onclose = function (e) {
            if (e.wasClean) {
                console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
            } else {
                console.log('[socket] соединение закрыто со стороны сервера ', e.code);
            }
            document.getElementById(connIcon).classList.add("error_red");
        };

        //ошибка
        this.socket.onerror = function (e) {
            console.log(`[socket] websocket error ${e.message}`);
        };

    }


    CloseWorker(res, ids, e) {
        console.log("ETHIS: ", e.this);
        console.log("E: ", e);
        console.log("ids: ", ids);
        console.log("res: ", res);
        // //отправить команду во вебсокету на прекращение работы этого обработчика
        // let MsgServer = {
        //     Command: "kill",
        //     Marked: res.worker.marked,
        // };
        // console.log("MSgServer=killaddeventlistener: ", MsgServer);
        // //отправляю на сервер команду на запуск проверки
        // this.socket.send(JSON.stringify(MsgServer));
        // //убираем бокс по этой горутине
        // document.getElementById(res.worker.ids).remove();
    };

    //функция создания блока под горутину
    MakeForm(core, ids) {
        let coreS = document.getElementById(core);
        let newdiv = document.createElement('div');
        newdiv.classList.add('box');
        let infobox = document.createElement('div');
        infobox.classList.add('box');
        let logbox = document.createElement('div');
        logbox.classList.add('box');
        newdiv.appendChild(infobox);
        newdiv.appendChild(logbox);
        coreS.appendChild(newdiv);
    }

    //кнопка отправки данных на сервер из формы создания обработчика
    SendForm(e) {
        //get form
        let cform = document.forms.noda;
        console.log("Form: ", cform);
        //make msg to server
        let MsgServer = {
            Command: "new",
            Dbs: cform.elements.db.value,
            Period: cform.elements.period.value,
            Timeout: cform.elements.timeout.value,
        };
        console.log("MSgServer: ", MsgServer);
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(MsgServer));
    }


    //вешать на кнопку старт
    startEvent(t) {
        //получаю кнопку-останов
        let bb = document.getElementById(this.btnstop);

        //очищаю предыдущий результат по списку
        let elemList = document.getElementById(this.list);
        console.log("Element list: ", elemList);
        elemList.innerHTML = "";
        elemList.innerText = "";
        console.log("[after] Element list: ", elemList);

        //добавляю инфу о количестве проверенных серверов
        document.getElementById(this.checked).innerText = 0;

        //получюа поле с чекбоксом
        let box = document.getElementById(this.single);
        console.log("BOX CHECKED : ", box.checked);
        //при выставленном чекбоксом идет одиночная проверка, без - проверка по таймеру
        let startStruct = {
            Status: "enable",
            Command: ""
        };
        if (box.checked) {
            startStruct.Command = "start_single";
        } else {
            startStruct.Command = "start_period";
        }

        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(startStruct));

        //получаю кнопку для изменения
        t.innerText = "в работе...";
        t.disabled = true;

        //активирую кнопку-останов
        bb.disabled = false;
    }

    //кнопка останов
    stopEvent(e) {
        let bb = document.getElementById(this.btnStop);
        //проверка рабочей версии - одиночной или по таймеру
        let btn = document.getElementById(this.single);
        let ss = {
            Status: "enable",
            Command: ""
        };
        if (btn.checked) {
            ss.command = "stop_single";

        } else {
            ss.command = "stop_period";
        }
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(ss));
    }

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//HandleWebsocketMultiChecker:: множественная проверка серверов с настройкой через вебморду
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class HandlerWebsocketMultiCheck {
    constructor(core, connIcon, serverAddr) {
        this.core = core;
        this.icon = connIcon;
        this.connectIcon = document.getElementById(connIcon);
        this.socket = new WebSocket(serverAddr);
        //OPEN->WS
        this.socket.onopen = function (e) {
            console.log('[socket] connection established');
            console.log('[socket] send data to server');
            document.getElementById(connIcon).classList.add("error_green");
        };


        //READ->WS
        this.socket.onmessage = function (e) {
            //парсинг блока данных
            let res = JSON.parse(e.data);
            console.log("------> ", res);
            //выстраиваю логическое дерево
            switch (res.client.command) {

                //установка параметров
                case "infoset":

                    // ---------------------------------------------------------
                    //общие действия которые надо выполнить, в любом случае
                    // ---------------------------------------------------------
                    //устанавливаю таймеры что заданы в конфиге
                    let timeoutTCP1 = document.getElementById('timeoutTCP');
                    let timeoutCTX1 = document.getElementById('timeoutCTX');
                    let timeoutTCPZBD1 = document.getElementById('timeoutTCPZBD');
                    let timeoutCTXZBD1 = document.getElementById('timeoutCTXZBD');
                    timeoutTCP1.value = res.timertcp;
                    timeoutCTX1.value = res.timerctx;
                    timeoutTCPZBD1.value = res.timertcpzbd;
                    timeoutCTXZBD1.value = res.timerctxzbd;

                    //установка таймера из конфига для постоянных проверок
                    let timerBlock = document.getElementById('timervalue');
                    timerBlock.value = res.timer_multi_check;

                    //проверяю есть ли уже запущенная проверка
                    if (res.status_multi_check === true) {
                        //инфо мессага в консоль
                        console.log("уже есть запущенная проверка, другая невозможно");

                        //устанавливаю общее количество серверов доступных для проверки
                        let block = document.getElementById('countServers');
                        block.innerText = res.client.nodes_checked;
                        console.log("Checked: ", res.client.nodes_checked);
                        console.log("ListNodes: ", res.client.nodes);
                        // console.log('infoset: ', res);

                        // //изменяю кнопку старта
                        // let bbStop = document.getElementById('stopMultiCheck');
                        // bbStop.disabled = true;

                        //изменяю текстовку кнопки старта
                        let bbStart = document.getElementById('startMultiCheck');
                        bbStart.innerText = 'в работе...';
                        bbStart.disabled = true;


                        //формирование списка серверов для возможности исключения
                        let listbox = document.getElementById('listbox');
                        //распределяю список серверов по боксам пропорционально (4 бокса)
                        console.log('Split list nodes: ', res.client.split_list_nodes_checked);
                        for (let c = 0; c < res.client.split_list_nodes.length; c++) {
                            console.log("[", c, "]", ">>>>>>>>>iteration: ", res.client.split_list_nodes[c]);
                            //создаю сегмент из среза в массиве срезов поступивших по вебсокету
                            let d = document.createElement('div');
                            d.classList.add('column');
                            let dul = document.createElement('ul');
                            d.appendChild(dul);
                            listbox.appendChild(d);
                            //проход по циклу среза
                            for (let t = 0; t < res.client.split_list_nodes[c].length; t++) {
                                console.log('[', c, ']------------', 'iter: ', res.client.split_list_nodes[c][t]);
                                let li = document.createElement('li');
                                let label = document.createElement('label');
                                label.classList.add('checkbox');
                                let checkbox = document.createElement('input');
                                checkbox.type = "checkbox";

                                console.log("FILID  -----: ", res.client.split_list_nodes[c][t].filid);
                                console.log("ELEMENT  -----: ", res.client.split_list_nodes[c][t].element);
                                if (res.client.split_list_nodes[c][t].element.status) {
                                    checkbox.checked = true;
                                }
                                checkbox.id = res.client.split_list_nodes[c][t].filid;
                                // checkbox.id = 'checkbox:'+c+t;
                                checkbox.classList.add('checkboxer');

                                let span = document.createElement('span');
                                span.classList.add('label_checkbox');
                                //поле чекбокса
                                // span.textContent = res.client.split_list_nodes[c][t].dbpath;
                                span.textContent = " " + res.client.split_list_nodes[c][t].filid;
                                label.appendChild(checkbox);
                                label.appendChild(span);
                                li.appendChild(label);
                                dul.appendChild(li);
                            }
                        }


                    } else {
                        //инфо мессага в консоль
                        console.log("проверки нет, все ок");

                        //изменяю текстовку кнопки старта
                        let bbStart = document.getElementById('startMultiCheck');
                        bbStart.innerText = 'запустить проверку с периодичностью по таймеру';
                        bbStart.disabled = false;

                        //формирование списка серверов для возможности исключения
                        let listbox = document.getElementById('listbox');
                        //распределяю список серверов по боксам пропорционально (4 бокса)
                        console.log('Split list nodes: ', res.client.split_list_nodes);
                        for (let c = 0; c < res.client.split_list_nodes.length; c++) {
                            console.log("[", c, "]", ">>>>>>>>>iteration: ", res.client.split_list_nodes[c]);
                            //создаю сегмент из среза в массиве срезов поступивших по вебсокету
                            let d = document.createElement('div');
                            d.classList.add('column');
                            let dul = document.createElement('ul');
                            d.appendChild(dul);
                            listbox.appendChild(d);
                            //проход по циклу среза
                            for (let t = 0; t < res.client.split_list_nodes[c].length; t++) {
                                console.log('[', c, ']------------', 'iter: ', res.client.split_list_nodes[c][t]);
                                let li = document.createElement('li');
                                let label = document.createElement('label');
                                label.classList.add('checkbox');
                                let checkbox = document.createElement('input');
                                checkbox.type = "checkbox";

                                console.log("FILID  -----: ", res.client.split_list_nodes[c][t].filid);
                                console.log("ELEMENT  -----: ", res.client.split_list_nodes[c][t].element);
                                if (res.client.split_list_nodes[c][t].element.status) {
                                    checkbox.checked = true;
                                }
                                checkbox.id = res.client.split_list_nodes[c][t].filid;
                                // checkbox.id = 'checkbox:'+c+t;
                                checkbox.classList.add('checkboxer');

                                let span = document.createElement('span');
                                span.classList.add('label_checkbox');
                                //поле чекбокса
                                // span.textContent = res.client.split_list_nodes[c][t].dbpath;
                                span.textContent = " " + res.client.split_list_nodes[c][t].filid;
                                label.appendChild(checkbox);
                                label.appendChild(span);
                                li.appendChild(label);
                                dul.appendChild(li);
                            }
                        }

                        //устанавливаю общее количество серверов доступных для проверки
                        let block = document.getElementById('countServers');
                        block.innerText = res.client.nodes;
                        console.log('infoset: ', res);

                        //делаю кнопку останова пассивной
                        let bbStop = document.getElementById('stopMultiCheck');
                        bbStop.disabled = true;
                    }

                    break;

                //обновление таймеров
                case "update_timers":
                    let timeoutTCP = document.getElementById('timeoutTCP');
                    let timeoutCTX = document.getElementById('timeoutCTX');
                    let timeoutTCPZBD = document.getElementById('timeoutTCPZBD');
                    let timeoutCTXZBD = document.getElementById('timeoutCTXZBD');
                    timeoutTCP.value = res.timertcp;
                    timeoutCTX.value = res.timerctx;
                    timeoutTCPZBD.value = res.timertcpzbd;
                    timeoutCTXZBD.value = res.timerctxzbd;
                    let spantimer = document.getElementById('updatetimerspan');
                    spantimer.innerText = "";

                    var currentdate = new Date();
                    var datetime = currentdate.getDate() + "/"
                        + (currentdate.getMonth() + 1) + "/"
                        + currentdate.getFullYear() + " @ "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();

                    spantimer.innerText = "таймеры успешно обновлены " + datetime;
                    // spantimer.innerText = "таймеры успешно обновлены " + dd.getFullYear()+"-"+dd.getMonth()+"-"+dd.getDay()+" "+dd.getHours()+":"+dd.getMinutes()+":"+dd.getMilliseconds();
                    break;


                //установока информации по количеству проверок и времени последней проверки
                case "setinfo":
                    let countinfo = document.getElementById('countinfo');
                    countinfo.innerText = "";

                    // var currentdate = new Date();
                    // var datetime =  currentdate.getDate() + "/"
                    //     + (currentdate.getMonth()+1)  + "/"
                    //     + currentdate.getFullYear() + " @ "
                    //     + currentdate.getHours() + ":"
                    //     + currentdate.getMinutes() + ":"
                    //     + currentdate.getSeconds();

                    countinfo.innerText = res.client.count_iteration + "  " + res.client.time_checked;
                    console.log("---->" + res.client.count_iteration + "  " + res.client.time_checked);
                    break;

                //одиночная проверка
                case "work":
                    if (res.worker.result === false) {
                        //пробую получить блок с данными по IDS
                        let block = document.getElementById(res.worker.ids);

                        if (block === null) {
                            //блок не найден, создаю
                            let coreS = document.getElementById(core);
                            let newdiv = document.createElement('div');
                            newdiv.classList.add('box');
                            newdiv.id = res.worker.ids;

                            let button = document.createElement('div');
                            button.classList.add('button');
                            button.classList.add('is-primary');

                            let fu2 = function (a, b, e) {
                                console.log(a, b, e);
                                let MsgServer = {
                                    Command: "kill",
                                    Marked: res.worker.marked,
                                };
                                a.send(JSON.stringify(MsgServer));
                                document.getElementById(b.worker.ids).remove();
                            }

                            //добавляю обработчик на кнопку для прекращения работы обработчика
                            button.addEventListener("click", event => {
                                this.CloseWorker(event)
                            });


                            let infobox = document.createElement('div');
                            infobox.classList.add('box');
                            infobox.id = res.worker.ids + "infobox";

                            let logbox = document.createElement('div');
                            logbox.classList.add('box');
                            logbox.id = res.worker.ids + "logbox";

                            let text = document.createElement('textarea');
                            text.classList.add('textarea');
                            text.id = res.worker.ids + "text";

                            logbox.appendChild(text);
                            newdiv.appendChild(infobox);
                            newdiv.appendChild(button);
                            newdiv.appendChild(logbox);
                            coreS.appendChild(newdiv);


                        } else {
                            //блок найден, обновляю данные
                            let infobox = document.getElementById(res.worker.ids + "infobox");
                            let text = document.getElementById(res.worker.ids + "text");
                            infobox.innerHTML = "";
                            infobox.innerHTML = "=> " + res.worker.node + " run: " + res.worker.time_run;
                            text.value += "\n";
                            text.value += res.worker.result + " " + res.worker.error + " " + res.worker.timerun;
                        }
                    }
                    break;

                //обработка сообщений от рабочих
                case "multinode":
                    console.log("Get result from worker `multinode` -- ", res.worker);
                    break;

                //постоянная проверка, с заданной периодичностью
                case "end":
                    //изменяю статус кнопки и делаю ее доступной
                    let bb = document.getElementById(btn);
                    bb.disabled = false;
                    bb.innerText = "проверить сервера";
                    //изменяю статус кнопки останов
                    let b = document.getElementById(btnStop);
                    b.disabled = true;

                    console.log("проверка закончена ", res);

                    //добавляю инфу о количестве проверенных серверов
                    document.getElementById(checked).innerText = res.client.count_check;

                    break;
                default:
                    console.log("wrong  res.client.command", res.client.command);
                    break;
            }
        };

        //соединеие закрыто
        this.socket.onclose = function (e) {
            if (e.wasClean) {
                console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
            } else {
                console.log('[socket] соединение закрыто со стороны сервера ', e.code);
            }
            document.getElementById(connIcon).classList.add("error_red");
        };

        //ошибка
        this.socket.onerror = function (e) {
            console.log(`[socket] websocket error ${e.message}`);
        };

    }


    //обновление таймеров (btn) id = `updateTimers`
    updateTimersMultiCheck(t) {
        let timeoutTCP = document.getElementById('timeoutTCP');
        let timeoutCTX = document.getElementById('timeoutCTX');
        let timeoutTCPZBD = document.getElementById('timeoutTCPZBD');
        let timeoutCTXZBD = document.getElementById('timeoutCTXZBD');
        let startStruct = {
            Status: "enable",
            Command: "update_timers",
            TimerCTX: parseInt(timeoutCTX.value),
            TimerTCP: parseInt(timeoutTCP.value),
            TimerCTXZBD: parseInt(timeoutCTXZBD.value),
            TimerTCPZBD: parseInt(timeoutTCPZBD.value),
        };
        console.log("update-timers->server: ", startStruct);
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(startStruct));
    }


    //вешать на кнопку старт - запуск постоянных проверок по заданному периоду
    startEvent(t) {
        //получаю кнопку-останов
        let bb = document.getElementById("stopMultiCheck");

        //получаю значение поля таймера для проверки
        let counter = document.getElementById('timervalue');

        //добавляю инфу о количестве проверенных серверов
        document.getElementById("countServers").innerText = 0;

        //получаю все отмеченные чекбоксы по исключенным серверам
        let listTotal = document.getElementsByClassName('checkboxer');
        let stocker = [];
        let countChecker = 0;

        console.log('ListTotal: ', listTotal);
        for (let x = 0; x < listTotal.length; x++) {
            let elem = {
                IDS: listTotal[x].id,
                Status: listTotal[x].checked,
            };
            if (!listTotal[x].checked) {
                countChecker++;
            }
            stocker.push(elem);
        }

        //изменяю количество серверов, которые нужно проверять
        let countBox = document.getElementById('countServers');
        countBox.innerText = countChecker;

        console.log('STOCKER: ', stocker, "\ncountCheked: ", countChecker);


        let startStruct = {
            Status: "enable",
            Command: "start",
            Timer: counter.value,
            Stocker: stocker,
        };

        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(startStruct));

        //получаю кнопку для изменения
        t.innerText = "в работе...";
        t.disabled = true;

        //активирую кнопку-останов
        bb.disabled = false;
    }

    //обработчик на открытие панели для исключения серверов
    showListServers(e) {
        //получаю бокс со списком серверов
        let box = document.getElementById('listcheck');
        if (box.style.display === 'none') {
            box.style.display = 'block';
            //изменяю окраску
            e.style.backgroundColor = 'aqua';
        } else {
            //изменяю окраску
            e.style.backgroundColor = 'white';
            box.style.display = 'none';
        }
    }


    //кнопка останов
    stopEvent(e) {
        console.log("Pressed stop even multicheck servers");
        let bb = document.getElementById('stopMultiCheck');
        //проверка рабочей версии - одиночной или по таймеру
        let btnStart = document.getElementById('startMultiCheck');
        btnStart.disabled = false;
        btnStart.innerText = "запустить проверку с периодичностью по таймеру";
        bb.disabled = true;

        let ss = {
            Status: "enable",
            Command: "stop"
        };

        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(ss));
    }
}


/////////////////////////////////////////////////////////
//WebsocketTimerSingleconfig:: запуск по параметрам воркеров c тиком проверки
///////////////////////////////////////////////////////////
class WebsocketTimerSingleconfig {
    constructor(idselect, idblock, core, connIcon, serverAddr) {
        this.idselect = idselect;
        this.idblock = idblock;
        this.core = core;
        this.icon = connIcon;
        this.connectIcon = document.getElementById(connIcon);
        this.socket = new WebSocket(serverAddr);

        //OPEN->WS
        this.socket.onopen = function (e) {
            console.log('[socket] connection established');
            console.log('[socket] send data to server');
            document.getElementById(connIcon).classList.add("error_green");
        };

        //READ->WS
        this.socket.onmessage = function (e) {
            //парсинг блока данных
            let res = JSON.parse(e.data);
            console.log("------> ", res);
            console.log("------> ", res.client.list_keys);
            //добавляю ключи активных воркеров
            let ids = document.getElementById(idselect);
            if (res.client.list_keys != null) {
                for (let i = 0; i < res.client.list_keys.length; i++) {
                    let opt = document.createElement('option');
                    opt.value = res.client.list_keys[i];
                    opt.innerHTML = res.client.list_keys[i];
                    ids.appendChild(opt);
                }
            } else {
                console.log("List keys is empty\n");
            }

            //выстраиваю логическое дерево
            switch (res.client.command) {
                //установки информационного значения
                case "infoset":
                    let block = document.getElementById(idblock);
                    block.innerText = "Количество работающих воркеров: " + res.client.nodes;
                    console.log("FROM CLIENT: ", res);
                    break;
            }
        };

        //соединеие закрыто
        this.socket.onclose = function (e) {
            if (e.wasClean) {
                console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
            } else {
                console.log('[socket] соединение закрыто со стороны сервера ', e.code);
            }
            document.getElementById(connIcon).classList.add("error_red");
        };

        //ошибка
        this.socket.onerror = function (e) {
            console.log(`[socket] websocket error ${e.message}`);
        };

    }

    //кнопка прибития активного воркера
    KillWorker(e) {
        let select = document.getElementById(this.idselect);
        console.log('Selected: ', select.value);
        let MsgServer = {
            Command: "kill",
            Marked: select.value,
        };
        console.log("MSgServer: ", MsgServer);
        this.socket.send(JSON.stringify(MsgServer));
    }


    //кнопка отправки данных на сервер из формы создания обработчика
    SendForm(e) {
        //get form
        let cform = document.forms.noda;
        console.log("Form: ", cform);
        //make msg to server
        let MsgServer = {
            Command: "new",
            Dbs: cform.elements.db.value,
            Period: cform.elements.period.value,
            Timeout: cform.elements.timeout.value,
        };
        console.log("MSgServer: ", MsgServer);
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(MsgServer));
    }
}

/////////////////////////////////////////////////////////
//WebsocketTimerSingleconfig:: отображение результатов проверки по таймеру
///////////////////////////////////////////////////////////
class WebsocketTimerSingleResult {
    constructor(tresult, idblock, core, connIcon, serverAddr) {
        this.tresult = tresult;
        this.idblock = idblock;
        this.core = core;
        this.icon = connIcon;
        this.connectIcon = document.getElementById(connIcon);
        this.socket = new WebSocket(serverAddr);

        //OPEN->WS
        this.socket.onopen = function (e) {
            console.log('[socket] connection established');
            console.log('[socket] send data to server');
            document.getElementById(connIcon).classList.add("error_green");
            console.log("[opensocket] idblock:", e);
        };


        //READ->WS
        this.socket.onmessage = function (e) {
            //парсинг блока данных
            let res = JSON.parse(e.data);
            let info = document.getElementById(idblock);
            console.log("RES -> ", res.stock);
            info.innerText = "Количество работающих воркеров: " + res.workers;

            //очищаю таблицу
            let table = document.getElementById(tresult);
            table.innerHTML = "";

            //формирую выдачу
            for (let i = 0; i < res.stock.length; i++) {
                let ee = res.stock[i];
                let tr = document.createElement('tr');
                //mark
                let th = document.createElement('th');
                th.innerText = ee.mark;
                tr.appendChild(th);
                //node
                let td1 = document.createElement('td');
                td1.innerHTML = ee.node;
                tr.appendChild(td1);
                //timeout
                let td2 = document.createElement('td');
                td2.innerHTML = ee.timeout;
                tr.appendChild(td2);
                //period
                let td3 = document.createElement('td');
                td3.innerHTML = ee.period;
                tr.appendChild(td3);

                //статус
                let td5 = document.createElement('td');
                if (ee.worker_packet.length > 0) {
                    td5.innerHTML = "недоступен";
                } else {
                    td5.innerHTML = "ок";
                }
                tr.appendChild(td5);

                //status check
                if (ee.worker_packet.length > 0) {
                    let td4 = document.createElement('td');
                    td4.innerHTML = ee.worker_packet[0].error;
                    tr.appendChild(td4);
                } else {
                    let td4 = document.createElement('td');
                    td4.innerHTML = "-";
                    tr.appendChild(td4);
                }


                table.appendChild(tr);
            }
        };

        //соединеие закрыто
        this.socket.onclose = function (e) {
            if (e.wasClean) {
                console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
            } else {
                console.log('[socket] соединение закрыто со стороны сервера ', e.code);
            }
            document.getElementById(connIcon).classList.add("error_red");
        };

        //ошибка
        this.socket.onerror = function (e) {
            console.log(`[socket] websocket error ${e.message}`);
        };

    }

    //кнопка отправки данных на сервер из формы создания обработчика
    SendForm(e) {
        //get form
        let cform = document.forms.noda;
        console.log("Form: ", cform);
        //make msg to server
        let MsgServer = {
            Command: "new",
            Dbs: cform.elements.db.value,
            Period: cform.elements.period.value,
            Timeout: cform.elements.timeout.value,
        };
        console.log("MSgServer: ", MsgServer);
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(MsgServer));
    }
}

function GetData(gc) {
    let MsgServer = {
        Command: "getinfo",
        Status: "",
    };
    console.log("MSgServer: ", MsgServer);
    //отправляю на сервер команду на запуск проверки
    gc.socket.send(JSON.stringify(MsgServer));
}

/////////////////////////////////////////////////////////
//WebsocketBackupSingle:: сокет для коммуникации проверка бэкапов
///////////////////////////////////////////////////////////
class WebsockeBackupSingle {
    constructor(resultbox, profilebox, infobox, connIcon, serverAddr) {
        this.resultbox = resultbox;
        this.profilebox = profilebox;
        this.infobox = infobox;
        this.icon = connIcon;
        this.connectIcon = document.getElementById(connIcon);
        this.socket = new WebSocket(serverAddr);
        this.flagResultShow = false;
        this.flagResultClear = false;


        //OPEN->WS
        this.socket.onopen = function (e) {
            console.log('[socket] connection established');
            console.log('[socket] send data to server');
            document.getElementById(connIcon).classList.add("error_green");
        };

        //READ->WS
        this.socket.onmessage = function (e) {
            console.log("THIS STATUS : ", e);
            //парсинг блока данных
            let res = JSON.parse(e.data);
            console.log("------> ", res);
            //выстраиваю логическое дерево
            switch (res.command) {
                //установки информационного значения
                case "infoset":
                    let block = document.getElementById(infobox);
                    block.innerText = "Директория с бэкапами: " + res.work_dir + "\nФайлотчет: "
                        + res.report_file + "\n" + "LogFile: "
                        + res.log_file + "\nDebug: "
                        + res.debug + "\nProduction: "
                        + res.production
                        + "\nПуть к дампу конфига: " + res.dump_file
                        + "\nКоличество баз из filials: " + res.count_database;
                    console.log("FROM CLIENT: ", res);
                    break;
                case "result":

                    let res1 = document.getElementById(resultbox);
                    res1.innerText = "";

                    let countDBS = document.getElementById("filtercount");

                    console.log("FILTER RESULT: " + res.filter_result);
                    if (res.filter_result === true) {
                        countDBS.innerText= res.filter_count_db;
                    } else {
                        countDBS.innerText = "-";
                    }


                    console.log("[command-result] Result: ", res.result);
                    let dd = new Date();
                    res1.innerHTML += dd + "\n <br>";

                    for (let i in res.result) {
                        console.log("Key " + i + res.result[i].Filid);
                        let checkValue = null;
                        if (res.result[i].TimeExpired) {
                            checkValue = `<button class='button is-warning'> FAIL </button>`;
                        } else if (res.result[i].NoFiles) {
                            checkValue = `<button class='button is-danger'> ERROR </button>`;
                        } else {
                            checkValue = `<button class='button is-success'> OK </button>`;
                        }
                        let profileCheck = null;
                        let per = null;
                        profileCheck = '[A]';
                        per = res.result[i].Period;
                        // добавление ввода правки + кнопки отправку значения на сервер
                        // let correct = document.createElement('input');
                        // correct.type = "text";
                        // correct.id = "filid" + res.result[i].Filid;
                        let lastfile = ""
                        if (res.result[i].FileLast !== null) {
                            lastfile = "Бэкап крайний по метке: " + res.result[i].FileLast.Name + " : размер=" + res.result[i].FileLast.Sizer + " тип=" + res.result[i].FileLast.Types + "<br>"
                        }

                        let ids = 'filid' + res.result[i].Filid;
                        res1.innerHTML += "<div class=box>"
                            // + res.result[i].CheckMsg + "<br>"
                            // + profileCheck + " Префикс бд:" + res.result[i].Prefix + "<br>"
                            + "БД: " + res.result[i].Filid + "<br>"
                            + lastfile
                            + "Период: " + per + "<br>"
                            + "Бэкап: " + checkValue
                            + "<br>"
                            + "Исключения: " + res.result[i].ErrorPeriod + "<br>"
                            + "\n " + "Коррекция в часах:" + "<input type='number'  id=" + ids + " value=" + res.result[i].Period + ">"
                            + "</div>";

                    }
                    break;
            }
        };

        //соединеие закрыто
        this.socket.onclose = function (e) {
            if (e.wasClean) {
                console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
            } else {
                console.log('[socket] соединение закрыто со стороны сервера ', e.code);
            }
            document.getElementById(connIcon).classList.add("error_red");
        };

        //ошибка
        this.socket.onerror = function (e) {
            console.log(`[socket] websocket error ${e.message}`);
        };

    }

    //
    //кнопка старта процесса проверки
    RunCheck(e) {
        let b = document.getElementById(this.resultbox);
        b.innerText = "";

        let MsgServer = {
            Command: "check",
        };
        console.log("MSgServer: ", MsgServer);
        this.socket.send(JSON.stringify(MsgServer));
    }

    //очистить результирующее окно
    ClearResultWindow(e) {
        let b = document.getElementById(this.resultbox);
        b.innerText = "";
    }

    //триггер при изменении состояние окна по выводу результата
    ChangeStatusResultBox(e) {
        console.log("[ChangeStatusResultBox] Status changed..." + e.text + "\n");
    }


    UpdatePeriods(e) {
        //соберу данные по всем inputs
        let lister = document.getElementById('resultbox')

        const result = [...lister.querySelectorAll('input')].reduce((acc, item) => (
            acc.push({
                name: item.id,
                value: item.value
            }), acc), []);

        let MsgServer = {
            Command: "period",
            Result: result,
        };
        console.log("MSgServer: ", MsgServer);
        this.socket.send(JSON.stringify(MsgServer));

    }

    //filter for ouput list result
    FilterOK(e) {
        console.log("Status Checkbox: ", e.checked);
        let MsgServer = {
            Command: "filter",
            Filter: e.checked,
        };
        console.log("[filterOK] : ", MsgServer);
        this.socket.send(JSON.stringify(MsgServer));
    }


    // //кнопка отправки данных на сервер из формы создания обработчика
    // SendForm(e) {
    //     //get form
    //     let cform = document.forms.noda;
    //     console.log("Form: ", cform);
    //     //make msg to server
    //     let MsgServer = {
    //         Command: "new",
    //         Dbs: cform.elements.db.value,
    //         Period: cform.elements.period.value,
    //         Timeout: cform.elements.timeout.value,
    //     };
    //     console.log("MSgServer: ", MsgServer);
    //     //отправляю на сервер команду на запуск проверки
    //     this.socket.send(JSON.stringify(MsgServer));
    // }
}




/////////////////////////////////////////////////////////
//WebsocketTEster для тестирования  работы вебсокета с множеством
///////////////////////////////////////////////////////////
class WebsocketTester {
    constructor(resultbox, profilebox, infobox, connIcon, serverAddr) {
        this.resultbox = resultbox;
        this.profilebox = profilebox;
        this.infobox = infobox;
        this.icon = connIcon;
        this.connectIcon = document.getElementById(connIcon);
        this.socket = new WebSocket(serverAddr);
        this.flagResultShow = false;
        this.flagResultClear = false;

        //OPEN->WS
        this.socket.onopen = function (e) {
            console.log('[socket] connection established');
            console.log('[socket] send data to server');
            document.getElementById(connIcon).classList.add("error_green");
        };

        //READ->WS
        this.socket.onmessage = function (e) {
            console.log("THIS STATUS : ", e);
            //парсинг блока данных
            let res = JSON.parse(e.data);
            console.log("------> ", res);
            console.log("[COMMAND] ------> ", res.command);
            //выстраиваю логическое дерево
            switch (res.command) {
                //установки информационного значения
                case "infoset":
                    console.log("FROM CLIENT: ", res);
                    break;
                case "remd":
                    console.log("=== REMD: ", res);
                    break;
                case "imek":
                    console.log("=== IMEK: ", res);
                    let page = document.getElementById("mainer");
                    page.classList.remove('cur_wait');
                    page.classList.add('cur_normal');
                    // page.style.cursor = "default";
                    alert("Answer: " + res);
                    break;
            }
        };

        //соединеие закрыто
        this.socket.onclose = function (e) {
            if (e.wasClean) {
                console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
            } else {
                console.log('[socket] соединение закрыто со стороны сервера ', e.code);
            }
            document.getElementById(connIcon).classList.add("error_red");
        };

        //ошибка
        this.socket.onerror = function (e) {
            console.log(`[socket] websocket error ${e.message}`);
        };
    }

    //send to server packet
    SendREMD(e) {
        let MsgServer = {
            Command: "remd",
        };
        console.log("MSgServer: ", MsgServer);
        this.socket.send(JSON.stringify(MsgServer));
    }

    //send to server packet
    SendIMEK(e) {
        //изменяю курсор мыши
        let page = document.getElementById("mainer");
        // page.style.cursor = "wait";
        page.classList.remove('cur_normal');
        page.classList.add('cur_wait');
        // page.style.cursor = "progress";
        // page.cursor.style = "color:red;";
        // let MsgServer = {
        //     Command: "imek",
        // };
        // console.log("MSgServer: ", MsgServer);
        // this.socket.send(JSON.stringify(MsgServer));
    }

    //functions for  tabs and more
    OpenTab(tabID, dataClassName) {
        let stock = document.getElementsByClassName(dataClassName);
        for (let i = 0; i < length(stock); i++) {
            stock[i].style.display = 'none';
        }
        document.getElementById(tabID).style.display = 'block';
    }
}




document.onreadystatechange = function () {
    if (document.readyState === "complete") {
        console.log("===DOM READY===");

        hashCode = s => s.split('').reduce((a, b) => {
            a = ((a << 5) - a) + b.charCodeAt(0);
            return a & a
        }, 0)
        console.log("Hashcode: ", "id" + hashCode("sysdba:sysdba@localhost:3055/employee"));
        //url == dbs single check
        if (window.location.href === "http://localhost:4001/checkdbsingle") {


            //создаю класс по вебсокетам при условии полной загрузки DOM
            //---------------------------------------------------------------------
            // infobd = общее количество серверов для проверки, взятых из конфига
            // list = список проблемных серверов
            // checked = количестве проверенных серверов
            // single - опрос одиночный если включено
            //---------------------------------------------------------------------
            // let  gconnect = new SocketTester('infobd', 'list', 'checked', 'single', 'btn', 'stop');
            //  console.log("--> http://localhost:4001/checkdbsingle :: connector ready: ", gconnect);
        }
        ;
        // setInterval(AJAX.updateOnTimerBlock('hash', 'block3'), 1000);
    }
};


class WS {
    constructor() {
        this.socket = new WebSocket("ws://localhost:4001/ws");

        //event->open::ws
        this.socket.onopen = function (e) {
            console.log('[socket] connection established');
            console.log('[socket] send data to server');
        };

        //event->read::ws
        this.socket.onmessage = function (e) {

            //парсинг блока данных
            let res = JSON.parse(e.data);
            console.log(res);
        };

        //соединеие закрыто
        this.socket.onclose = function (e) {
            if (e.wasClean) {
                console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
            } else {
                console.log('[socket] соединение закрыто со стороны сервера ', e.code);
            }
        };

        //ошибка
        this.socket.onerror = function (e) {
            console.log(`[socket] websocket error ${e.message}`);
        };
    }
}


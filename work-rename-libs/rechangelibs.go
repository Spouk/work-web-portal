//----------------------------------------------------------------------
// алгоритм 2 аргумента 1дир 2дир
// из первой надо скопировать файлы с заменой во вторую
// составляется список файлов источника ()
// делается проход по списку файлов и ищется аналогичный файл в приемнике (2)
// если находит то в приемнике изменяет название (названиефайоа-save)
// и копирует файл из приемника , если файла не находит то просто копирует в приемник
//-----------------------------------------------------------------------

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func main() {
	newDir := flag.String("newdir", "", "директория источник")
	oldDir := flag.String("olddir", "", "директория приемник")
	flag.Parse()

	if *newDir == "" || *oldDir == "" {
		flag.PrintDefaults()
		return

	}
	r := Renamer{}
	err := r.ChangeLinearDir(*newDir, *oldDir)
	if err != nil {
		log.Fatal(err)
	}
}

type Renamer struct{}

//замена НЕ вложенных директрий - список файла в одной директории
func (r *Renamer) ChangeLinearDir(indir, outdir string) error {
	listF, err := ioutil.ReadDir(indir)
	if err != nil {
		return err
	}
	listOut, err := ioutil.ReadDir(outdir)
	if err != nil {
		return err
	}
	for _, f := range listF {
		//check exists file in OldDir
		if r.checkExist(listOut, f.Name()) {
			//file exists
			for _, out := range listOut {
				if f.IsDir() == false && f.Name() == out.Name() {
					//make new name file
					fpOut := strings.Join([]string{outdir, out.Name()}, "/")
					fpNew := strings.Join([]string{out.Name(), "old"}, "-")
					fmt.Printf("file renaming ... %v\n", fpOut)
					//rename file old
					err := os.Rename(fpOut, strings.Join([]string{outdir, fpNew}, "/"))
					if err != nil {
						log.Printf("[error] %v\n", err)
					}
					//make full path new file
					newFile := strings.Join([]string{indir, f.Name()}, "/")
					newFileWithDir := strings.Join([]string{outdir, f.Name()}, "/")
					//copy file new
					input, err := ioutil.ReadFile(newFile)
					if err != nil {
						log.Printf("[error] %v\n", err)
					} else {
						err = ioutil.WriteFile(newFileWithDir, input, os.ModePerm)
						if err != nil {
							log.Printf("[error] %v\n", err)
						}
					}
				}
			}
		} else {
			//file not found in new dir, then simpy copy to dir
			//make full path new file
			newFile := strings.Join([]string{indir, f.Name()}, "/")
			newFileWithDir := strings.Join([]string{outdir, f.Name()}, "/")
			//copy file new
			input, err := ioutil.ReadFile(newFile)
			if err != nil {
				log.Printf("[error] %v\n", err)
			} else {
				err = ioutil.WriteFile(newFileWithDir, input, os.ModePerm)
				if err != nil {
					log.Printf("[error] %v\n", err)
				}
			}
		}
	}
	return nil
}
func (r *Renamer) checkExist(stock []os.FileInfo, nameFile string) bool {
	for _, f := range stock {
		if f.Name() == nameFile {
			return true
		}
	}
	return false
}

package library

import (
	"database/sql"
	"fmt"
	_ "github.com/nakagami/firebirdsql"
	"time"
)

//----------------------------------------------------------------------
// REMD + IMEK чистая передача без вебсокета
//-----------------------------------------------------------------------
func (s *Server) WorkerIMEK_Single() []ResultImek {
	res := InfoFunctionTime{
		Service:    "iemk",
		TimeRun:    time.Now().Format(time.RFC3339),
		TimeSQLRun: "",
	}

	start := time.Now()
	uAll := []ResultImek{}
	//делаю запрос к базе
	conn, err := sql.Open("firebirdsql", s.cfg.DbsserviceDB)
	if err != nil {
		s.log.Printf("%s %s\n", "[ERROR]", "ошибка при создании подключения к базе данных")
	} else {
		req := fmt.Sprintf(s.cfg.IemkSqlRequest)
		rows, err := conn.Query(req)
		if err != nil {
			s.log.Printf("[error-dbs-query] %v\n", err)
		} else {
			for rows.Next() {
				var u ResultImek
				err = rows.Scan(&u.Filial, &u.Fullname, &u.Pcode, &u.TreatCode, &u.TreatDate, &u.ResponseDate)
				if err != nil {
					s.log.Printf("[error-dbs-query] %v\n", err)
					continue
				}
				uAll = append(uAll, u)
			}
		}
	}
	time.Sleep(time.Second * 4)
	end := time.Since(start).String()
	res.TimeSQLRun = end

	//добавляю запись в учет проверок
	if err := s.FifoQueue.Add(res); err != nil {
		s.log.Printf("[iemk][error]  %v\n", err)
	}

	s.log.Printf("===TIMERS RUN: `%v` : `%v` : `%v`\n", res.Service, res.TimeRun, res.TimeSQLRun)
	return uAll

}

// REMD SINGLE
func (s *Server) WorkerREMD_Single() []ResultRemd {
	res := InfoFunctionTime{
		Service:    "remd",
		TimeRun:    time.Now().Format(time.RFC3339),
		TimeSQLRun: "",
	}

	start := time.Now()
	uAll := []ResultRemd{}
	//делаю запрос к базе
	conn, err := sql.Open("firebirdsql", s.cfg.DbsserviceDB)
	if err != nil {
		s.log.Printf("%s %s\n", "[ERROR]", "ошибка при создании подключения к базе данных")
	} else {
		req := fmt.Sprintf(s.cfg.RemdSqlRequest)
		rows, err := conn.Query(req)
		if err != nil {
			s.log.Printf("[error-dbs-query] %v\n", err)
		} else {
			for rows.Next() {
				var u ResultRemd
				err = rows.Scan(&u.Filial, &u.LastTime, &u.ErrorAccess)
				if err != nil {
					s.log.Printf("[error-dbs-query] %v\n", err)
					continue
				}
				uAll = append(uAll, u)
			}
		}
	}
	time.Sleep(time.Second * 4)
	end := time.Since(start).String()
	res.TimeSQLRun = end

	//добавляю запись в учет проверок
	if err := s.FifoQueue.Add(res); err != nil {
		s.log.Printf("[iemk][error]  %v\n", err)
	}
	s.log.Printf("===TIMERS RUN: `%v` : `%v` : `%v`\n", res.Service, res.TimeRun, res.TimeSQLRun)
	return uAll
}

//----------------------------------------------------------------------
// REMD + IEMK Workers
//-----------------------------------------------------------------------
func (s *Server) WorkerIMEK(toWrite chan interface{}) {
	t := BaseMSG{
		Command: "imek",
		Body:    "simple body",
	}
	//небольшая пауза
	s.log.Println("---------- start sleeping")
	time.Sleep(time.Second * 5)
	s.log.Println("---------- end sleeping")

	//добавляю запись в учет проверок
	if err := s.FifoQueue.Add(fmt.Sprintf("[iemk] %s", time.Now().Format(time.RFC3339))); err != nil {
		s.log.Printf("[iemk][error]  %v\n", err)
	}

	toWrite <- t
	s.log.Println("---------- message to client sending")

}
func (s *Server) WorkerREMD(toWrite chan interface{}) {
	//структура по результатам добавляется в фифу
	t := BaseMSG{
		Command: "remd",
		Body:    "simple body",
	}
	////делаю запрос к базе
	//conn, err := sql.Open("firebirdsql", s.cfg.DbsserviceDB)
	//if err != nil {
	//	s.log.Printf("%s %s\n", "[ERROR]", "ошибка при создании подключения к базе данных")
	//} else {
	//	uAll  := []Users{}
	//	rows, err := conn.Query(s.cfg.RemdSqlRequest, "01.01.2021", "03.02.2021")
	//	if err != nil {
	//		s.log.Printf("[error-dbs-query] %v\n", err)
	//	} else {
	//		for rows.Next() {
	//			var u Users
	//			err =  rows.Scan(&u.ID, &u.Name, &u.Maked)
	//			if err != nil {
	//				s.log.Printf("[error-dbs-query] %v\n", err)
	//				continue
	//			}
	//			uAll = append(uAll, u)
	//		}
	//	}
	//
	//
	//}

	//добавляю запись в учет проверок
	if err := s.FifoQueue.Add(fmt.Sprintf("[remd] %s", time.Now().Format(time.RFC3339))); err != nil {
		s.log.Printf("[remd][error]  %v\n", err)
	}

	//отдаю результат на морду
	toWrite <- t

	s.log.Println("---------- message to client sending")

}

//*******************************************************************
// user CRUD
//*******************************************************************
type userinfo struct {
	fio      string
	filid    []int //where need make user account
	conftype string
}

//func (s *Server) userCRUD(crud string, uf userinfo) error {
//
//	//make connector to firebird database
//	conn, err := sql.Open("firebirdsql", s.cfg.DbsserviceDB)
//	if err != nil {
//		s.log.Printf("%s %s\n", "[ERROR]", "ошибка при создании подключения к базе данных")
//		return err
//	} else {
//		req := fmt.Sprintf(s.cfg.IemkSqlRequest)
//		rows, err := conn.Query(req)
//		if err != nil {
//			s.log.Printf("[error-dbs-query] %v\n", err)
//		} else {
//			for rows.Next() {
//				var u ResultImek
//				err = rows.Scan(&u.Filial, &u.Fullname, &u.Pcode, &u.TreatCode, &u.TreatDate, &u.ResponseDate)
//				if err != nil {
//					s.log.Printf("[error-dbs-query] %v\n", err)
//					continue
//				}
//				uAll = append(uAll, u)
//			}
//		}
//	}
//}

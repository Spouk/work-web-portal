package library

import (
	"context"
	"crypto/md5"
	"database/sql"
	"fmt"
	"github.com/gorilla/websocket"
	_ "github.com/nakagami/firebirdsql"
	"log"
	"net"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	//messages
	prefixConsole         = "[work-dbs-checker] "
	errorReadConfigFile   = "ошибка чтения конфигурационного файла"
	infoWorkerStart       = "worker #%d в работе по dsn = `%s`"
	infoWorkerEnd         = "worker #%d закончил работу по dsn = `%s`"
	infoWorkerCheckStatus = "....[ %s ]"
)

//структура для ошибок
// type error : 1 - tcp, 2 - firebird
type Dbscheckerror struct {
	TypeError int    // тип ошибки
	ErrorMsg  string // тело ошибки
}

func (d *Dbscheckerror) MakeErr(tcode int, msgErr string) *Dbscheckerror {
	d.TypeError = tcode
	d.ErrorMsg = msgErr
	return d
}

type WorkDBSChecker struct {
	Log *log.Logger
	C   *ConfigStruct
	wg  *sync.WaitGroup
}

//создание инстанса тулзы
func NewDBSChecker(c *ConfigStruct) *WorkDBSChecker {
	//создаю новый инстанс
	ins := &WorkDBSChecker{
		Log: log.New(os.Stdout, prefixConsole, log.LstdFlags),
		C:   c,
		wg:  &sync.WaitGroup{},
	}
	//возвращаю инстанс
	return ins
}

//func (w *WorkDBSChecker) RunChecker() {
//	w.managerWorker()
//}

//запуск консольного менеджера горутин
//берет значения из C.NodeList
//и по каждому значению запускает отдельную горутину
//для взаимодействия создает 1 канал - для передачи горутинам команд
//алгоритм в целом такой: запускается менеджер, по количеству элементов в списке запускает горутин
// длина nodeList = количеству работающих горутин,
//в каждой горутине в цикле устанавливается тикер который берется из конфига и с заданной периодичностью
//делает нужное(ые) действия с нодой - опрашивает, получает/неполучает ответ, анализирует, выводит результат
//анализа в os.stdout, после уходит на очередую итерацию,
//при получении команды из канала от менеджера, обрабатывает ее (это вероятнее всего будет либо завершение работы)
//!!! менеджер нужен только в версии, когда используется WEB морда, тогда основной тред будет висеть за http-сервером,
//	  а менеджер будет работать как горутина, отличие будут в запуске
//    в консольной как запуск функции, с ВЕБмордой как горутина
//func (w *WorkDBSChecker) managerWorker() {
//	//создаю канал для передачи команд
//	ch := make(chan string)
//	//запускаю горутины по спику нод
//	for x := 0; x < len(w.C.NodeList); x++ {
//		w.wg.Add(1)
//		go w.worker(ch, w.C.NodeList[x], x)
//	}
//	w.wg.Wait()
//}

//реализация горутины-рабочего
func (w *WorkDBSChecker) worker(chCommand chan string, node string, workerNumber int, chanEnd chan bool) {
	w.Log.Printf(fmt.Sprintf(infoWorkerStart, workerNumber, node))
	defer func() {
		w.wg.Done()
	}()
	ticker := time.NewTicker(time.Duration(w.C.NodeTicker) * time.Minute)
	for {
		select {
		//канал от менеджера
		case s := <-chCommand:
			switch s {
			case "quit":
				return
			}
			//ждем события и запускаем проверку сервера
		case <-ticker.C:
			var (
				nerr = &Dbscheckerror{}
			)
			//проверка по доступности tcp
			host := strings.Split(strings.Split(strings.Split(node, "@")[1], "/")[0], ":")
			_, err := net.DialTimeout("tcp", strings.Join(host, ":"), time.Second*time.Duration(w.C.TimeoutTCP))
			if err != nil {
				w.Log.Println(nerr.MakeErr(1, err.Error()))
			}
			//создаю коннектор к базе данных
			conn, err := sql.Open("firebirdsql", node)
			if err != nil {
				w.Log.Printf("%s %s\n", "[ERROR]", "ошибка при создании подключения к базе данных")
			} else {

				ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(w.C.TimeoutCTX))
				defer cancel()

				var n int
				conn.QueryRowContext(ctx, "select  first 1 MON$SERVER_PID  from MON$ATTACHMENTS;").Scan(&n)
				if n > 0 {
					//база доступна
					w.Log.Printf("%s %s\n", "[OK]", node)
				} else {
					//база недоступна
					w.Log.Printf("%s %s\n", "[FAIL]", node)

				}
			}
		}
	}
}

// реализация воркера с привязкой к структуре сервера
// chanEnd - канал оповщеение о прекращении работы
// chanInfo - канал передачи информации о проверке
// node - сервер, который надо проверкить воркером
// workerNumber - номер воркера
// repeatCheck - флаг, выставляющий повторение проверки сервера, если выставлен то будет цикл-  проверка
// при отсуствии флага, будет одиночная проверка
//func (s *Server) workerCheckDBS(node string, workerNumber int, chanEnd chan bool, chanInfo chan DBWorkerPacket, repeatCheck bool, timeoutSecond int) {
//	s.log.Printf(fmt.Sprintf(infoWorkerStart, workerNumber, node))
//	defer func() {
//		s.log.Printf(fmt.Sprintf(infoWorkerStart, workerNumber, node))
//		chanEnd <- true
//	}()
//
//	////время паузы исскуственно для проверки возврата значений
//	//time.Sleep(time.Second * time.Duration(workerNumber))
//
//	//одиночный запрос
//	if repeatCheck == false {
//		err, res, timeRun := s.checkServer(node)
//		if err != nil || res == false {
//			//сервер не доступен по порту (порт закрыт, или ошибка создания хэндлера)
//			chanInfo <- DBWorkerPacket{
//				IDS:        fmt.Sprintf("worker#%d", workerNumber),
//				Node:       node,
//				Result:     false,
//				Error:      err.Error(),
//				StatusWork: "single",
//				TimeRun:    timeRun,
//			}
//		} else {
//			if res {
//				//сервер доступен
//				chanInfo <- DBWorkerPacket{
//					IDS:        fmt.Sprintf("worker#%d", workerNumber),
//					Node:       node,
//					Result:     true,
//					Error:      "",
//					StatusWork: "single",
//					TimeRun:    timeRun,
//				}
//			}
//		}
//		return
//	}
//
//	//циклично по таймеру чекать ноду
//	if repeatCheck {
//		ticker := time.NewTicker(time.Duration(s.cfg.NodeTicker) * time.Minute)
//		for {
//			select {
//			//команда о завершении воркера
//			case <-chanEnd:
//				s.log.Printf("worker#%d the end\n", workerNumber)
//				return
//			//ждем события и запускаем проверку сервера
//			case <-ticker.C:
//				err, res, timeRun := s.checkServer(node)
//				if err != nil || res == false {
//					//сервер не доступен по порту (порт закрыт, или ошибка создания хэндлера)
//					chanInfo <- DBWorkerPacket{
//						IDS:        fmt.Sprintf("worker#%d", workerNumber),
//						Node:       node,
//						Result:     false,
//						Error:      err.Error(),
//						StatusWork: "repeat",
//						TimeRun:    timeRun,
//					}
//				} else {
//					if res {
//						//сервер доступен
//						chanInfo <- DBWorkerPacket{
//							IDS:        fmt.Sprintf("worker#%d", workerNumber),
//							Node:       node,
//							Result:     true,
//							Error:      "",
//							StatusWork: "repeat",
//							TimeRun:    timeRun,
//						}
//					}
//				}
//			}
//		}
//	}
//}

//------------------------------------------------------------------------------------------
//проверка БД по порту
//------------------------------------------------------------------------------------------
func (s *Server) checkServerTCP(node string, timerTCP int) (error, bool) {
	//проверка tcp хоста на доступность с таймером
	host := strings.Split(strings.Split(strings.Split(node, "@")[1], "/")[0], ":")

	//_, err := net.DialTimeout("tcp", strings.Join(host, ":"), time.Second*time.Duration(timeoutSecond))
	_, err := net.DialTimeout("tcp", strings.Join(host, ":"), time.Second*time.Duration(timerTCP))
	if err != nil {
		return err, false
	}
	return nil, true
}

//функция для проверки хоста на доступность по нодам формата "sysdba:sysdba@192.168.122.66:3050/employee"
func (s *Server) checkHostTimeout(node string, timeOut int) error {
	host := strings.Split(strings.Split(strings.Split(node, "@")[1], "/")[0], ":")
	_, err := net.DialTimeout("tcp", strings.Join(host, ":"), time.Second*time.Duration(timeOut))
	if err != nil {
		return err
	}
	return nil
}

//----------------------------------------------------------------------
// проверка с контекстом
// задумка - на каждый воркер делать враппер с контекстом с фиксированным таймаутом
//-----------------------------------------------------------------------
//func (s *Server) workerCheckDBS(node string, workerNumber int, chanEnd chan bool, chanInfo chan DBWorkerPacket, repeatCheck bool, timeoutSecond int) {
//	s.log.Printf(fmt.Sprintf(infoWorkerStart, workerNumber, node))
//	defer func() {
//		s.log.Printf(fmt.Sprintf(infoWorkerStart, workerNumber, node))
//		chanEnd <- true
//	}()
//
//	////время паузы исскуственно для проверки возврата значений
//	//time.Sleep(time.Second * time.Duration(workerNumber))
//
//	//одиночный запрос
//	if repeatCheck == false {
//		err, res, timeRun := s.checkServer(node)
//		if err != nil || res == false {
//			//сервер не доступен по порту (порт закрыт, или ошибка создания хэндлера)
//			chanInfo <- DBWorkerPacket{
//				IDS:        fmt.Sprintf("worker#%d", workerNumber),
//				Node:       node,
//				Result:     false,
//				Error:      err.Error(),
//				StatusWork: "single",
//				TimeRun:    timeRun,
//			}
//		} else {
//			if res {
//				//сервер доступен
//				chanInfo <- DBWorkerPacket{
//					IDS:        fmt.Sprintf("worker#%d", workerNumber),
//					Node:       node,
//					Result:     true,
//					Error:      "",
//					StatusWork: "single",
//					TimeRun:    timeRun,
//				}
//			}
//		}
//		return
//	}
//
//	//циклично по таймеру чекать ноду
//	if repeatCheck {
//		ticker := time.NewTicker(time.Duration(s.cfg.NodeTicker) * time.Minute)
//		for {
//			select {
//			//команда о завершении воркера
//			case <-chanEnd:
//				s.log.Printf("worker#%d the end\n", workerNumber)
//				return
//			//ждем события и запускаем проверку сервера
//			case <-ticker.C:
//				err, res, timeRun := s.checkServer(node)
//				if err != nil || res == false {
//					//сервер не доступен по порту (порт закрыт, или ошибка создания хэндлера)
//					chanInfo <- DBWorkerPacket{
//						IDS:        fmt.Sprintf("worker#%d", workerNumber),
//						Node:       node,
//						Result:     false,
//						Error:      err.Error(),
//						StatusWork: "repeat",
//						TimeRun:    timeRun,
//					}
//				} else {
//					if res {
//						//сервер доступен
//						chanInfo <- DBWorkerPacket{
//							IDS:        fmt.Sprintf("worker#%d", workerNumber),
//							Node:       node,
//							Result:     true,
//							Error:      "",
//							StatusWork: "repeat",
//							TimeRun:    timeRun,
//						}
//					}
//				}
//			}
//		}
//	}
//}

//----------------------------------------------------------------------
//одиночная проверка
//-----------------------------------------------------------------------
//func (s *Server) SingleLooper(conn *websocket.Conn, chanTOTALBREAK chan int) {
func (s *Server) SingleLooper(chanTOTALBREAK chan int, toWrite chan interface{}) {

	//определение переменных
	chanEnd := make(chan bool)
	chanWork := make(chan DBWorkerPacket)
	chanCommand := make(chan bool)
	var count = 0
	var countCurrent = 0
	var flagTerminated = false

	//запуск горутин
	for i, n := range s.cfg.NodeList {
		//go s.workerCheckDBS(n, i, chanEnd, chanWork, false, s.cfg.TimeoutTCP)
		go s.workerSINGLE(n, i, chanEnd, chanWork, chanCommand)
	}

	//основной цикл прослушивания каналов
	for {
		select {

		//канал для внезапного отключения/потери соединения сокета etc...
		case <-chanTOTALBREAK:

			s.log.Printf("[TERMINATED] = %d\n", count)

			//передача клиенту сообщения о количестве отработавших горутин
			t := TOClient{Client: FromServer{
				Nodes:      len(s.cfg.NodeList),
				Command:    "count",
				CountCheck: count,
			}, Worker: DBWorkerPacket{
				IDS:    "",
				Node:   "",
				Result: false,
				Error:  "",
			}}

			toWrite <- t

			//рассылаю команду отмены работы активным воркерам (закрыть канал)
			close(chanCommand)

			//выставляю флаг терминального прерывания
			flagTerminated = true

			//возврат (делать не надо, точкой выхода должно быть получения от всех незавершенных воркеров уведомолений
			//о завершении работы )

			//return

		//канал поступление сообщений от воркеров
		case v := <-chanWork:
			//веду подсчет полученных сообщений от рабочих
			countCurrent++

			//формирую пакет-результат для передачи в вебсокет
			t := TOClient{Client: FromServer{
				Nodes:      len(s.cfg.NodeList),
				Command:    "work",
				CountCheck: countCurrent,
			}, Worker: v}

			//передаю результат в вебсокет
			toWrite <- t

		//канал сообщение от враппера воркера  о завершении работы
		case _ = <-chanEnd:
			//подсчитываю количество сигналов от рабочих
			count++

			//проверяю флаг терминального окончания, если выставлен то данные о завершенных не передаю
			if flagTerminated && count == len(s.cfg.NodeList) {
				return
			} else {

				s.log.Printf("----GETTING SIGNAL FROM CHANNEL THE END: %v\n", count)

				//если общее количество равно количеству запущенных воркеров то возврат
				if count == len(s.cfg.NodeList) {
					//работа воркеров завершена
					t := TOClient{Client: FromServer{
						Nodes:      len(s.cfg.NodeList),
						Command:    "end",
						CountCheck: count,
					}, Worker: DBWorkerPacket{
						IDS:    "",
						Node:   "",
						Result: false,
						Error:  "",
					}}

					//передача результата в вебсокет
					toWrite <- t

					//возврат из горутины
					return
				}
			}
		} //for
	}
}

//адапатация под новую структуру с выборкой списка из бд
func (s *Server) SingleLooperADAPTIVE(chanTOTALBREAK chan int, toWrite chan interface{}, listFILID []int, typeCheck string) {

	//определение переменных
	chanEnd := make(chan bool)
	chanWork := make(chan DBWorkerPacket)
	chanCommand := make(chan bool)
	var count = 0
	var countCurrent = 0
	var flagTerminated = false

	////запуск горутин
	//for i, n := range s.cfg.NodeList {
	//	//go s.workerCheckDBS(n, i, chanEnd, chanWork, false, s.cfg.TimeoutTCP)
	//	go s.workerSINGLE(n, i, chanEnd, chanWork, chanCommand)
	//}
	listRes := []*DatabaseNode{}
	for i, n := range listFILID {
		for _, x := range s.ListDatabaseNodes.ListDBNode {
			if x.Filid == n {
				listRes = append(listRes, x)
				go s.workerSINGLEADAPTIVE(x, i, chanEnd, chanWork, chanCommand)
			}
		}
	}

	//отправка информации о количестве в работе горутин и серверов
	//передача клиенту сообщения о количестве отработавших горутин
	t := TOClient{Client: FromServer{
		Nodes: len(listFILID),
		//Nodes:      len(s.ListDatabaseNodes.ListDBNode),
		Command:    "count",
		CountCheck: count,
		TypeCheck:  typeCheck,
	}, Worker: DBWorkerPacket{
		IDS:    "",
		Node:   "",
		Result: false,
		Error:  "",
	}}

	toWrite <- t

	//основной цикл прослушивания каналов
	for {
		select {

		//канал для внезапного отключения/потери соединения сокета etc...
		case <-chanTOTALBREAK:

			s.log.Printf("[TERMINATED] = %d\n", count)

			//передача клиенту сообщения о количестве отработавших горутин
			t := TOClient{Client: FromServer{
				Nodes:      len(s.ListDatabaseNodes.ListDBNode),
				Command:    "terminate",
				CountCheck: count,
				TypeCheck:  typeCheck,
			}, Worker: DBWorkerPacket{
				IDS:    "",
				Node:   "",
				Result: false,
				Error:  "",
			}}

			toWrite <- t

			//рассылаю команду отмены работы активным воркерам (закрыть канал)
			//прежде закрытия канала,надо его проверить на открытость (?)
			close(chanCommand)

			//выставляю флаг терминального прерывания
			flagTerminated = true

			//возврат (делать не надо, точкой выхода должно быть получения от всех незавершенных воркеров уведомолений
			//о завершении работы )

			//return

		//канал поступление сообщений от воркеров
		case v := <-chanWork:
			//веду подсчет полученных сообщений от рабочих
			countCurrent++

			//формирую пакет-результат для передачи в вебсокет
			t := TOClient{Client: FromServer{
				Nodes:      len(s.ListDatabaseNodes.ListDBNode),
				Command:    "work",
				CountCheck: countCurrent,
			}, Worker: v}

			//передаю результат в вебсокет
			toWrite <- t

		//канал сообщение от враппера воркера  о завершении работы
		case _ = <-chanEnd:
			//подсчитываю количество сигналов от рабочих
			count++

			//проверяю флаг терминального окончания, если выставлен то данные о завершенных не передаю
			if flagTerminated && count == len(listFILID) {
				return
			} else {
				s.log.Printf("----GETTING SIGNAL FROM CHANNEL THE END: %v\n", count)

				//если общее количество равно количеству запущенных воркеров то возврат
				if count == len(listFILID) {
					//работа воркеров завершена
					t := TOClient{Client: FromServer{
						Nodes:      len(s.ListDatabaseNodes.ListDBNode),
						Command:    "end" + typeCheck,
						CountCheck: count,
						TypeCheck:  typeCheck,
					}, Worker: DBWorkerPacket{
						IDS:    "",
						Node:   "",
						Result: false,
						Error:  "",
					}}

					//передача результата в вебсокет
					toWrite <- t

					//возврат из горутины
					return
				}
			}
		} //for
	}
}
func (s *Server) workerSINGLEADAPTIVE(node *DatabaseNode, workerNumber int, chanEnd chan bool, chanInfo chan DBWorkerPacket, chanCommand chan bool) {
	defer func() {
		chanEnd <- false
		s.log.Printf("[worker#%d] end working\n", workerNumber)
	}()

	//host := strings.Split(strings.Split(strings.Split(node.SqlRequest, "@")[1], "/")[0], ":")

	//контекст с таймаутом
	ctx, cancelCTX := context.WithTimeout(context.Background(), time.Second*time.Duration(node.TimeoutCTX))
	//канал для получения данных от воркера
	chanEndWorker := make(chan FromWorker)
	//запуск воркера
	go s.checkServer(node.SqlRequest, chanEndWorker)
	//цикл по прослушиванию событий
	for {
		select {

		//канал для получения команды от глобального управленца
		case <-chanCommand:
			//прекращаю работу воркера экстренно
			cancelCTX()
			s.log.Printf("[workerSINGLE--worker#%d] get COMMAND TERMINATED!!!!\n", workerNumber)
			return
		//канал для оповещения о истечении таймера по контексту
		case <-ctx.Done():

			//истек заданный таймаут, база подвисшая
			chanInfo <- DBWorkerPacket{
				IDS:        fmt.Sprintf("worker#%d", workerNumber),
				Node:       node.Dbipaddr,
				Result:     false,
				Error:      fmt.Sprintf("`%s` таймаут истек\n", node.Dbipaddr),
				StatusWork: "single",
				TimeRun:    fmt.Sprintf("%d s", node.TimeoutCTX),
				FILID:      node.Filid,
			}
			s.log.Printf("[workerSINGLE--worker#%d][%d] timeout expired!!!!\n", workerNumber, node.Filid)
			return
		//канал для приема сообщения от воркера, я ее сразу перенправляю клиенту-вебсокет
		case res := <-chanEndWorker:
			dbs := DBWorkerPacket{
				IDS:        fmt.Sprintf("worker#%d", workerNumber),
				Node:       node.Dbipaddr,
				Result:     res.ResultCheck,
				StatusWork: "single",
				TimeRun:    res.TimeRun,
				Error:      res.Error,
				FILID:      node.Filid,
			}
			chanInfo <- dbs
			s.log.Printf("[workerSINGLE--worker#%v][%v] get result, its ok!!!!\n", workerNumber, dbs, node.Filid)
			return
		}
	}
}

//----------------------------------------------------------------------
// декомпозиция рулит в данном случае
// отдельная функция проверки контекста таймаута и получения и обработки работы горутины
// выходит цепочка (обертка) надо воркером в виде функции-менеджера - принимающие входные аргументы для выполнения
// workerSINGLE -> враппер-функция для  воркера с отслеживаемым контекстом
// node = сервер-нода, которую нужно проверить
// chandEnd = канал от хэндлера
// chainInfo = канал для передачи пакетов от воркеров клиенту-вебсокет
//-----------------------------------------------------------------------
func (s *Server) workerSINGLE(node string, workerNumber int, chanEnd chan bool, chanInfo chan DBWorkerPacket, chanCommand chan bool) {
	defer func() {
		chanEnd <- false
		s.log.Printf("[worker#%d] end working\n", workerNumber)
	}()

	host := strings.Split(strings.Split(strings.Split(node, "@")[1], "/")[0], ":")

	//контекст с таймаутом
	ctx, cancelCTX := context.WithTimeout(context.Background(), time.Second*time.Duration(s.cfg.TimeoutCTX))
	//канал для получения данных от воркера
	chanEndWorker := make(chan FromWorker)
	//запуск воркера
	go s.checkServer(node, chanEndWorker)
	//цикл по прослушиванию событий
	for {
		select {

		//канал для получения команды от глобального управленца
		case <-chanCommand:
			//прекращаю работу воркера экстренно
			cancelCTX()
			s.log.Printf("[workerSINGLE--worker#%d] get COMMAND TERMINATED!!!!\n", workerNumber)
			return
		//канал для оповещения о истечении таймера по контексту
		case <-ctx.Done():

			//истек заданный таймаут, база подвисшая
			chanInfo <- DBWorkerPacket{
				IDS:        fmt.Sprintf("worker#%d", workerNumber),
				Node:       strings.Join(host, ":"),
				Result:     false,
				Error:      fmt.Sprintf("`%s` таймаут истек\n", strings.Join(host, ":")),
				StatusWork: "single",
				TimeRun:    fmt.Sprintf("%d s", s.cfg.TimeoutCTX),
			}
			s.log.Printf("[workerSINGLE--worker#%d] timeout expired!!!!\n", workerNumber)
			return
		//канал для приема сообщения от воркера, я ее сразу перенправляю клиенту-вебсокет
		case res := <-chanEndWorker:
			dbs := DBWorkerPacket{
				IDS:        fmt.Sprintf("worker#%d", workerNumber),
				Node:       strings.Join(host, ":"),
				Result:     res.ResultCheck,
				StatusWork: "single",
				TimeRun:    res.TimeRun,
				Error:      res.Error,
			}
			chanInfo <- dbs
			s.log.Printf("[workerSINGLE--worker#%d] get result, its ok!!!!\n", workerNumber, dbs)
			return
		}
	}
}

//-------------------------------------------------------------------------------------------
//проверка firebird базы на доступность по tcp, по длинным запросам
//возвращает ошибку, булево значение (true=база доступна, false = недоступна, время запроса
// upd: chandEnd = канал возвращает значение для оповещение мнедежера о завершении работы
// 		возращает FormWorker в канал менеджеру
//-------------------------------------------------------------------------------------------
func (s *Server) checkServer(node string, chanEnd chan FromWorker) {
	var fw FromWorker
	defer func() {
		chanEnd <- fw
	}()
	//изучение времени выполнения подключени к бд + запрос
	start := time.Now()

	s.log.Printf("[checkServer] node: `%v`\n", node)
	//создаю коннектор к базе данных
	conn, err := sql.Open("firebirdsql", node)
	if err != nil {
		s.log.Printf("%s %s\n", "[ERROR]", "ошибка при создании подключения к базе данных")
		fw.Error = err.Error()
		fw.ResultCheck = false
		fw.TimeRun = ""
	} else {
		s.log.Printf("[checkServer] sqlHandler: `%v`\n", conn)
		//делаю выборку из BD
		var n int
		row := conn.QueryRow("select first 1 MON$SERVER_PID  from MON$ATTACHMENTS;")
		err := row.Scan(&n)
		endTime := time.Since(start)
		if err != nil {
			fw.Error = err.Error()
			fw.ResultCheck = false
			fw.TimeRun = endTime.String()
		} else {
			if n > 0 {
				//база доступна
				fw.Error = ""
				fw.ResultCheck = true
				fw.TimeRun = endTime.String()
			} else {
				//база недоступна
				fw.Error = fmt.Sprintf("ответ по запросу некорректен  %n ", n)
				fw.ResultCheck = false
				fw.TimeRun = endTime.String()
			}
		}
	}
	//закрываю сокет, если не закрывать с течением времени накапливается количество => limit в системе и будет атата
	defer func() {
		if err := conn.Close(); err != nil {
			s.log.Printf("error closing socket: %v\n", err)
		}
	}()
	return
}

//----------------------------------------------------------------------
// декомпозиция рулит в данном случае
// отдельная функция проверки контекста таймаута и получения и обработки работы горутины
// выходит цепочка (обертка) надо воркером в виде функции-менеджера - принимающие входные аргументы для выполнения
// workerSINGLE -> враппер-функция для  воркера с отслеживаемым контекстом
// node = сервер-нода, которую нужно проверить
// chandEnd = канал от хэндлера
// chainInfo = канал для передачи пакетов от воркеров клиенту-вебсокет
//-----------------------------------------------------------------------

func (s *Server) workerSINGLEForm(d DBWorkerFormParams, chanEnd chan bool, chanWork chan DBWorkerPacket, chanCommand chan string) {
	wname := fmt.Sprintf("ids%s", d.Node)
	defer func() {
		s.log.Printf("[worker#%s] end working\n", wname)
	}()

	host := strings.Split(strings.Split(strings.Split(d.Node, "@")[1], "/")[0], ":")

	//контекст с таймаутом
	ctx, cancelCTX := context.WithTimeout(context.Background(), time.Second*time.Duration(d.Timeout))
	//канал для получения данных от воркера
	chanEndWorker := make(chan FromWorker)
	//запуск воркера
	go s.checkServer(d.Node, chanEndWorker)
	//цикл по прослушиванию событий
	for {
		select {

		//канал для получения команды от глобального управленца
		case <-chanCommand:
			//прекращаю работу воркера экстренно
			cancelCTX()
			s.log.Printf("[workerSINGLE--worker#%d] get COMMAND TERMINATED!!!!\n", wname)
			return

		//канал для оповещения о истечении таймера по контексту
		case <-ctx.Done():
			//истек заданный таймаут, база подвисшая
			chanWork <- DBWorkerPacket{
				IDS:        wname,
				Node:       strings.Join(host, ":"),
				Result:     false,
				Error:      fmt.Sprintf("`%s` таймаут истек\n", strings.Join(host, ":")),
				StatusWork: "single",
				TimeRun:    fmt.Sprintf("%d s", s.cfg.TimeoutCTX),
			}
			s.log.Printf("[workerSINGLE--worker# `%s` ] timeout expired!!!!\n", wname)
			return

		//канал для приема сообщения от воркера, я ее сразу перенправляю клиенту-вебсокет
		case res := <-chanEndWorker:
			dbs := DBWorkerPacket{
				IDS:        fmt.Sprintf("worker# `%s` ", wname),
				Node:       strings.Join(host, ":"),
				Result:     res.ResultCheck,
				StatusWork: "single",
				TimeRun:    res.TimeRun,
				Error:      res.Error,
			}
			chanWork <- dbs
			s.log.Printf("[workerSINGLE--worker# %s ] get result, its ok!!!!\n", wname, dbs)
			return
		}
	}
}

//----------------------------------------------------------------------
// горутина обертка над workerSINGLEForm для организации работы по таймеру
//-----------------------------------------------------------------------
func (s *Server) WorkerRepeatTimerFormSingleChecker(w DBWorkerFormParams,
	chanGCommand chan GlobalCommandToWorker, chanGEnd chan string, chanGWork chan DBWorkerFormParams) {

	s.log.Printf("--starting => %v\n", w.Node)
	defer func() {
		s.log.Printf("--ending => %v\n", w.Node)

		//оповещаю глобального управленца о прекращении работы
		chanGEnd <- w.Mark

	}()

	//определение переменных
	chanEnd := make(chan bool)            //окончание работы воркера
	chanWork := make(chan DBWorkerPacket) //результат работы воркера
	chanCommand := make(chan string)      //управляющие команды

	var countCurrent = 0

	//в зависимости от опции в конфиге (devo = секунды, prod = минуты)
	var ticker *time.Ticker
	if s.cfg.Developing {
		ticker = time.NewTicker(time.Second * time.Duration(w.Period))
	} else {
		ticker = time.NewTicker(time.Minute * time.Duration(w.Period))
	}

	//первый запуск до отработки первого тика по периоду
	go s.workerSINGLEForm(DBWorkerFormParams{
		Node:    w.Node,
		Timeout: w.Period,
		Period:  w.Timeout,
	}, chanEnd, chanWork, chanCommand)

	for {
		select {

		//канал команды от глобалуправленца
		case com := <-chanGCommand:
			switch com.Command {
			case "endwork":
				if w.Mark == com.Marked {
					s.log.Printf("-----[%v] END WORK COMMAND FROM GLOBAL MANAGER ----\n", com.Marked)
					//рассылаю команду отмены работы активным воркерам (закрыть канал)
					close(chanCommand)
					//немедленный возврат = прекращение работы
					return
				} else {
					s.log.Printf("-----[%v] END WORK COMMAND FROM GLOBAL MANAGER ---- NOT ME %s\n", com.Marked, w.Mark)
				}

			default:
				s.log.Printf("[WARNING] WRONG COMMAND FROM GLOBAL MANAGER: %v\n", com)
			}

		//обработка действий по таймеру
		case t := <-ticker.C:
			s.log.Printf("[*] next iteration timeout `%v` \n", t.String())
			go s.workerSINGLEForm(DBWorkerFormParams{
				Node:    w.Node,
				Timeout: w.Period,
				Period:  w.Timeout,
			}, chanEnd, chanWork, chanCommand)

		//канал поступление сообщений от воркеров
		case v := <-chanWork:
			//веду подсчет полученных сообщений от рабочих
			countCurrent++
			if s.cfg.Debugcheckdb {
				s.log.Printf("Getting msg from worker: %v\nSTATUS: %v\n", v, s.stockTimerSingle)
			}

			//фиксирую только "плохие" проверки
			if v.Result == false {
				w.WorkerPacket = append(w.WorkerPacket, v)
			}
			//формирую пакет для передачи в общий сток
			dbform := w
			dbform.StartTime = time.Now()
			dbform.Counter = int64(countCurrent)

			//передаю результат в вебсокет
			chanGWork <- dbform

		//канал сообщение от враппера воркера  о завершении работы
		case _ = <-chanEnd:
			return

			//поступил сигнал о немедленном прекращении работы
		case <-chanGEnd:
			s.log.Printf("==============[TERMINATED = %s]================\n", time.Now().String())

			//рассылаю команду отмены работы активным воркерам (закрыть канал)
			close(chanCommand)

			//возврат из функции горутины
			return
		}
	}

}

//----------------------------------------------------------------------
// общая горутина, которая будет заниматься обработкой всей записи в вебсокет
// единоличная запись  в контексте внешнего вызова, данная горутина не читает вебсокет
// чтением занимается вебхэндлер
// toRead - канал для чтения
// toWrite - канал для записи
// command - управляющий канал
//-----------------------------------------------------------------------
func (s *Server) WriterWebsocket(w *websocket.Conn, toWrite chan interface{}, command chan string) {
	s.log.Println("[WriterWebsocket] starting...")
	defer func() {
		s.log.Println("[WriterWebsocket] exit...")
	}()
	for {
		select {
		//канал для  получения структур для записи в вебсокет извне
		case some := <-toWrite:
			err := w.WriteJSON(some)
			if err != nil {
				s.log.Printf("[WriterWebsocket] [error] %v\n", err)
			}
			//управляющий канал
		case <-command:
			return
		}
	}
}

//----------------------------------------------------------------------
// hash random
//-----------------------------------------------------------------------
func (s *Server) Hash() string {
	return fmt.Sprintf("%x", md5.Sum([]byte(time.Now().String())))
}

//----------------------------------------------------------------------
// проверка бэкапа разово
//-----------------------------------------------------------------------

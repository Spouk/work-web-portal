package library

import (
	"fmt"
	"github.com/go-chi/chi"
	`github.com/go-chi/chi/middleware`
	"github.com/gorilla/websocket"
	"net/http"
	"regexp"
	"strconv"
	"time"
)

//*******************************************************************
// access routines
//*******************************************************************
func (s *Server) privateroutes() {
	p := chi.NewRouter()

	//middlewares
	p.Use(middleware.Logger)
	p.Use(middleware.Recoverer)
	p.Use(middleware.StripSlashes)
	p.Use(s.MiddlewareSession)

	//users
	p.Get("/p/functions/{command}", s.HandlerPrivates)
	p.Post("/p/functions/{command}", s.HandlerPrivates)
	p.Get("/p/functions/{command}/{id}", s.HandlerPrivates)
	p.Post("/p/functions/{command}/{id}", s.HandlerPrivates)

	//websocket :: remd + imek + users crud
	p.Get("/ws/functions", s.WebsocketFu)
	p.Post("/ws/functions", s.WebsocketFu)

}

//*******************************************************************
// private  handlers
//*******************************************************************
func (s *Server) HandlerPrivates(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	s.log.Printf("[HandlerPrivates] Current URL: %v\n", r.URL.Path)
	switch r.URL.Path {

	case "/functions/imek/check":
		vals := r.URL.Query()
		s.log.Printf("===FOUND: %v\n", vals)
		if vals.Get("imek") == "start" {
			res := s.WorkerIMEK_Single()
			ses.SetDATA("stock", "resultimek", res)
			s.SaverFunctions.SaveIMEK(res)
		} else {
			ses.SetDATA("stock", "resultimek", s.SaverFunctions.GetIMEK())
		}

	case "/functions/remd/check":
		vals := r.URL.Query()
		s.log.Printf("===FOUND: %v\n", vals)
		if vals.Get("remd") == "start" {
			res := s.WorkerREMD_Single()
			ses.SetDATA("stock", "resultremd", res)
			s.SaverFunctions.SaveREMD(res)
		} else {
			ses.SetDATA("stock", "resultremd", s.SaverFunctions.GetREMD())
		}

	case "/functions/imek/stat", "/functions/remd/stat":
		ses := s.getContext(r)
		ready := []InfoFunctionTime{}
		for _, x := range s.FifoQueue.GetStock() {
			if x == nil {
				continue
			}
			tmp, ok := x.(InfoFunctionTime)
			if ok {
				s.log.Printf("--convert: %v\n", tmp)
				ready = append(ready, tmp)
			} else {
				s.log.Printf("ERROR CONVERT TO TYPE: %v: %v\n", tmp, ok)
			}

		}
		ses.SetDATA("stock", "fifo", ready)

	case "/functions/user/create":
		s.log.Printf("-----user route found----\n")
	}

	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// http.404
//-----------------------------------------------------------------------
func (s *Server) Handler404(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	_ = s.R.RenderCode(http.StatusNotFound, "404.html", ses, w)
}

//----------------------------------------------------------------------
// http.405
//-----------------------------------------------------------------------
func (s *Server) Handler405(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	_ = s.R.RenderCode(http.StatusMethodNotAllowed, "405.html", ses, w)
}

//----------------------------------------------------------------------
// root
//-----------------------------------------------------------------------
func (s *Server) HandlerRoot(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// ajax
//-----------------------------------------------------------------------
func (s *Server) HandlerAjax(w http.ResponseWriter, r *http.Request) {
	//ses := s.getContext(r)
	command := chi.URLParam(r, "command")
	if r.Method == "POST" {
		switch command {
		case "testerevent":
			//сгенерировал тестовый сток запросов
			var stockAnswers = []TesterAnswerAjax{}
			for i := 0; i < 10; i++ {
				stockAnswers = append(stockAnswers,
					TesterAnswerAjax{
						IDS:    i,
						Name:   fmt.Sprintf("gorutine#%d", i),
						URL:    fmt.Sprintf("http://www.spouk.ru/?id=%d", i),
						Status: "start",
					},
				)
			}
			err := s.R.JSON(http.StatusOK, &Answer{
				Stage: "start",
				T:     stockAnswers,
			}, w)
			if err != nil {
				s.log.Println("error send JSON ajax: ", err)
			}

		default:
			s.log.Printf("--ajax default unknow :%v\n", command)
		}
	}

}

//----------------------------------------------------------------------
// config
//-----------------------------------------------------------------------
func (s *Server) HandlerConfig(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	//s.log.Printf("Context: %v\n", ses)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
		//s.log.Printf("Error: %v\n", err)
	}
}

var upgrader1 = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

//----------------------------------------------------------------------
// single database check
//-----------------------------------------------------------------------
func (s *Server) HandlerCheck(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	s.log.Printf("[HandlerCheck] Current URL: %v\n", r.URL.Path)
	switch r.URL.Path {

	case "/functions/imek/check":
		vals := r.URL.Query()
		s.log.Printf("===FOUND: %v\n", vals)
		if vals.Get("imek") == "start" {
			res := s.WorkerIMEK_Single()
			ses.SetDATA("stock", "resultimek", res)
			s.SaverFunctions.SaveIMEK(res)
		} else {
			ses.SetDATA("stock", "resultimek", s.SaverFunctions.GetIMEK())
		}

	case "/functions/remd/check":
		vals := r.URL.Query()
		s.log.Printf("===FOUND: %v\n", vals)
		if vals.Get("remd") == "start" {
			res := s.WorkerREMD_Single()
			ses.SetDATA("stock", "resultremd", res)
			s.SaverFunctions.SaveREMD(res)
		} else {
			ses.SetDATA("stock", "resultremd", s.SaverFunctions.GetREMD())
		}

	case "/functions/imek/stat", "/functions/remd/stat":
		ses := s.getContext(r)
		ready := []InfoFunctionTime{}
		for _, x := range s.FifoQueue.GetStock() {
			if x == nil {
				continue
			}
			tmp, ok := x.(InfoFunctionTime)
			if ok {
				s.log.Printf("--convert: %v\n", tmp)
				ready = append(ready, tmp)
			} else {
				s.log.Printf("ERROR CONVERT TO TYPE: %v: %v\n", tmp, ok)
			}

		}
		ses.SetDATA("stock", "fifo", ready)

	case "/functions/user/create":
		s.log.Printf("-----user route found----\n")
	}

	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// timer  database check
//-----------------------------------------------------------------------
func (s *Server) HandlerCheckTimer(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// wrapper python
//-----------------------------------------------------------------------
func (s *Server) Handlerwrappython(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// wrapper dbs
//-----------------------------------------------------------------------
func (s *Server) Handlerwrapdbs(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// spider bd-net (smb/cifs)
//-----------------------------------------------------------------------
func (s *Server) Handlerspidersmb(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// spider bd-net (ftp)
//-----------------------------------------------------------------------
func (s *Server) Handlerspiderftp(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// singleconfig
//-----------------------------------------------------------------------
func (s *Server) HandlerSingleConfig(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// checkbackupSingle
//-----------------------------------------------------------------------
func (s *Server) HandlerSingleBackupConfig(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// timer-single-config
//-----------------------------------------------------------------------
func (s *Server) HandlerTimerSingleConfig(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// timer-single-result
//-----------------------------------------------------------------------
func (s *Server) HandlerTimerSingleResult(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// checkbackupTimer
//-----------------------------------------------------------------------
func (s *Server) HandlerSingleBackupTimer(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//root - tabs /root/{command}
func (s *Server) HandlerRootTabs(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	command := chi.URLParam(r, "command")
	switch command {
	case "":
	case "tab1":
	case "tab2":
	case "tab3":
	case "tab4":
	default:
		s.Handler404(w, r)
		return
	}
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// тестовый
//-----------------------------------------------------------------------
func (s *Server) WebSocket2(w http.ResponseWriter, r *http.Request) {
	//переменные
	chanTOTALBREAK := make(chan int)

	//создаю коннектор для обработки websocketa
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		s.log.Println(err)
		return
	}
	s.log.Println("connection established from client")

	//дефер функция
	defer func() {
		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER]\n")
		err := conn.Close()
		if err != nil {
			s.log.Printf("error close websocket `%v`\n", err)
		}
		//отправка команды горутинам на выход //при обрыве websocket соединения
		chanTOTALBREAK <- 0
		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER][SENDED COMMAND TO TERMINATE GOROUTINES]\n")
	}()
	//передача информационного блока для установки первичных значений
	t := TOClient{Client: FromServer{
		Nodes:   len(s.cfg.NodeList),
		Command: "infoset",
	}, Worker: DBWorkerPacket{
		IDS:    "empty",
		Node:   "empty",
		Result: false,
		Error:  "empty",
	}}
	err = conn.WriteJSON(&t)
	if err != nil {
		s.log.Println(err)
	}
	//бесконечный цикл для обработки входящих от клиента и отправка ему ответов
	for {
		//читаю с клиента
		var fromClient FromClient
		err = conn.ReadJSON(&fromClient)
		if err != nil {
			//TODO: добавить анализ ошибок + добавить логику
			s.log.Printf("[error read JSON from client] %v\n", err)
			return
		} else {
			switch fromClient.Command {
			case "start_single":
				s.log.Printf("single\n")
				//go s.SingleLooper(conn, chanTOTALBREAK)

			case "start_period":
				s.log.Printf("period\n")

			case "stop_period", "stop_single":
				s.log.Printf("stop period or single\n")
				//останов всех горутин
				chanTOTALBREAK <- 0
				//count := <- chanTOTALBREAK
				//s.log.Printf("---COUNT FROM HANDLER: %v\n", count)
				//отправка конструкта-уведомления о прекращении работы
				t := TOClient{Client: FromServer{
					Nodes:      len(s.cfg.NodeList),
					Command:    "end",
					CountCheck: 0,
				}, Worker: DBWorkerPacket{
					IDS:    "",
					Node:   "",
					Result: false,
					Error:  "",
				}}
				err = conn.WriteJSON(&t)
				if err != nil {
					s.log.Println(err)
				}

			default:
				s.log.Printf("wrong command from client\n")
			}
		}
	}
}

//func (s *Server) WebSocketDBSTester(w http.ResponseWriter, r *http.Request) {
//	//созданик коннектора вод вебсокет
//	conn, err := upgrader.Upgrade(w, r, nil)
//	if err != nil {
//		s.log.Println(err)
//		return
//	}
//	s.log.Println("connection established from client")
//
//repeatPoint:
//
//	//передача информационного блока для установки первичных значений
//	t := TOClient{Client: FromServer{
//		Nodes:   len(s.cfg.NodeList),
//		Command: "infoset",
//	}, Worker: DBWorkerPacket{
//		IDS:    "empty",
//		Node:   "empty",
//		Result: false,
//		Error:  "empty",
//	}}
//
//	err = conn.WriteJSON(&t)
//	if err != nil {
//		s.log.Println(err)
//	}
//
//	//обработка
//	for {
//		//читаю с клиента
//		var fromClient FromClient
//		err = conn.ReadJSON(&fromClient)
//		if err != nil {
//			//TODO: добавить анализ ошибок + добавить логику
//			s.log.Println(err)
//			//ошибка, вероятнее всего ошибка связанная с пропаданием сокета для записи/чтения
//		} else {
//			switch fromClient.Command {
//			case "start_single":
//				chanEnd := make(chan bool)
//				chanWork := make(chan DBWorkerPacket)
//				count := 0
//
//				//запуск горутин
//				for i, n := range s.cfg.NodeList {
//					go s.workerCheckDBS(n, i, chanEnd, chanWork, false, s.cfg.TimeoutTCP)
//				}
//
//				//основной цикл прослушивания каналов
//				for {
//					select {
//					case v := <-chanWork:
//						t := TOClient{Client: FromServer{
//							Nodes:   len(s.cfg.NodeList),
//							Command: "work",
//						}, Worker: v}
//						err = conn.WriteJSON(&t)
//						if err != nil {
//							s.log.Println(err)
//						}
//					case _ = <-chanEnd:
//						count++
//						if count == len(s.cfg.NodeList) {
//							//работа воркеров завершена
//							t := TOClient{Client: FromServer{
//								Nodes:      len(s.cfg.NodeList),
//								Command:    "end",
//								CountCheck: count,
//							}, Worker: DBWorkerPacket{
//								IDS:    "",
//								Node:   "",
//								Result: false,
//								Error:  "",
//							}}
//							err = conn.WriteJSON(&t)
//							if err != nil {
//								s.log.Println(err)
//							}
//							goto repeatPoint
//						}
//					}
//				}
//
//			case "start_period":
//				chanEnd := make(chan bool)
//				chanWork := make(chan DBWorkerPacket)
//				ticker := time.NewTicker(s.cfg.NodeTicker * time.Minute)
//
//				//основной цикл
//				for {
//				nextIteration:
//
//					select {
//
//					//пакеты от воркеров
//					case v := <-chanWork:
//						t := TOClient{Client: FromServer{
//							Nodes:   len(s.cfg.NodeList),
//							Command: "work",
//						}, Worker: v}
//						err = conn.WriteJSON(&t)
//						if err != nil {
//							s.log.Println(err)
//						}
//
//					//сработал таймер, пора на работу
//					case _ = <-ticker.C:
//						chanEnd := make(chan bool)
//						chanWork := make(chan DBWorkerPacket)
//						count := 0
//
//						//запуск горутин
//						for i, n := range s.cfg.NodeList {
//							go s.workerCheckDBS(n, i, chanEnd, chanWork, true, s.cfg.TimeoutTCP)
//						}
//
//						//цикл прослушивания горутин в рамках периода срабатывания таймера
//						for {
//							select {
//							case v := <-chanWork:
//								t := TOClient{Client: FromServer{
//									Nodes:   len(s.cfg.NodeList),
//									Command: "work",
//								}, Worker: v}
//								err = conn.WriteJSON(&t)
//								if err != nil {
//									s.log.Println(err)
//								}
//							case _ = <-chanEnd:
//								count++
//								if count == len(s.cfg.NodeList) {
//									//работа воркеров завершена
//									t := TOClient{Client: FromServer{
//										Nodes:   len(s.cfg.NodeList),
//										Command: "end",
//									}, Worker: DBWorkerPacket{
//										IDS:    "",
//										Node:   "",
//										Result: false,
//										Error:  "",
//									}}
//									err = conn.WriteJSON(&t)
//									if err != nil {
//										s.log.Println(err)
//									}
//
//									//следующая итерация
//									goto nextIteration
//								}
//							}
//						}
//
//						//заглушка
//					case _ = <-chanEnd:
//						continue
//					}
//				}
//
//			default:
//				s.log.Println("Ошибочная команда от клиента")
//			}
//		}
//	}
//}

//real websocket checker dbs
//func (s *Server) WebSocketDBS(w http.ResponseWriter, r *http.Request) {
//	//созданик коннектора вод вебсокет
//	conn, err := upgrader.Upgrade(w, r, nil)
//	if err != nil {
//		s.log.Println(err)
//		return
//	}
//
//	//	метка для безусловного перехода
//repeat:
//	var enableStart = false
//
//	for {
//		var fromClient FromClient
//		err = conn.ReadJSON(&fromClient)
//		if err != nil {
//			s.log.Println(err)
//		} else {
//			s.log.Printf("MESSAGE FROM WEBSOCKET CLIENT: %v\n", fromClient)
//			switch fromClient.Command {
//			//старт работы горутин
//			case "start":
//				//проверка на уже работающие горутины
//				if enableStart == false {
//					enableStart = true
//					//определение переменных для работы горутин
//					var count = 3
//					var workRes = make(chan ResultWorker) //канал для получения результатов работы воркеров
//					var endWork = make(chan bool)         //канал для сообщения о завершении работы воркера
//					var sw = &sync.WaitGroup{}
//					var countEnd = 0
//					for i := 0; i < count; i++ {
//						sw.Add(1)
//						go s.workerWC(workRes, 2, sw, fmt.Sprintf("worker#%d", i), endWork)
//					}
//					for {
//						select {
//						case _ = <-endWork:
//							countEnd += 1
//							if countEnd == count {
//								s.log.Printf("Все воркеры закончили работу\n")
//								//отправка клиенту уведомления о завершении работ
//								var endRes = ResultWorker{
//									IDS:        "",
//									Status:     "",
//									Result:     "",
//									FromServer: "end",
//								}
//								err = conn.WriteJSON(&endRes)
//								if err != nil {
//									s.log.Println(err)
//								} else {
//									s.log.Printf("send success\n")
//								}
//								//изменяю флаг
//								enableStart = false
//								//делаю переход на начало определение переменных
//								goto repeat
//
//							} else {
//								countEnd++
//							}
//						case res := <-workRes:
//							s.log.Printf("From workers :%v\n", res)
//							//передача клиенту
//							err = conn.WriteJSON(&res)
//							if err != nil {
//								s.log.Println(err)
//							} else {
//								s.log.Printf("send success\n")
//							}
//						}
//					}
//				}
//			case "end":
//			}
//		}
//	}
//}

//----------------------------------------------------------------------
// websocket tester
//-----------------------------------------------------------------------
func (s *Server) HandlerWebSocket(w http.ResponseWriter, r *http.Request) {

	s.log.Printf("--get new connection--\n")
	//создаю коннектор для обработки websocketa
	conn, err := upgrader.Upgrade(w, r, nil)
	s.log.Printf("Connection: %v\n,ErrorConnection: %v\n", conn, err)
	if err != nil {
		s.log.Println(err)
		return
	}
	s.log.Println("connection established from client")

	//дефер функция
	defer func() {
		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER]\n")
		err := conn.Close()
		if err != nil {
			s.log.Printf("error close websocket `%v`\n", err)
		}
	}()
	//передача информационного блока для установки первичных значений
	t := TOClient{Client: FromServer{
		Nodes:   len(s.cfg.NodeList),
		Command: "infoset",
	}, Worker: DBWorkerPacket{
		IDS:    "empty",
		Node:   "empty",
		Result: false,
		Error:  "empty",
	}}
	err = conn.WriteJSON(&t)
	if err != nil {
		s.log.Println(err)
	}

	for {
		//читаю с клиента
		var fromClient FromClient
		err = conn.ReadJSON(&fromClient)
		if err != nil {
			//TODO: добавить анализ ошибок + добавить логику
			s.log.Printf("[error read JSON from client] %v\n", err)
			return
		} else {
			s.log.Printf("From client: %v\n", fromClient)
		}
	}
}

////----------------------------------------------------------------------
//// websocket single handler
////-----------------------------------------------------------------------
//func (s *Server) HandlerWebsocketSingleCheck(w http.ResponseWriter, r *http.Request) {
//	//переменные
//	var chanTOTALBREAK = make(chan int)  // канал для прерывания работы запущенных воркеров
//	var command = make(chan string)      // управляющий канал для монопольного писателя в вебсокет
//	var toWrite = make(chan interface{}) // канал для передачи данных, какие надо передать в вебсокет
//
//	//создаю коннектор для обработки websocketa
//	conn, err := upgrader1.Upgrade(w, r, nil)
//	if err != nil {
//		s.log.Println(err)
//		return
//	}
//	s.log.Println("connection established from client")
//
//	//создаю и запускаю горутину для монопольной записи в вебсокет
//	go s.WriterWebsocket(conn, toWrite, command)
//
//	//дефер функция
//	defer func() {
//		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER]\n")
//		err := conn.Close()
//		if err != nil {
//			s.log.Printf("error close websocket `%v`\n", err)
//		}
//		//отправка команды горутинам на выход //при обрыве websocket соединения
//		chanTOTALBREAK <- 0
//	}()
//
//	//передача информационного блока для установки первичных значений
//	t := TOClient{Client: FromServer{
//		Nodes:   len(s.cfg.NodeList),
//		Command: "infoset",
//	}, Worker: DBWorkerPacket{
//		IDS:    "empty",
//		Node:   "empty",
//		Result: false,
//		Error:  "empty",
//	}}
//	toWrite <- t
//
//	//бесконечный цикл для обработки входящих от клиента и отправка ему ответов
//	for {
//		//читаю с клиента
//		var fromClient FromClient
//		err = conn.ReadJSON(&fromClient)
//		if err != nil {
//			//TODO: добавить анализ ошибок + добавить логику
//			s.log.Printf("[error read JSON from client] %v\n", err)
//			return
//		} else {
//			switch fromClient.Command {
//			case "start_single":
//				s.log.Printf("single\n")
//				go s.SingleLooper(chanTOTALBREAK, toWrite)
//
//			case "start_period":
//				s.log.Printf("period\n")
//
//			case "stop_period", "stop_single":
//				s.log.Printf("stop period or single\n")
//
//				//останов всех горутин посредством передачи сигнала глобалменеджеру
//				chanTOTALBREAK <- 0
//
//				//отправка конструкта-уведомления о прекращении работы
//				t := TOClient{Client: FromServer{
//					Nodes:      len(s.cfg.NodeList),
//					Command:    "end",
//					CountCheck: 0,
//				}, Worker: DBWorkerPacket{
//					IDS:    "",
//					Node:   "",
//					Result: false,
//					Error:  "",
//				}}
//				toWrite <- t
//
//			default:
//				s.log.Printf("wrong command from client\n")
//			}
//		}
//	}
//}

//----------------------------------------------------------------------
// websocket single handler
//-----------------------------------------------------------------------
func (s *Server) HandlerWebsocketSingleCheck(w http.ResponseWriter, r *http.Request) {
	//переменные
	var chanTOTALBREAK = make(chan int)  // канал для прерывания работы запущенных воркеров
	var command = make(chan string)      // управляющий канал для монопольного писателя в вебсокет
	var toWrite = make(chan interface{}) // канал для передачи данных, какие надо передать в вебсокет

	//создаю коннектор для обработки websocketa
	conn, err := upgrader1.Upgrade(w, r, nil)
	if err != nil {
		s.log.Println(err)
		return
	}
	s.log.Println("connection established from client")

	//создаю и запускаю горутину для монопольной записи в вебсокет
	go s.WriterWebsocket(conn, toWrite, command)

	//дефер функция
	defer func() {
		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER]\n")
		err := conn.Close()
		if err != nil {
			s.log.Printf("error close websocket `%v`\n", err)
		}
		//отправка команды горутинам на выход //при обрыве websocket соединения
		chanTOTALBREAK <- 0
	}()

	//передача информационного блока для установки первичных значений
	t := TOClient{
		Client: FromServer{
			Nodes:          len(s.ListDatabaseNodes.ListDBNode),
			Command:        "infoset",
			SplitListNodes: s.Spliter4(s.ListDatabaseNodes.ListDBNode),
		},
		Worker: DBWorkerPacket{
			IDS:    "empty",
			Node:   "empty",
			Result: false,
			Error:  "empty",
		},
		ListNodes:   s.ListDatabaseNodes,
		TimerCTX:    s.cfg.TimeoutCTX,
		TimerTCP:    s.cfg.TimeoutTCP,
		TimerCTXZBD: s.cfg.TimeoutCTXZBD,
		TimerTCPZBD: s.cfg.TimeoutTCPZBD,
	}
	toWrite <- t

	//бесконечный цикл для обработки входящих от клиента и отправка ему ответов
	for {
		//читаю с клиента
		var fromClient FromClient
		err = conn.ReadJSON(&fromClient)
		if err != nil {
			//TODO: добавить анализ ошибок + добавить логику
			s.log.Printf("[error read JSON from client] %v `%v`\n", err, fromClient)
			return
		} else {
			switch fromClient.Command {

			case "start_all":
				s.log.Printf("START_ALL\n")

				s.log.Printf("STOCKER: %v\n", fromClient.Stocker)
				//исключаю из проверки выбранные на вебморде
				s.checkboxSelectAndUncheck(fromClient.Stocker)

				for q, x := range s.ListDatabaseNodes.ListDBNode {
					s.log.Printf("[ALL ELEMENT] %d--------- %v\n", q, x.Element)
				}

				//формирую список ID
				var ids []int
				for _, x := range s.ListDatabaseNodes.ListDBNode {
					// check test ismain set flag and update timers
					if x.Ismain > 0 {
						s.log.Printf("ISMAIN : %v\n", x)
					}
					if x.Element.Status == false {
						ids = append(ids, x.Filid)
					}
				}
				go s.SingleLooperADAPTIVE(chanTOTALBREAK, toWrite, ids, fromClient.Command)

			case "start_zod":
				s.log.Printf("START_ZOD\n")
				//исключаю из проверки выбранные на вебморде
				s.checkboxSelectAndUncheck(fromClient.Stocker)

				//формирую список ID
				var ids []int
				for _, x := range s.ListDatabaseNodes.ListDBNode {
					//безусловно добавляет в список ЦОДовских бд с флагом ismain, по остальным проверка регспеком
					if x.Ismain > 0 && x.Element.Status == false {
						ids = append(ids, x.Filid)
					} else {
						f, err := regexp.MatchString(s.cfg.RegexLPU, x.Dbipaddr)
						if err != nil {
							s.log.Printf("error: %v\n", err)
						} else {

							if f && x.Element.Status == false {
								ids = append(ids, x.Filid)
							}
						}
					}
				}
				//проверка на количество проверяемых серверов, если количество 0 то возврат сообщения
				if len(ids) > 0 {
					go s.SingleLooperADAPTIVE(chanTOTALBREAK, toWrite, ids, fromClient.Command)
				} else {
					//работа воркеров завершена
					t := TOClient{Client: FromServer{
						Nodes:      len(s.ListDatabaseNodes.ListDBNode),
						Command:    "end" + "start_zod",
						CountCheck: 0,
						TypeCheck:  "start_zod",
					}, Worker: DBWorkerPacket{
						IDS:    "",
						Node:   "",
						Result: false,
						Error:  "",
					}}

					//передача результата в вебсокет
					toWrite <- t
				}

			case "start_lpu":
				s.log.Printf("START_LPU\nStocker: %v\n", fromClient.Stocker)
				//исключаю из проверки выбранные на вебморде
				s.checkboxSelectAndUncheck(fromClient.Stocker)

				//формирую список ID
				var ids []int
				for _, x := range s.ListDatabaseNodes.ListDBNode {
					f, err := regexp.MatchString(s.cfg.RegexLPU, x.Dbipaddr)
					if err != nil {
						s.log.Printf("error: %v\n", err)
					} else {
						//добавляю только элементы у которых статус на вебморде "отжат" - чекбокс не выставлен, остальные пропускаю
						if f == false && x.Element.Status == false {
							ids = append(ids, x.Filid)
						}
					}
				}
				if len(ids) > 0 {
					go s.SingleLooperADAPTIVE(chanTOTALBREAK, toWrite, ids, fromClient.Command)
				} else {
					s.log.Printf("(start_lpu) -> Count IDS = %d, break\n", len(ids))

					//работа воркеров завершена
					t := TOClient{Client: FromServer{
						Nodes:      len(s.ListDatabaseNodes.ListDBNode),
						Command:    "end" + "start_lpu",
						CountCheck: 0,
						TypeCheck:  "start_zod",
					}, Worker: DBWorkerPacket{
						IDS:    "",
						Node:   "",
						Result: false,
						Error:  "",
					}}
					//передача результата в вебсокет
					toWrite <- t
				}

			case "update_timers":
				s.log.Printf("ws: update_timers: %v: %v\n", fromClient.TimerCTXZBD, fromClient.TimerTCPZBD)
				s.UpdateTimersForListNode(time.Duration(fromClient.TimerTCP), time.Duration(fromClient.TimerCTX),
					time.Duration(fromClient.TimerTCPZBD), time.Duration(fromClient.TimerCTXZBD))

				//отправка уведомления об обновлении таймеров
				t := TOClient{
					Client: FromServer{
						Nodes:   len(s.ListDatabaseNodes.ListDBNode),
						Command: "update_timers",
					},
					TimerCTX:    s.cfg.TimeoutCTX,
					TimerTCP:    s.cfg.TimeoutTCP,
					TimerCTXZBD: s.cfg.TimeoutCTXZBD,
					TimerTCPZBD: s.cfg.TimeoutTCPZBD,
				}
				toWrite <- t

			case "update_list":
				s.log.Printf("ws: update_list\n")
				s.GetListServerFromDatabase()

				//отправка уведомления о обновлении списка
				t := TOClient{
					Client: FromServer{
						Nodes:   len(s.ListDatabaseNodes.ListDBNode),
						Command: "update_list",
					},
					ListNodes: s.ListDatabaseNodes,
				}
				toWrite <- t

			case "stop_single":
				s.log.Printf("stop period or single\n")

				//останов всех горутин посредством передачи сигнала глобалменеджеру
				chanTOTALBREAK <- 0
				s.log.Printf("send `0` to chanTOTALBREAK\n")

				//отправка конструкта-уведомления о прекращении работы
				t := TOClient{Client: FromServer{
					Nodes:      len(s.cfg.NodeList),
					Command:    "end",
					CountCheck: 0,
				}, Worker: DBWorkerPacket{
					IDS:    "",
					Node:   "",
					Result: false,
					Error:  "",
				}}
				toWrite <- t
				s.log.Printf("send PACKAHGE  to `toWrite`\n")

			default:
				s.log.Printf("wrong command from client\n")
			}
		}
	}
}

//----------------------------------------------------------------------
// websocket for single form checker database (upgrader)
//-----------------------------------------------------------------------
// make new websocket instanse
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func (s *Server) HandlerWebsocketSingleFormChecker(w http.ResponseWriter, r *http.Request) {
	//переменные
	var chanTOTALBREAK = make(chan int)  // канал для прерывания работы запущенных воркеров
	var command = make(chan string)      // управляющий канал для монопольного писателя в вебсокет
	var toWrite = make(chan interface{}) // канал для передачи данных, какие надо передать в вебсо
	var countWorkers = 0                 //количество работающих задача в текущем вебсокете

	//создаю коннектор для обработки websocketa
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		s.log.Println(err)
		return
	}
	s.log.Println("connection established from client")

	//создаю и запускай горутина для монопольной записи в вебсокет
	go s.WriterWebsocket(conn, toWrite, command)

	//дефер функция
	defer func() {
		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER]\n")
		err := conn.Close()
		if err != nil {
			s.log.Printf("error close websocket `%v`\n", err)
		}
		//отправка команды горутинам на выход //при обрыве websocket соединения
		chanTOTALBREAK <- 0
	}()

	//бесконечный цикл для обработки входящих от клиента и отправка ему ответов
	for {
		if conn == nil {
			s.log.Printf("[WARNING-ERROR] WEBSOCKET IS CLOSED!!!!!\n")
		} else {
			//читаю с клиента
			var fromClient FromClientNewNode
			err = conn.ReadJSON(&fromClient)
			if err != nil {
				//TODO: добавить анализ ошибок + добавить логику
				s.log.Printf("[error read JSON from client] %v %v\n", err, fromClient)
				return
			} else {
				switch fromClient.Command {
				case "new":
					s.log.Printf("===get new node from client===\n%v\n", fromClient)
					//формирую горутину-воркера по проверке одной базы по таймеру
					//tt, _ := strconv.Atoi(fromClient.Timeout)
					//pp, _ := strconv.Atoi(fromClient.Period)
					//go s.WorkerRepeatTimerFormSingleChecker(DBWorkerFormParams{
					//	Node:        fromClient.Dbs,
					//	WorkerID:    0,
					//	chanCommand: command,        //управляющие команды
					//	chanBREAK:   chanTOTALBREAK, //досрочное прекращение обработки
					//	toWrite:     toWrite,        //канало для передачи сообщений горутине-писателю в сокет
					//	Timeout:     tt,
					//	Period:      pp,
					//	Mark:        s.Hash(),
					//})
					//увеличиваем счетчик работающих горутин
					countWorkers++

					//команда от вебморды о прекращении работы конкретной проверки
				case "kill":
					s.log.Printf("====KILL COMMAND FROM GUID => `%v`\n", fromClient.Marked)
					//отправляю всем менеджерам команду
					for x := 0; x < countWorkers; x++ {
						command <- fromClient.Marked
					}
					s.log.Printf("====KILL COMMAND FROM GUID => `%v` [MASS SENDED] \n", fromClient.Marked)

				case "stop_period", "stop_single":
					s.log.Printf("stop period or single\n")

					//останов всех горутин посредством передачи сигнала глобалменеджеру
					chanTOTALBREAK <- 0

					//отправка конструкта-уведомления о прекращении работы
					t := TOClient{Client: FromServer{
						Nodes:      len(s.cfg.NodeList),
						Command:    "end",
						CountCheck: 0,
					}, Worker: DBWorkerPacket{
						IDS:    "",
						Node:   "",
						Result: false,
						Error:  "",
					}}
					toWrite <- t

				default:
					s.log.Printf("wrong command from client\n")
				}
			}
		}
	}
}

//----------------------------------------------------------------------
// websocket multi check (обработки постоянных проверок с заданной периодичностью)
//-----------------------------------------------------------------------
func (s *Server) HandlerWebsocketMultiCheck(w http.ResponseWriter, r *http.Request) {
	//переменные
	var chanTOTALBREAK = make(chan int)  // канал для прерывания работы запущенных воркеров
	var command = make(chan string)      // управляющий канал для монопольного писателя в вебсокет
	var toWrite = make(chan interface{}) // канал для передачи данных, какие надо передать в вебсокет

	//создаю коннектор для обработки websocketa
	conn, err := upgrader1.Upgrade(w, r, nil)
	if err != nil {
		s.log.Println(err)
		return
	}
	s.log.Println("connection established from client")

	//создаю и запускаю горутину для монопольной записи в вебсокет
	go s.WriterWebsocket(conn, toWrite, command)

	//дефер функция
	defer func() {
		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER]\n")
		err := conn.Close()
		if err != nil {
			s.log.Printf("error close websocket `%v`\n", err)
		}
		//отправка команды горутинам на выход //при обрыве websocket соединения
		chanTOTALBREAK <- 0
	}()

	//
	if s.StatusMCheck.Status {

	} else {

	}

	//передача информационного блока для установки первичных значений
	t := TOClient{
		Client: FromServer{
			Nodes:                 len(s.ListDatabaseNodes.ListDBNode),
			NodesChecked:          len(s.ListDatabaseNodes.ListDBNodeChecked),
			Command:               "infoset",
			SplitListNodes:        s.Spliter4(s.ListDatabaseNodes.ListDBNode),
			SplitListNodesChecked: s.Spliter4(s.ListDatabaseNodes.ListDBNodeChecked),
		},
		Worker: DBWorkerPacket{
			IDS:    "empty",
			Node:   "empty",
			Result: false,
			Error:  "empty",
		},
		ListNodes:        s.ListDatabaseNodes,
		TimerCTX:         s.cfg.TimeoutCTX,
		TimerTCP:         s.cfg.TimeoutTCP,
		TimerCTXZBD:      s.cfg.TimeoutCTXZBD,
		TimerTCPZBD:      s.cfg.TimeoutTCPZBD,
		TimerMultiCheck:  s.cfg.TimeoutMultiCheck,
		StatusMultiCheck: s.StatusMCheck.Status,
	}
	toWrite <- t

	//бесконечный цикл для обработки входящих от клиента и отправка ему ответов
	for {
		//читаю с клиента
		var fromClient FromClient
		err = conn.ReadJSON(&fromClient)
		if err != nil {
			//TODO: добавить анализ ошибок + добавить логику
			s.log.Printf("[error read JSON from client] %v\n", err)
			return
		} else {
			switch fromClient.Command {

			case "start":
				//проверка уже запущенных проверок
				if s.StatusMCheck.Status {
					s.log.Printf("[multicheck] ПРОВЕРКА УЖЕ ЗАПУЩЕНА и РАБОТАЕТ\n")
					//TODO: отправляю уведомление клиентской части

				} else {
					s.log.Printf("START_ALL\n")
					s.log.Printf("From client: %v: %T\n", fromClient.Timer, fromClient.Timer)
					fmt.Printf("FROMClient..[STOCKER]: %d \n", len(fromClient.Stocker))

					//исключаю из проверки выбранные на вебморде
					s.checkboxSelectAndUncheck(fromClient.Stocker)
					s.countRL()

					//формирую список ID
					var (
						ids         []int
						checked     []*DatabaseNode
						idsNotCheck []int
					)

					for _, x := range s.ListDatabaseNodes.ListDBNode {
						if x.Element.Status == false {
							ids = append(ids, x.Filid)
							checked = append(checked, x)
						} else {
							idsNotCheck = append(idsNotCheck, x.Filid)
						}
					}

					//сохраняю список отмеченных серверов ддя проверки
					s.ListDatabaseNodes.Lock()
					s.ListDatabaseNodes.ListDBNodeChecked = checked
					s.ListDatabaseNodes.Unlock()

					s.log.Printf("[multicheck] count servers:countChecked(false) ==  %d\ncountIsNoTcheck(true): %d\n", len(ids), len(idsNotCheck))
					go s.MultiCheckADAPTIVE(chanTOTALBREAK, toWrite, ids, "", s.cfg.TimeoutMultiCheck)

					//изменяю статус
					s.StatusMCheck.Enable()
				}

			case "update_timers":
				s.log.Printf("ws: update_timers\n")

				s.UpdateTimersForListNode(time.Duration(fromClient.TimerTCP), time.Duration(fromClient.TimerCTX),
					time.Duration(fromClient.TimerTCPZBD), time.Duration(fromClient.TimerCTXZBD))

				//отправка уведомления о обновлении таймеров
				t := TOClient{
					Client: FromServer{
						Nodes:   len(s.ListDatabaseNodes.ListDBNode),
						Command: "update_timers",
					},
					TimerCTX:    s.cfg.TimeoutCTX,
					TimerTCP:    s.cfg.TimeoutTCP,
					TimerCTXZBD: s.cfg.TimeoutCTXZBD,
					TimerTCPZBD: s.cfg.TimeoutTCPZBD,
				}
				toWrite <- t

			case "stop":
				s.log.Printf("stop multi check server\n")

				s.StatusMCheck.Status = false

				//проверка наличия запущенного процесса по обработке проверки серверов
				if s.StatusMCheck.Status {
					////останов всех горутин посредством передачи сигнала глобалменеджеру
					chanTOTALBREAK <- 0
					s.StatusMCheck.Status = false
				} else {
					s.log.Println("--Нет запущенных процессов для остановки--")
				}

				////отправка конструкта-уведомления о прекращении работы
				//t := TOClient{Client: FromServer{
				//	Nodes:      len(s.cfg.NodeList),
				//	Command:    "end",
				//	CountCheck: 0,
				//}, Worker: DBWorkerPacket{
				//	IDS:    "",
				//	Node:   "",
				//	Result: false,
				//	Error:  "",
				//}}
				//toWrite <- t

			default:
				s.log.Printf("wrong command from client\n")
			}
		}
	}
}

//----------------------------------------------------------------------
// вебсокет для формирования и запуска рабочих по таймеру для проверки бд
//-----------------------------------------------------------------------
// make new websocket instanse
var upgrader3 = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func (s *Server) WebsocketTimerSingleConfig(w http.ResponseWriter, r *http.Request) {
	//переменные
	var command = make(chan string)      // управляющий канал для монопольного писателя в вебсокет
	var toWrite = make(chan interface{}) // канал для передачи данных, какие надо передать в вебсо

	//создаю коннектор для обработки websocketa
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		s.log.Println(err)
		return
	}
	s.log.Println("connection established from client")

	//создаю и запускай горутина для монопольной записи в вебсокет
	go s.WriterWebsocket(conn, toWrite, command)

	//дефер функция
	defer func() {
		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER]\n")
		err := conn.Close()
		if err != nil {
			s.log.Printf("error close websocket `%v`\n", err)
		}
	}()

	//передача информационного блока для установки первичных значений
	var keys []string
	for k, _ := range s.stockTimerSingle {
		keys = append(keys, k)
	}
	t := TOClient{Client: FromServer{
		Nodes:    len(s.stockTimerSingle),
		Command:  "infoset",
		ListKeys: keys,
	}, Worker: DBWorkerPacket{
		IDS:    "empty",
		Node:   "empty",
		Result: false,
		Error:  "empty",
	}}
	toWrite <- t

	//бесконечный цикл для обработки входящих от клиента и отправка ему ответов
	for {
		//читаю с клиента
		var fromClient FromClientNewNode
		err = conn.ReadJSON(&fromClient)
		if err != nil {
			//TODO: добавить анализ ошибок + добавить логику
			s.log.Printf("[error read JSON from client] %v %v\n", err, fromClient)
			return
		} else {
			switch fromClient.Command {
			case "new":
				//формирую горутину-воркера по проверке одной базы по таймеру
				tt, _ := strconv.Atoi(fromClient.Timeout)
				pp, _ := strconv.Atoi(fromClient.Period)
				go s.WorkerRepeatTimerFormSingleChecker(DBWorkerFormParams{
					Node:    fromClient.Dbs,
					Timeout: tt,
					Period:  pp,
					Mark:    s.Hash(),
				}, s.chanCommand, s.chanEnd, s.chanWork)

				//сообщаю о количестве работающих воркеров
				var keys []string
				for k, _ := range s.stockTimerSingle {
					keys = append(keys, k)
				}
				t := TOClient{Client: FromServer{
					Nodes:    len(s.stockTimerSingle),
					Command:  "infoset",
					ListKeys: keys,
				}, Worker: DBWorkerPacket{
					IDS:    "empty",
					Node:   "empty",
					Result: false,
					Error:  "empty",
				}}
				toWrite <- t
				//прибить активный воркер
			case "kill":
				s.log.Printf("KILL WORKER: %v\n", fromClient.Marked)
				//передача команды глобальному управленцу, который `finish him` воркера
				s.chanHandler <- GlobalHandler{
					Command: "kill",
					Marked:  fromClient.Marked,
				}

			default:
				s.log.Printf("wrong command from client\n")
			}
		}
	}
}

//----------------------------------------------------------------------
// вебсокет для отображения результатов работы воркеров в режиме реального времени
//-----------------------------------------------------------------------
// make new websocket instanse
var upgrader4 = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func (s *Server) WebsocketTimerSingleResult(w http.ResponseWriter, r *http.Request) {
	//переменные
	var command = make(chan string)      // управляющий канал для монопольного писателя в вебсокет
	var toWrite = make(chan interface{}) // канал для передачи данных, какие надо передать в вебсо

	//создаю коннектор для обработки websocketa
	conn, err := upgrader4.Upgrade(w, r, nil)
	if err != nil {
		s.log.Println(err)
		return
	}
	s.log.Println("connection established from client")

	//создаю и запускай горутина для монопольной записи в вебсокет
	go s.WriterWebsocket(conn, toWrite, command)

	//дефер функция
	defer func() {
		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER]\n")
		err := conn.Close()
		if err != nil {
			s.log.Printf("error close websocket `%v`\n", err)
		}
	}()

	//бесконечный цикл для обработки входящих от клиента и отправка ему ответов
	for {
		//читаю с клиента
		var fromClient FromClient
		err = conn.ReadJSON(&fromClient)
		if err != nil {
			//TODO: добавить анализ ошибок + добавить логику
			s.log.Printf("[error read JSON from client] %v %v\n", err, fromClient)
			return
		} else {
			switch fromClient.Command {
			//обновить информацию по работающим воркерам
			case "getinfo":
				res := s.StockSplitResult()
				tmp := WebPrint{
					Workers: len(s.stockTimerSingle),
					Stock:   res,
				}
				if s.cfg.Debugcheckdb {
					s.log.Printf("[websocket] GETINFO SEND: %v\n", tmp)
				}


				toWrite <- tmp

			default:
				s.log.Printf("wrong command from client\n")
			}
		}
	}
}

//----------------------------------------------------------------------
// вебсокет для коммуникации в части проверки бэкапов
//-----------------------------------------------------------------------
// make new websocket instanse
var wsbackup = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func (s *Server) WebsocketBackupSingle(w http.ResponseWriter, r *http.Request) {
	//переменные
	var command = make(chan string)      // управляющий канал для монопольного писателя в вебсокет
	var toWrite = make(chan interface{}) // канал для передачи данных, какие надо передать в вебсо

	//создаю коннектор для обработки websocketa
	conn, err := wsbackup.Upgrade(w, r, nil)
	if err != nil {
		s.log.Println(err)
		return
	}
	s.log.Println("connection established from client")

	//создаю и запускай горутина для монопольной записи в вебсокет
	go s.WriterWebsocket(conn, toWrite, command)

	//дефер функция
	defer func() {
		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER]\n")
		err := conn.Close()
		if err != nil {
			s.log.Printf("error close websocket `%v`\n", err)
		}
	}()

	//передача информационного блока для установки первичных значений
	t := BackupWS{
		Command:      "infoset",
		WorkDir:      s.cfg.WorkDir,
		ReportFile:   s.cfg.ReportFile,
		Databases:    s.cfg.Databases,
		LogFile:      s.cfg.LogFile,
		Debug:        s.cfg.Debug,
		Production:   s.cfg.Production,
		CounDatabase: len(s.BackupPlugin.Stocker),
		DumpFile:     s.cfg.BackupConfigPathFile,
	}
	toWrite <- t

	//бесконечный цикл для обработки входящих от клиента и отправка ему ответов
	for {
		//читаю с клиента
		var fromClient BackupWS
		err = conn.ReadJSON(&fromClient)
		if err != nil {
			//TODO: добавить анализ ошибок + добавить логику
			s.log.Printf("[error read JSON from client] %v %v\n", err, fromClient)
			return
		} else {
			switch fromClient.Command {
			case "check":
				//формирую горутину-воркера по проверке бэкапа разово
				go s.BackupPlugin.WebSingleCheck(toWrite)
			case "period":
				fmt.Printf("PERIOD COMMAND\nRes: %v\n", fromClient)
				go s.BackupPlugin.UpdateConvertAndUpdater(fromClient.Result)
			case "filter":
				fmt.Printf("Filter command from websocket accepted   %v\n", fromClient.FilterStatus)
				go s.BackupPlugin.FilterData(toWrite, fromClient.FilterStatus)

			default:
				s.log.Printf("wrong command from client\n")
			}
		}
	}
}

//----------------------------------------------------------------------
// WEBSOCKET TESTER HANDLER MULTI CONNECTIONS HANDLER
//-----------------------------------------------------------------------
// make new websocket instanse
var tester = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func (s *Server) WebsocketTester(w http.ResponseWriter, r *http.Request) {
	//переменные
	var command = make(chan string)      // управляющий канал для монопольного писателя в вебсокет
	var toWrite = make(chan interface{}) // канал для передачи данных, какие надо передать в вебсо

	//создаю коннектор для обработки websocketa
	conn, err := tester.Upgrade(w, r, nil)
	if err != nil {
		s.log.Println(err)
		return
	}
	s.log.Println("-- connection established from client-- ")

	//создаю и запускай горутина для монопольной записи в вебсокет
	go s.WriterWebsocket(conn, toWrite, command)

	//дефер функция
	defer func() {
		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER]\n")
		err := conn.Close()
		if err != nil {
			s.log.Printf("error close websocket `%v`\n", err)
		}
	}()

	//передача информационного блока для установки первичных значений
	t := TOClient{Client: FromServer{
		Nodes:   len(s.stockTimerSingle),
		Command: "infoset",
	}, Worker: DBWorkerPacket{
		IDS:    "empty",
		Node:   "empty",
		Result: false,
		Error:  "empty",
	}}
	toWrite <- t

	//бесконечный цикл для обработки входящих от клиента и отправка ему ответов
	for {
		//читаю с клиента
		var fromClient FromClientNewNode
		err = conn.ReadJSON(&fromClient)
		if err != nil {
			//TODO: добавить анализ ошибок + добавить логику
			s.log.Printf("[error read JSON from client] %v %v\n", err, fromClient)
			return
		} else {
			switch fromClient.Command {
			case "remd":
				s.log.Println("======SOCKER: REMD COMMAND")
				//сообщаю о количестве работающих воркеров
				var keys []string
				for k, _ := range s.stockTimerSingle {
					keys = append(keys, k)
				}
				t := BaseMSG{
					Command: "remd",
					Body:    "simple body",
				}
				toWrite <- t
				//прибить активный воркер
			case "imek":
				s.log.Println("======SOCKER: IMEK  COMMAND")
				go s.WorkerIMEK(toWrite)

				//t := TOClient{Client: FromServer{
				//	Nodes:    len(s.stockTimerSingle),
				//	Command:  "imek",
				//}, Worker: DBWorkerPacket{
				//	IDS:    "empty",
				//	Node:   "empty",
				//	Result: false,
				//	Error:  "empty",
				//}}
				//toWrite <- t

			default:
				s.log.Printf("wrong command from client\n")
			}
		}
	}
}

//----------------------------------------------------------------------
// WEBSOCKET REMD + IMEK + USERS + RPN
//-----------------------------------------------------------------------
// make new websocket instanse
var fun = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func (s *Server) WebsocketFu(w http.ResponseWriter, r *http.Request) {
	//переменные
	var command = make(chan string)      // управляющий канал для монопольного писателя в вебсокет
	var toWrite = make(chan interface{}) // канал для передачи данных, какие надо передать в вебсо

	//создаю коннектор для обработки websocketa
	conn, err := fun.Upgrade(w, r, nil)
	if err != nil {
		s.log.Println(err)
		return
	}
	s.log.Println("-- connection established from client-- ")
	s.log.Printf("Current URL: %v\n", r.URL.Path)

	//создаю и запускаю горутину для монопольной записи в вебсокет
	go s.WriterWebsocket(conn, toWrite, command)

	//дефер функция
	defer func() {
		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER]\n")
		err := conn.Close()
		if err != nil {
			s.log.Printf("error close websocket `%v`\n", err)
		}
	}()

	//передача информационного блока для установки первичных значений
	t := TOClient{Client: FromServer{
		Nodes:   len(s.stockTimerSingle),
		Command: "infoset",
	}, Worker: DBWorkerPacket{
		IDS:    "empty",
		Node:   "empty",
		Result: false,
		Error:  "empty",
	}}
	toWrite <- t

	//бесконечный цикл для обработки входящих от клиента и отправка ему ответов
	for {
		//читаю с клиента
		var fromClient FromClientNewNode
		err = conn.ReadJSON(&fromClient)
		if err != nil {
			//TODO: добавить анализ ошибок + добавить логику
			s.log.Printf("[error read JSON from client] %v %v\n", err, fromClient)
			//t := BaseMSG{
			//	Command: "errorJSON",
			//	Body:    err.Error(),
			//}
			//toWrite <- t
			//continue
			return
		} else {
			switch fromClient.Command {

			case "rpn":
				s.log.Printf("---socket rpn command")
				t := BaseMSG{
					Command: "rpn",
					Body:    "simple body",
				}
				toWrite <- t

			case "user":
				s.log.Printf("---socket user command")
				t := BaseMSG{
					Command: "user",
					Body:    "simple body",
				}
				toWrite <- t
			case "remd":
				s.log.Println("======SOCKER: REMD COMMAND")
				go s.WorkerREMD(toWrite)

			case "imek":
				s.log.Println("======SOCKER: IMEK  COMMAND")
				go s.WorkerIMEK(toWrite)
			case "stat":
				s.log.Println("======SOCKET: STAT COMMAND==========")
				continue
			default:
				s.log.Printf("wrong command from client\n")
			}
		}
	}
}

package library

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	_ "github.com/nakagami/firebirdsql"
	"gitlab.com/Spouk/datastruct/fifo"
	"gitlab.com/Spouk/gotool/config"
	"gitlab.com/Spouk/gotool/convert"
	"gitlab.com/Spouk/gotool/render"
	"gitlab.com/Spouk/gotool/session"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"sort"
	"strings"
	"sync"
	"syscall"
	"time"
)

const (
	logPrefixServer  = "[work-web-portal-log]"
	logServerBitMask = log.LstdFlags | log.Lshortfile
)

type Server struct {
	Mux     *chi.Mux
	log     *log.Logger
	R       *render.Render
	cfg     *ConfigStruct
	Pool    sync.Pool
	Convert *convert.Convert
	// функционал в виде плагинов
	DbChecker *WorkDBSChecker

	//каналы для отслеживания прерываний
	sigs chan os.Signal
	done chan bool

	//каналы для глобального управленца всеми работами
	chanCommand chan GlobalCommandToWorker //управляющий канал между менеджером и рабочими
	chanEnd     chan string                //канал для завершения работы всех работающих воркеров
	chanWork    chan DBWorkerFormParams    //канал для приема результатов работ
	chanHandler chan GlobalHandler         //канал для обмена между хэндлерами и глобальным управленцем

	//структура для накопления результатов работы воркеров по одиночной проверки по таймеру
	stockTimerSingle map[string]DBWorkerFormParams

	//проверка бэкапов
	BackupPlugin *BackupChecker //инстанс проверки бэкапов на протухлость

	//список бд-структур полученный выборкой из бд
	ListDatabaseNodes *ListDatabaseNode

	//статус запущенной постоянной проверки
	StatusMCheck *StatusMultiCheck

	//структура fifo для ведения логов запросов в раме на период работы приложения
	FifoQueue *fifo.Fifo

	//временной бокс для хранения промежуточных результатов проверок IMEK + REMD
	SaverFunctions *SaveResultFunctions

	//pool for sessions
	Pool sync.Pool
}

//----------------------------------------------------------------------
// глобальный менеджер управленец
//-----------------------------------------------------------------------
func (s *Server) GlobalManager() {
	s.log.Printf("GlobalManager starting...\n")
	defer func() {
		s.log.Printf("GlobalManager end work...\n")
	}()
	for {
		select {

		//канал для обмена между хэндлерами и глобальным управленцем
		case cc := <-s.chanHandler:
			switch cc.Command {
			case "kill":
				//отправляю команду на прекращения работы воркера
				for x := 0; x < len(s.stockTimerSingle); x++ {
					s.chanCommand <- GlobalCommandToWorker{
						Command: "endwork",
						Marked:  cc.Marked,
					}
				}
				//удаляю все результаты после прекращения работы воркера
				if _, ok := s.stockTimerSingle[cc.Marked]; ok {
					delete(s.stockTimerSingle, cc.Marked)
				}
			}
		//канал для приема результатов работы воркеров для аккумуляции и отображения по мере необходимости на странице результатов
		case w := <-s.chanWork:
			s.stockTimerSingle[w.Mark] = w
			if s.cfg.Debugcheckdb {
				s.log.Printf("[GLOBAL MANAGER] from chanWork: %v\n", w, s.stockTimerSingle)
			}

		//канал для приема сообщений о прекращении работы нижестоящих менеджеров-воркеров
		case e := <-s.chanEnd:
			//удаляю все результаты после прекращения работы воркера
			if _, ok := s.stockTimerSingle[e]; ok {
				delete(s.stockTimerSingle, "e")
			}
		}
	}
}

//создание нового инстанса
func NewServer(configFile string) *Server {

	//читаю конфигурационный файл
	cf := ConfigStruct{}
	c := config.NewConf("", os.Stdout)
	err := c.ReadConfig(configFile, &cf)
	if err != nil {
		log.Fatal(err)
	}

	//создаю инстанс сервера
	var s = &Server{
		log: log.New(os.Stdout, logPrefixServer, logServerBitMask),
		R:   render.NewRender(cf.TemplatePath, cf.TemplateDebug, nil, cf.TemplateDebugFatal),
		Mux: chi.NewMux(),
		cfg: &cf,
	}

	//определение пула для сессий
	s.Pool = sync.Pool{
		New: func() interface{} {
			return session.New()
		},
	}

	//добавляю миддлы
	s.Mux.Use(middleware.Logger)
	s.Mux.Use(middleware.Recoverer)
	s.Mux.Use(middleware.StripSlashes)
	s.Mux.Use(s.MiddlewareSession)

	//статичные файлы
	s.Mux.Route("/static/", func(root chi.Router) {
		//формирую данные исходя из конфига
		//workDir, _ := os.Getwd()
		//c.Log.Printf("GETWD: %v\n", workDir)
		filesDir := filepath.Join(s.cfg.StaticPath, "static")
		s.log.Printf("===> STATIC PATH: %v\n", filesDir)
		s.FileServer(root, "/static", "/", http.Dir(filesDir))
	})

	//создаю конвертер
	s.Convert = convert.NewConverter(s.log)

	//404Error
	s.Mux.NotFound(s.Handler404)

	//405Error
	s.Mux.MethodNotAllowed(s.Handler405)

	//создание dbchecker
	s.DbChecker = NewDBSChecker(s.cfg)

	//создание стока под результаты
	s.stockTimerSingle = make(map[string]DBWorkerFormParams)

	//создагие каналов
	s.chanCommand = make(chan GlobalCommandToWorker)
	s.chanEnd = make(chan string)
	s.chanWork = make(chan DBWorkerFormParams)
	s.chanHandler = make(chan GlobalHandler)

	//запуск глобалуправленца
	go s.GlobalManager()

	//создание инстанса проверки бэкапов
	s.BackupPlugin = NewBackupChecker(s.cfg)

	//делаю подключение к базе данных
	s.ListDatabaseNodes = new(ListDatabaseNode)
	//s.GetListServerFromDatabase()
	s.GetListServerFromDatabaseTest()

	//создание инстанса для изменения статуса проверки
	s.StatusMCheck = new(StatusMultiCheck)

	//создание механизма обработки прерываний приложения
	s.sigs = make(chan os.Signal, 1)
	s.done = make(chan bool, 1)
	signal.Notify(s.sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP, syscall.SIGKILL, syscall.SIGSTOP)
	go s.catcherSigKiller()
	go s.theEnd()

	//создание Fifo
	s.FifoQueue = fifo.NewFifo(100, true)

	//временный бокс для хранения проежуточных результатов проверки
	s.SaverFunctions = &SaveResultFunctions{
		REMD:    []ResultRemd{},
		IMEK:    []ResultImek{},
		RWMutex: sync.RWMutex{},
	}

	//определение пула для сессий
	s.Pool = sync.Pool{
		New: func() interface{} {
			return session.New()
		},
	}

	//возврат инстанса
	return s
}

func (s *Server) theEnd() {
	<-s.done
	s.log.Printf("-- удачи и успехов --\n")
	os.Exit(1)
}

//перехват прерываний
func (s *Server) catcherSigKiller() {
	//поймали прерывание, завершаем все делишки
	sig := <-s.sigs
	s.log.Printf("\n\n[WARNING] пойман сигнал прерывания работы сервера `%v`,  что не есть хорошо!, подождите немного пока я корректно завершу работу, ок? я так и думал... \n\n", sig)

	err := s.BackupPlugin.SaveToGob(s.cfg.BackupConfigPathFile, s.BackupPlugin.StockFilialDump)
	if err != nil {
		s.BackupPlugin.Log.Printf("[error] ошибка сохранения дампа конфига, причина =  `%v`\n", err)
	} else {
		s.BackupPlugin.Log.Printf("[success] дамп  конфиг успешно сохранен\n")
	}
	time.Sleep(time.Second * 2)
	//отправляю сигнал об успешной обработке выхода
	s.done <- true
	return
}

//статичные файлы
func (s *Server) FileServer(r chi.Router, basePath string, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	fs := http.StripPrefix(basePath+path, http.FileServer(root))

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}

//запуск сервера без сертификатов
func (s *Server) Run() {
	s.log.Printf("starting server on `%s`\n", s.cfg.AdressHTTP)
	s.log.Fatal(http.ListenAndServe(s.cfg.AdressHTTP, s.Mux))
}

func (s *Server) getContext(r *http.Request) *session.Session {
	ses, foundSession := r.Context().Value("session").(*session.Session)
	if foundSession {
		//добавление в сессию нужных переменных
		ccc := chi.RouteContext(r.Context())

		//текстовые
		ses.SetTEXT("request", "routepath", ccc.RoutePath)
		ses.SetTEXT("request", "routemethod", ccc.RouteMethod)
		ses.SetTEXT("request", "routepatern", ccc.RoutePattern())
		ses.SetTEXT("request", "urlparams", ccc.URLParams)

		//остальное
		ses.SetDATA("stock", "config", s.cfg)
		ses.SetDATA("stock", "command", ccc.URLParam("command"))
		ses.SetDATA("stock", "id", ccc.URLParam("id"))
		ses.SetDATA("stock", "idint", s.Convert.DirectStringtoInt(ccc.URLParam("id")))

	} else {
		s.log.Println("ОШИБКА -- СЕССИЯ НЕ НАЙДЕНА В ТЕКУЩЕМ КОНТЕКСТЕ\n")
	}
	return ses
}

//формирует результат для выдачи на страницу результа
//убирать положительную выдачу
//делать выборку всех случаев, когда сервер недоступен
func (s *Server) StockSplitResult() []DBWorkerFormParams {
	result := []DBWorkerFormParams{}
	for _, v := range s.stockTimerSingle {
		_, res := s.sortResult(v)
		v.WorkerPacket = res
		result = append(result, v)
	}
	//sort
	tmpStr := []string{}
	for _, x := range result {
		tmpStr = append(tmpStr, x.Mark)
	}
	sort.Strings(tmpStr)

	resTmp := []DBWorkerFormParams{}
	for _, x := range tmpStr {
		for _, j := range result {
			if j.Mark == x {
				resTmp = append(resTmp, j)
			}
		}
	}
	return resTmp
}

//чекает на предмет "плохих" реквестов весь список
func (s *Server) sortResult(r DBWorkerFormParams) (flag bool, res []DBWorkerPacket) {
	if len(r.WorkerPacket) >= 1 {
		e := r.WorkerPacket[len(r.WorkerPacket)-1]
		if e.Result == false {
			res = r.WorkerPacket[len(r.WorkerPacket)-1:]
			flag = false
		} else {
			res = []DBWorkerPacket{}
			flag = true
		}
	} else {
		res = []DBWorkerPacket{}
		flag = true
	}
	return
}

//разбивает список на 4 колонки
func (s *Server) SplitSlice(a []*DatabaseNode, count int) [][]*DatabaseNode {
	fs := len(a) % count
	repeat := len(a) / count
	if fs > 0 {
		repeat++
	}
	itogo := make([][]*DatabaseNode, repeat)
	for x := 0; x < repeat; x++ {
		if repeat-1 == x {
			itogo[x] = a[x*count:]
		} else {
			itogo[x] = a[x*count : x*count+count]
		}
	}
	return itogo
}
func (s *Server) Spliter4(a []*DatabaseNode) [][]*DatabaseNode {
	//создаю 4 выходных контейнера
	itogo := make([][]*DatabaseNode, 4)
	fs := len(a) % 4
	countMedian := len(a) / 4 //получение целой части
	flag := false
	//есть остаток
	if fs > 0 {
		flag = true
	}

	if len(a) < 4 {
		//если количество элементов в слайсе меньше 4 то добавляю все в первый контейнер
		itogo[0] = append(itogo[0], a...)
	} else {
		//провожу количество итераций = количеству емкостей для разлива входного списка
		for x := 0; x < 4; x++ {
			itogo[x] = append(itogo[x], a[x*countMedian:x*countMedian+countMedian]...)
		}
		if flag {
			//есть остаток, надо раскидать по слайсам, в данном случае остаток всегда будет меньше 4
			//посему, раскидываю начиная с 0 контейнера
			ostatok := a[countMedian*4:]
			for x := 0; x < len(ostatok); x++ {
				itogo[x] = append(itogo[x], ostatok[x])
			}
		}
	}
	s.log.Printf("INPUT: %v\n\nOUTPUT: %v\n\n", a, itogo)
	return itogo
}

package library

import (
	"github.com/gorilla/websocket"
	"sync"
	"time"
)

type (
	//----------------------------------------------------------------------
	// конфигурация
	//-----------------------------------------------------------------------
	ConfigStruct struct {
		//template
		TemplatePath       string        `yaml:"templatepath"`
		TemplateDebug      bool          `yaml:"templatedebug"`
		TemplateDebugFatal bool          `yaml:"teamplatedebugfatal"`
		ReadTimeout        time.Duration `yaml:"readtimeout"`
		WriteTimeout       time.Duration `yaml:"writetimeout"`

		//regex for split range dbs checker
		RegexLPU string `yaml:"reglpu"`

		//key for switch developing/production mode
		Debugcheckdb bool `yaml:"debugcheckdb"`
		Developing   bool `yaml:"developing"`

		//serverjs developing
		ServerJSDEV   string `yaml:"serverjsdev"`
		ServerJSWSDEV string `yaml:"serverjswsdev"`

		//serverjs
		ServerJS   string `yaml:"serverjs"`
		ServerJSWS string `yaml:"serverjsws"`

		//server
		AdressHTTP            string `yaml:"adresshttp"`
		RedirectTrailingSlash bool   `yaml:"redirecttrailingslash"`
 		RedirectFixedPath     bool   `yaml:"redirectfixedpath"`

		//Logging -> статичный сегмент
		Logfile      string `yaml:"logfile"`
		LogTagsyslog string `yaml:"logtagsyslog"`
		LogPrefix    string `yaml:"logprefix"`

		//files
		StaticPath     string        `yaml:"staticpath"`
		StaticPrefix   string        `yaml:"staticprefix"`
		FileTimerSleep time.Duration `yaml:"filetimersleep"`
		UploadPath     string        `yaml:"uploadpath"`

		//dbschecker config section
		NodeList      []string      `yaml:"nodelist"`       //список серверов для опроса
		NodeTicker    time.Duration `yaml:"nodeticker"`     // интервал опроса
		TimeoutCTX    time.Duration `yaml:"timeoutctx"`     //таймаут в секундах для проверки "зависиших баз" `longtime query`
		TimeoutTCP    time.Duration `yaml:"timeouttcp"`     //таймаут в секундах для проверки доступности хоста с портом
		TimeoutCTXZBD time.Duration `yaml:"timeoutctx_zbd"` //таймаут в секундах для проверки "зависиших баз" `longtime query` ЦБД
		TimeoutTCPZBD time.Duration `yaml:"timeouttcp_zbd"` //таймаут в секундах для проверки доступности хоста с портом ЦБД

		//backup checker
		CheckPeriod          int           `yaml:"checkperiod"`
		WorkDir              string        `yaml:"workdir"`
		ReportFile           string        `yaml:"reportfile"`
		Databases            []profileFile `yaml:"databases"`
		Production           bool          `yaml:"production"`
		LogFile              string        `yaml:"logfilebackup"`
		Debug                bool          `yaml:"debug"`
		Correction           int           `yaml:"correction"`
		BackupConfigEnable   bool          `yaml:"backup_config_enable"`      //флаг статуса включение/выключения использования динамического конфига
		BackupConfigPathFile string        `yaml:"backup_config_path"`        //путь к дампу динамического конфига
		BackupConfigDBList   string        `yaml:"backup_config_server_list"` //адрес сервера к которому следует делать запрос на получение списка серверов

		//выборка списка bd из базы
		DBReqAuth     string `yaml:"dbreq_auth"`
		DBReqSQL      string `yaml:"dbreq_sql"`
		DBReqUSER     string `yaml:"dbreq_user"`
		DBReqPASSWORD string `yaml:"dbreq_password"`

		//multicheck
		DebugMulticheck     bool          `yaml:"debugmulticheck"`    //выставление флага задает подробный вывод для отладки
		TimeoutMultiCheck   time.Duration `yaml:"timeoutmulticheck"`  //период осуществления проверки серверов для постоянных проверок, в минутах, через веб морду можно поменять
		DBMulticheckAuth    string        `yaml:"db_multicheck_auth"` //строка для подключения к 161 базе данных для обновления таблицы filialstate
		DBSMultiStatusOK    int           `yaml:"db_filialstate_status_ok"`
		DBSMultiStatusERROR int           `yaml:"db_filialstate_status_error"`

		//db logger
		DBLoggerAUTH string `yaml:"db_logger_auth"` //авторизация на сервер логирования проверок

		//REMD + IMEK
		DbsserviceDB       string `yaml:"dbsservice_db"`
		DbsserviceUser     string `yaml:"dbsservice_user"`
		DbsservicePassword string `yaml:"dbsservice_password"`
		RemdSqlRequest     string `yaml:"remd_sql_request"`
		IemkSqlRequest     string `yaml:"imek_sql_request"`
	}
	//SELECT f.filid, f.dbpath, f.shortname, f.fullname, f.dbpath, f.dbipaddr, f.dbport, f.ismain  FROM filials f
	//краткая структура для выборки из базы списка, поля структуры определяют порядок выборки из базы данных + доп.опции
	DatabaseNode struct {
		//for select
		Filid     int    `json:"filid"`
		Dbpath    string `json:"dbpath"`
		Shortname string `json:"shortname"`
		Fullname  string `json:"fullname"`
		Dbipaddr  string `json:"dbipaddr"`
		Dbport    int    `json:"dbport"`
		Ismain    int    `json:"ismain"`
		//for work
		Element Element `json:"element"` //\>
		//Enabled bool   `json:"enabled"` //включена/выключена ли нода для проверки через веб морду
		//IDS     string `json:"ids"`     //идентификатор элемента на странице
		//for int application
		SqlRequest string        `json:"SqlRequest"` //for ex: "sysdba:sysdba@localhost:3055/employee"
		TimeoutCTX time.Duration `json:"timeoutctx"` //таймаут в секундах для проверки "зависиших баз" `longtime query`
		TimeoutTCP time.Duration `json:"timeouttcp"` //таймаут в секундах для проверки доступности хоста с портом
	}
	ListDatabaseNode struct {
		TimeUpdate time.Time       `json:"timeupdate"` //время формирования списка
		ListDBNode []*DatabaseNode `json:"listdbnode"` //сам список
		//параметры структуры, которые нужны для отслеживания состояния
		CountIteration    int             `json:"count_iteration"`      //количество итераций
		CountChecked      int             `json:"count_checked"`        //количество проверенных
		ListDBNodeChecked []*DatabaseNode `json:"list_db_node_checked"` //список серверов, которые выбраны через вебморду
		TimeLastChecked   time.Time       `json:"time_last_checked"`    //время последней проверки
		sync.RWMutex
	}
	//структура для коммуникации по вебсокету в части проверки бэкапов
	BackupWS struct {
		Command       string         `json:"command"`
		WorkDir       string         `json:"work_dir"`
		ReportFile    string         `json:"report_file"`
		Databases     []profileFile  `json:"databases"`
		LogFile       string         `json:"log_file"`
		Debug         bool           `json:"debug"`
		Production    bool           `json:"production"`
		Correction    int            `json:"correction"`
		CounDatabase  int            `json:"count_database"`  //количество бд полученных из таблицы filials = []*Filials
		DumpFile      string         `json:"dump_file"`       //путь к дампу конфига
		Result        []UpdateResult `json:"result"`          //обновление периодов
		FilterStatus  bool           `json:"filter"`          //статус фильтра по команде фильтрации
		FilterCountDB int            `json:"filter_count_db"` //количество баз в фильтрованном списке
	}
	UpdateResult struct {
		Filid     string `json:"Name"`
		NewPeriod string `json:"value"`
	}
	//формирование ответа после проверки бэкапов
	BackupWSResult struct {
		Command string        `json:"command"`
		Result  []profileFile `json:"result"`
	}
	BackupWSResultFilial struct {
		Command      string          `json:"command"`
		Result       map[int]*Filial `json:"result"`
		FilterCount  int             `json:"filter_count_db"`
		FilterResult bool            `json:"filter_result"`
	}
	CheckEachPrefix struct {
		Msg string `json:"result"`
	}
	//----------------------------------------------------------------------
	// ajax
	//-----------------------------------------------------------------------
	Answer struct {
		Stage string             `json:"stage"` //статус событийной цепочки - начало, в процессе, конечный результат
		T     []TesterAnswerAjax `json:"t"`
	}
	TesterAnswerAjax struct {
		IDS    int    `json:"ids"`    // идентификатор горутины
		Name   string `json:"Name"`   // имя горутины
		URL    string `json:"url"`    //url запроса
		Status string `json:"status"` //статус запроса (start, success, fail, not response)
	}
	//----------------------------------------------------------------------
	// websocket
	//-----------------------------------------------------------------------
	ConnWS struct {
		mu   sync.Mutex
		Conn *websocket.Conn //
	}
	FromClient struct {
		Status      string `json:"status"`
		Command     string `json:"command"`
		ListNode    []int  `json:"listnode"`
		TimerCTX    int    `json:"timerctx"`
		TimerTCP    int    `json:"timertcp"`
		TimerCTXZBD int    `json:"timerctxzbd"`
		TimerTCPZBD int    `json:"timertcpzbd"`
		//список отмеченных серверов на вебморде для исключения из проверки
		Stocker []Element `json:"stocker"`
		Timer   string    `json:"timer"` // время таймера

	}
	Element struct {
		IDS    string `json:"ids"`
		Status bool   `json:"status"`
	}
	FromClientNewNode struct {
		Command string `json:"command"`
		Dbs     string `json:"dbs"`
		Period  string `json:"period"`
		Timeout string `json:"timeout"`
		Marked  string `json:"marked"` //метка процесса
	}
	AnswerClient struct {
		Status  string `json:"status"`
		Message string `json:"message"`
	}
	FromWorker struct {
		Error       string `json:"error"`        //описание ошибки
		ResultCheck bool   `json:"result_check"` //результат проверки ноды
		TimeRun     string `json:"time_run"`     //время выполнения проверки ноды
	}
	//пакет данных от воркера серверу
	DBWorkerPacket struct {
		IDS         string `json:"ids"`          //идентификатор воркера
		Node        string `json:"node"`         //нода, которую он проверяет
		Result      bool   `json:"result"`       //результат проверки ноды, true - доступна, false - не доступна, + возможно наличие ошибки
		Error       string `json:"error"`        // код ошибки + текст ошибки
		StatusWork  string `json:"status_work"`  //статус выполнения текущей работы `single`, `repeat`
		TimeRun     string `json:"time_run"`     //время исполнение запроса к базе данных
		TimeCurrent string `json:"time_current"` //текущее время формированич ответа
		FILID       int    `json:"filid"`
	}
	//структура для одиночного воркера проверябщего доступность бд по форме
	DBWorkerFormParams struct {
		Node         string           `json:"node"`          //база данных для проверки
		WorkerID     int              `json:"worker_id"`     //идентфикатор рабочего
		WorkerPacket []DBWorkerPacket `json:"worker_packet"` //пакет данных от рабочего
		Timeout      int              `json:"timeout"`       //параметр: таймаут по длинным запросам
		Period       int              `json:"period"`        //параметр: период тикера в секундах(devo) часах(product)
		Mark         string           `json:"mark"`          // метка горутины-менеджера одиночной проверки, который нужно идентифицировать
		StartTime    time.Time        `json:"start_time"`    //время старта работы
		Counter      int64            `json:"counter"`       //счетчик проверок-итераций по таймеру
	}
	WebPrint struct {
		Workers int                  `json:"workers"`
		Stock   []DBWorkerFormParams `json:"stock"`
	}
	//структура для обмена по по каналу между глобалмэенеджеров и хэндлерами
	GlobalHandler struct {
		Command string //команда управленцу с внешнего контура = вебморды
		Marked  string //для прибия процесса
	}
	//структура от глобального управленца воркерам рассылается по каналу chanCommand
	GlobalCommandToWorker struct {
		Command string
		Marked  string
	}
	FromServer struct {
		Nodes                 int               `json:"nodes"`         //количество серверов
		NodesChecked          int               `json:"nodes_checked"` // количество серверов выбранных через вебморду
		Command               string            `json:"command"`       //управляющая команда
		CountCheck            int               `json:"count_check"`   //количество проверенных серверов
		ListKeys              []string          `json:"list_keys"`     //список идентификаторов активных воркеров
		ListNodes             ListDatabaseNode  `json:"listnodes" `    //список серверов полученных из БД-filials
		TimerCTX              time.Duration     `json:"timerctx"`
		TimerTCP              time.Duration     `json:"timertcp"`
		TimerCTXZBD           time.Duration     `json:"timerctxzbd"`
		TimerTCPZBD           time.Duration     `json:"timertcpzbd"`
		TypeCheck             string            `json:"typecheck"`                //тип проверки
		SplitListNodes        [][]*DatabaseNode `json:"split_list_nodes"`         //список разбитых на количество сегментов-серверов
		SplitListNodesChecked [][]*DatabaseNode `json:"split_list_nodes_checked"` //список разбитых на количество сегментов-серверов с веб морды отобранных
		WorkerPacket          DBWorkerPacket    `json:"workerpacket"`             //пакет с результатми от рабочего
		CountIteration        int               `json:"count_iteration"`          //количество итераций в рамках времени работы одного процесса
		TimeChecked           string            `json:"time_checked"`             //время проверки
	}
	TOClient struct {
		Client           FromServer        `json:"client"`
		Worker           DBWorkerPacket    `json:"worker" `
		Node             DatabaseNode      `json:"node" `
		ListNodes        *ListDatabaseNode `json:"listnode" `
		TimerCTX         time.Duration     `json:"timerctx"`
		TimerTCP         time.Duration     `json:"timertcp"`
		TimerCTXZBD      time.Duration     `json:"timerctxzbd"`
		TimerTCPZBD      time.Duration     `json:"timertcpzbd"`
		TimerMultiCheck  time.Duration     `json:"timer_multi_check"`
		StatusMultiCheck bool              `json:"status_multi_check"`
	}
	//структура для включения флаг запущенной/отключенной постоянной проверки
	StatusMultiCheck struct {
		sync.RWMutex
		Status bool
	}
	BaseMSG struct {
		Command string `json:"command"`
		Body    string `json:"body"`
	}

	//структура для записи в FIFO время проверки + время выполнения SQL запроса к бд
	InfoFunctionTime struct {
		Service    string `json:"service"`
		TimeRun    string `json:"time_run"`
		TimeSQLRun string `json:"time_sql_run"`
	}
	//структура для результата выборки по РЕМДу
	ResultRemd struct {
		Filial   int    `json:"filial"`
		LastTime string `json:"last_time"`
		//CountDocsEmdArc int    `json:"count_docs_emd_arc"`
		//CountProtTP     int    `json:"count_prot_tp"`
		ErrorAccess string `json:"error_access"`
	}
	ResultImek struct {
		Filial       int    `json:"filial"`
		Fullname     string `json:"fullname"`
		Pcode        int64  `json:"pcode"`
		TreatCode    int64  `json:"treat_code"`
		TreatDate    string `json:"treat_date"`
		ResponseDate string `json:"response_date"`
	}

	//test users struct table
	Users struct {
		ID    int
		Name  string
		Maked string
	}

	//временной бокс для хранения промежуточных результатов проверок IMEK + REMD
	SaveResultFunctions struct {
		REMD []ResultRemd
		IMEK []ResultImek
		sync.RWMutex
	}
)

func (s *SaveResultFunctions) SaveREMD(res []ResultRemd)  {
	s.Lock()
	defer s.Unlock()
	s.REMD= res
}
func (s *SaveResultFunctions) SaveIMEK(res []ResultImek)  {
	s.Lock()
	defer s.Unlock()
	s.IMEK = res
}

func (s *SaveResultFunctions) GetREMD() []ResultRemd {
	s.Lock()
	defer s.Unlock()
	return s.REMD
}
func (s *SaveResultFunctions) GetIMEK() []ResultImek {
	s.Lock()
	defer s.Unlock()
	return s.IMEK
}

func (s *StatusMultiCheck) Enable() {
	defer s.Unlock()
	s.Status = true
	s.Lock()
}
func (s *StatusMultiCheck) Disable() {
	defer s.Unlock()
	s.Status = false
	s.Lock()
}

func (c *ConnWS) WriteJSON(s interface{}) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.Conn.WriteJSON(s)
}

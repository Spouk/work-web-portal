//----------------------------------------------------------------------
// проверка происходит по следующему алгоритму
// + выборка списка бд из базы данных
// + вебморда кнопка "обновить список бд"
// + вебморда поле для ввода времени выполнения
// + метка для показа количество работающих воркеров по бд
// + кнопка "остановить все воркеры"
// + кнопка "запустить проверку по таймеру с поля"
// + поле "таймер"= поле в котором следует указывать время периода проверки
//   при нажатиии кнопки "старт" будет считано значение из этого поля, если его там не будет, будет
//   использовано дефолтное значение из конфига равное 10 минутам
//   при проверке воркером бд он записывает в бд результат проверки бд вида
//-----------------------------------------------------------------------

//----------------------------------------------------------------------
// как показывать логи, первая мысль в виде разбивки по календарным суткам
//	1 апреля = сводная информация о результатах проверки, отображать только недоступные бд
//	в виде таблицы  - 10:00 - 3,4,5;
//
//	горизонтальная колонка дни
//	вертикальная колонка результаты проверок - неудачных
//-----------------------------------------------------------------------

//выборка - получаю текущую дату, делаю период текущего месяца для корректной выборк
//currentMonth = 03 then between 01/03/2021 - 31/03/2021
//(!) не забыть добавить проверк количества дней в текущем месяце с учетом високосного года
//
//select * from tableCheck
//where DateTime between cast()
//
//
//проверка пример
//1,2,3 бд период 1 час
//1,dsfg,dfgdfg,true,"",13:00, 30/03/2021
//2,dsfg,dfgdfg,true,"",13:01, 30/03/2021
//3,dsfg,dfgdfg,true,"",13:11, 30/03/2021
//
//нужно делать выборку под таблицу в месяц исходя из заданных полей а это день и время
//1 сделать выборку всех проверок за конкретный день
//	1.1 провести сортировку результатов - выбрать недоступные базы, сформировать из них список
//	1.2 формирую  матрицу значений вида map[int][]int (ключ = день, значение = списрк FILID недоступных на момент)
//				map[int]map[int][]int (день, час, список бд недоступных)
//				1=1=[]
//				1=2=[1,33]
//				1=3=[1,33]
//				1=15=[]
//

package library

import (
	"context"
	"database/sql"
	"fmt"
	"time"
)

//структура таблицы дял записи логов статистики всех проведенных проверок баз данных
type TablelogCheck struct {
	ID                int64
	FILID             int64
	DBNAME            string
	DBPATH            string
	ResultCheckStatus bool      //статус ошибки
	ResultCheckError  string    //текст ошибки
	TimeCheck         time.Time //время проверки
	DateTime          time.Time //дата проверки
}

//структуры таблиц во внешних бд, которые надо изменять (хотя по существу эти таблицы можно не пожтягивать)

//----------------------------------------------------------------------
// multicheck adaptive : timer= int = minutes
//-----------------------------------------------------------------------
func (s *Server) MultiCheckADAPTIVE(chanTOTALBREAK chan int, toWrite chan interface{}, listFILID []int, typeCheck string, timer time.Duration) {
	//создание временных каналов для работы воркеров
	chanEnd := make(chan bool)
	chanWork := make(chan DBWorkerPacket)
	chanCommand := make(chan bool)

	//тикер, по запуску новых проверок в периоде параметра аргумента вызова функции
	timerTicker := time.NewTicker(time.Minute * timer)
	var (
		countWorkers = 0
		countCurrent = 0
	)

	//первичный запуск горутин после старта менедежера
	if s.cfg.DebugMulticheck {
		s.log.Printf("[multicheck-MultiCheckADAPTIVE] timer ready...\n")

		//устаанвливаю время последней итерации-проверки
		s.ListDatabaseNodes.TimeLastChecked = time.Now()

		//отправка вебморде пакета информации о итерации и времени начала проверки
		toWrite <- TOClient{
			Client: FromServer{
				Command:        "setinfo",
				CountIteration: s.ListDatabaseNodes.CountIteration,
				TimeChecked:    s.ListDatabaseNodes.TimeLastChecked.Format(time.RFC822),
			},
		}
	}

	//увеличиваю счетчик количества итераций проверок
	s.ListDatabaseNodes.CountIteration++

	s.log.Printf("[MULTICHECK] NEXT ITERATION: %v\n", listFILID, ListDatabaseNode{})
	var tmpCount = 0
	for i, n := range listFILID {
		for _, x := range s.ListDatabaseNodes.ListDBNode {
			if x.Filid == n {
				tmpCount++
				go s.workerMULTIADAPTIVE(x, i, chanEnd, chanWork, chanCommand)
			}
		}
	}

	//если время обработки не успеет откатиться к моменту когда последние воркеры закончат работу,
	//потребуется общее количество воркеров с целью корректного отслеживания их окончания работы
	countWorkers += tmpCount

	for {
		select {

		//запуск очередной порции рабочих
		case <-timerTicker.C:
			if s.cfg.DebugMulticheck {
				s.log.Printf("[multicheck-MultiCheckADAPTIVE] timer ready...\n")

				//увеличиваю счетчик количества итераций проверок
				s.ListDatabaseNodes.CountIteration++

				//устаанвливаю время последней итерации-проверки
				s.ListDatabaseNodes.TimeLastChecked = time.Now()

				//отправка вебморде пакета информации о итерации и времени начала проверки
				toWrite <- TOClient{
					Client: FromServer{
						Command:        "setinfo",
						CountIteration: s.ListDatabaseNodes.CountIteration,
						TimeChecked:    s.ListDatabaseNodes.TimeLastChecked.Format(time.RFC822),
					},
				}
			}
			s.log.Printf("[MULTICHECK] NEXT ITERATION: %v\n", listFILID, ListDatabaseNode{})
			var tmpCount = 0
			for i, n := range listFILID {
				for _, x := range s.ListDatabaseNodes.ListDBNode {
					if x.Filid == n {
						tmpCount++
						go s.workerMULTIADAPTIVE(x, i, chanEnd, chanWork, chanCommand)
					}
				}
			}

			//если время обработки не успеет откатиться к моменту когда последние воркеры закончат работу,
			//потребуется общее количество воркеров с целью корректного отслеживания их окончания работы
			countWorkers += tmpCount

		//прослушивание канала с результатами работы воркеров
		case worketPacket := <-chanWork:
			toWrite <- TOClient{
				Client: FromServer{
					Command: "multinode",
				},
				Worker:          worketPacket,
				Node:            DatabaseNode{},
				ListNodes:       new(ListDatabaseNode),
				TimerCTX:        0,
				TimerTCP:        0,
				TimerCTXZBD:     0,
				TimerTCPZBD:     0,
				TimerMultiCheck: 0,
			}

		//прослушивание канала по прекрашению работы воркеров
		case <-chanEnd:
			if countCurrent == countWorkers {
				s.log.Printf("[MultiCheckADAPTIVE] countCurrent == countWorkers\n")
			} else {
				countCurrent++
			}

		//прослушивание канала с командами от манипулятора
		//тут команда будет одна, прекрашение работы, посему при поступлении любого значения в этотй канал
		//прекращаю работу, посредством закрытия канала по взаимодействию с горутинами, дожидаюсь завершения всех горутин
		//и осуществляю выход из менеджера
		case <-chanTOTALBREAK:
			// для отладки проверка на закрытость канала
			_, status := <-chanCommand
			if status == false {
				s.log.Printf("[ERROR] channel chanCommand IS CLOSED!!!\n")
			} else {
				//закрываю канал для работы с горутинами -> передаю сигнал о авариийном прекращении работы ими
				close(chanCommand)
			}
			if s.cfg.DebugMulticheck {
				s.log.Printf("[MultiCheckADAPTIVE] get command from `server` for FAST BREAK  TOTAL WORK")
			}
		}
	}
}

//функция рабочего адаптированного под множественную проверку серверов
func (s *Server) workerMULTIADAPTIVE(node *DatabaseNode, workerNumber int, chanEnd chan bool, chanInfo chan DBWorkerPacket, chanCommand chan bool) {

	//уведомлние в канал о завершении работы горутины при выходе из тела
	defer func() {
		chanEnd <- false
		s.log.Printf("[workerMULTIADAPTIVE#%d] end working\n", workerNumber)

	}()

	//host := strings.Split(strings.Split(strings.Split(node.SqlRequest, "@")[1], "/")[0], ":")

	//контекст с таймаутом
	ctx, cancelCTX := context.WithTimeout(context.Background(), time.Second*node.TimeoutCTX)

	//канал для получения данных от воркера проверяющего сервер на предмет доступности по TCP порту
	chanEndWorker := make(chan FromWorker)

	//запуск воркера - проверка доступности сервера по TCP порту
	go s.checkServer(node.SqlRequest, chanEndWorker)

	//цикл по прослушиванию событий
	for {
		select {

		//канал для получения команды от глобального управленца
		case <-chanCommand:
			//прекращаю работу воркера экстренно
			cancelCTX()
			if s.cfg.DebugMulticheck {
				s.log.Printf("[workerMULTICHECK--worker#%d] get COMMAND TERMINATED!!!!\n", workerNumber)
			}
			return

		//канал для оповещения о истечении таймера по контексту
		case <-ctx.Done():
			//истек заданный таймаут, база подвисшая:: изменяю статус в таблице filialstate

			//проверка наличия записи в таблице. если записи нет, создаю новую с изменением статуса
			if !s.checkRecordTableStateExists(node.Filid) {
				s.log.Printf("[checkRecordTableStateExists] --> TRUE: %v\n", node.Filid)
				//connect dbs
				conn, err := sql.Open("firebirdsql", s.cfg.DBMulticheckAuth)
				if err != nil {
					s.log.Println(err)
				}
				//записи нет, создаю новую
				_, err = conn.Exec(`insert into filialstate (filial, state, comment) values (?, ?, ?);`,
					node.Filid, s.cfg.DBSMultiStatusERROR, "Error database connection: timeout expired")
				if err != nil {
					s.log.Println(err)
				}
				//делать обязательно коммит иначе не сохранятся внесенные изменения
				_, err = conn.Exec(`commit;`)
				if err != nil {
					s.log.Println(err)
				}
				_ = conn.Close()
			} else {
				s.log.Printf("[checkRecordTableStateExists] --> FALSE: %v\n", node.Filid)
				//connect dbs
				conn, err := sql.Open("firebirdsql", s.cfg.DBMulticheckAuth)
				if err != nil {
					s.log.Println(err)
				}
				//запись есть, обновляю статус в записи
				_, err = conn.Exec(`update filialstate set state = ?, comment = ? where filial = ?`,
					node.Filid, s.cfg.DBSMultiStatusERROR, "Error database connection: timeout expired")
				if err != nil {
					s.log.Println(err)
				} else {
					s.log.Printf("ALL OK UPDATE FILIALSTYATE\n")
				}
				//делать обязательно коммит иначе не сохранятся внесенные изменения
				_, err = conn.Exec(`commit;`)
				if err != nil {
					s.log.Println(err)
				}
				_ = conn.Close()
			}

			//TODO: добавить запись в лог результата проверки
			////делаю запись в таблицу логов о проведенной проверке
			//conn2, err2 := sql.Open("firebirdsql", s.cfg.DBMulticheckAuth)
			//if err2 != nil {
			//	s.log.Println(err2)
			//}
			//_, err = conn2.Exec(`update filialstate set state = ?, comment = ? where filial = ?`,
			//	s.cfg.DBSMultiStatusERROR, "Error database connection: timeout expired", node.Filid)
			//if err != nil {
			//	s.log.Println(err)
			//}

			//выстроить таблицу в виде проверяемых бд , к каждой прикрутить иконку кружка которая будет менять цвет
			//красный/зеленый при смене статуса, поиск производить через filid
			//истек заданный таймаут, база подвисшая
			chanInfo <- DBWorkerPacket{
				IDS:        fmt.Sprintf("worker#%d", workerNumber),
				Node:       node.Dbipaddr,
				Result:     false,
				Error:      fmt.Sprintf("`%s` таймаут истек\n", node.Dbipaddr),
				StatusWork: "single",
				TimeRun:    fmt.Sprintf("%d s", node.TimeoutCTX),
				FILID:      node.Filid,
			}
			if s.cfg.DebugMulticheck {
				s.log.Printf("[workerMULTICHECKER--worker#%d][%d] timeout expired!!!!\n", workerNumber, node.Filid)
			}
			return

		//канал для приема сообщения от воркера
		//тут надо изменять таблицу filiastate на 161 бд + добавлять статистику проверки в локальную бд
		case res := <-chanEndWorker:
			dbs := DBWorkerPacket{
				IDS:        fmt.Sprintf("workerMULTICHECKER#%d", workerNumber),
				Node:       node.Dbipaddr,
				Result:     res.ResultCheck,
				StatusWork: "multinode",
				TimeRun:    res.TimeRun,
				Error:      res.Error,
				FILID:      node.Filid,
			}
			chanInfo <- dbs

			//проверка успешна, база данных доступна
			////connect dbs
			conn, err := sql.Open("firebirdsql", s.cfg.DBMulticheckAuth)
			if err != nil {
				s.log.Println(err)
			}

			//проверка наличия записи в таблице. если записи нет, создаю новую с изменением статуса
			if s.checkRecordTableStateExists(node.Filid) {
				//записи нет, создаю новую
				_, err = conn.Exec(`insert into filialstate (filial, state, comment) values (?, ?, ?);`,
					node.Filid, s.cfg.DBSMultiStatusOK, "OK")
				if err != nil {
					s.log.Println(err)
				}
			} else {
				//запись есть, обновляю статус в записи
				_, err = conn.Exec(`update filialstate set state = ?, comment = ? where filial = ?`,
					node.Filid, s.cfg.DBSMultiStatusOK, "OK")
				if err != nil {
					s.log.Println(err)
				}
			}
			_ = conn.Close()

			//делать обязательно коммит иначе не сохранятся внесенные изменения
			_, err = conn.Exec(`commit;`)
			if err != nil {
				s.log.Println(err)
			}

			s.log.Printf("[workerMULTICHECKER--worker#%v][%v] get result, its ok!!!!\n", workerNumber, dbs, node.Filid)
			return
		}
	}
}

//проверяет наличие записи в таблице
func (s *Server) checkRecordTableStateExists(filid int) bool {
	//connect dbs
	dbs, err := sql.Open("firebirdsql", s.cfg.DBMulticheckAuth)
	if err != nil {
		s.log.Println(err)
	}

	res, err := dbs.Query(`select count(1) from filialstate where filial = ?`, filid)
	if err != nil {
		s.log.Println(err)
		return false
	}
	var rr int
	_ = res.Scan(&rr)
	if rr == 1 {
		return true
	} else {
		return false
	}
}

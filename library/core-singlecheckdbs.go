//----------------------------------------------------------------------
// http://localhost:4001/checkdbsingle
// одиночная проверка баз данных с выборкой бд из бд (тавтология)
//-----------------------------------------------------------------------
package library

import (
	"database/sql"
	"fmt"
	_ "github.com/nakagami/firebirdsql"
	"strconv"
	`strings`
	"time"
)

//установка(замещение) таймеров по имеющемуся списку серверов
func (s *Server) UpdateTimersForListNode(timeoutTCP, timeoutCTX, timeoutTCPZBD, timeoutCTXZBD time.Duration) {
	for _, x := range s.ListDatabaseNodes.ListDBNode {
		if x.Ismain > 0 {
			s.log.Printf("Found ISMAIN > 0: %v\n", x)
			x.TimeoutCTX = timeoutCTXZBD
			x.TimeoutTCP = timeoutTCPZBD
		} else {
			x.TimeoutCTX = timeoutCTX
			x.TimeoutTCP = timeoutTCP
		}
	}
	s.cfg.TimeoutTCP = timeoutTCP
	s.cfg.TimeoutCTX = timeoutCTX
	s.cfg.TimeoutTCPZBD = timeoutTCPZBD
	s.cfg.TimeoutCTXZBD = timeoutCTXZBD
}

func (s *Server) GetListServerFromDatabaseTest() {
	//connect dbs
	conn, err := sql.Open("firebirdsql", s.cfg.DBReqAuth)
	if err != nil {
		s.log.Fatal(err)
	}
	//выборка
	result := []*DatabaseNode{}
	rows, err := conn.Query("select filid from filials")
	if err != nil {
		s.log.Printf("ERROR SQL EXEC: %v\n", err)
		return
	} else {
		if rows.Err() != nil {
			s.log.Printf("ERROR SQL EXEC: %v\n", rows.Err())
		} else {
			var count = 0
			for rows.Next() {
				count++
				var fu DatabaseNode
				//SELECT f.filid, f.dbpath, f.shortname, f.fullname, f.dbipaddr, f.dbport, f.ismain  FROM filials f
				err = rows.Scan(&fu.Filid)

				if err != nil {
					s.log.Printf("ERROR SQL.SCAN: %v\n", err)
				} else {
					s.log.Printf("OK SQL.SCAN: %v\n", fu.Filid)
					////dbpath  = 172.30.30.12:t002  (t002 = Name database)
					////dbipaddr = 172.30.32.12
					////dbport = 3050
					////make -> "sysdba:sysdba@localhost:3055/employee"
					////split := strings.Split(fu.Dbpath, ":")
					//split := strings.SplitN(fu.Dbpath, ":", 2)
					//fu.SqlRequest = s.makeSqlRequest(s.cfg.DBReqUSER, s.cfg.DBReqPASSWORD, split[0], strconv.Itoa(fu.Dbport), split[1])
					//
					////установка таймеров из конфига (первый запуск сервера)
					//if fu.Ismain > 0 {
					//	fu.TimeoutCTX = s.cfg.TimeoutCTXZBD
					//	fu.TimeoutTCP = s.cfg.TimeoutTCPZBD
					//} else {
					//	fu.TimeoutTCP = s.cfg.TimeoutTCP
					//	fu.TimeoutCTX = s.cfg.TimeoutCTX
					//}

					result = append(result, &fu)
				}
			}
		}
		s.ListDatabaseNodes.ListDBNode = result
		s.ListDatabaseNodes.TimeUpdate = time.Now()
	}
	fmt.Printf("[SQLReqList] Result: %v\n", result)
	for i, x := range result {
		fmt.Printf("[%d] %v\n", i, x)
	}
}



//делаю выборку из бд, конвертируют список в структуры для удобной обработки
func (s *Server) GetListServerFromDatabase() {
	//connect dbs
	conn, err := sql.Open("firebirdsql", s.cfg.DBReqAuth)
	if err != nil {
		s.log.Fatal(err)
	}
	//выборка
	result := []*DatabaseNode{}
	rows, err := conn.Query(s.cfg.DBReqSQL)
	if err != nil {
		s.log.Printf("ERROR SQL EXEC: %v\n", err)
		return
	} else {
		if rows.Err() != nil {
			s.log.Printf("ERROR SQL EXEC: %v\n", rows.Err())
		} else {
			var count = 0
			for rows.Next() {
				count++
				var fu DatabaseNode
				//SELECT f.filid, f.dbpath, f.shortname, f.fullname, f.dbipaddr, f.dbport, f.ismain  FROM filials f
				err = rows.Scan(&fu.Filid, &fu.Dbpath, &fu.Shortname, &fu.Fullname, &fu.Dbipaddr, &fu.Dbport, &fu.Ismain)

				if err != nil {
					s.log.Printf("ERROR SQL.SCAN: %v\n", err)
				} else {
					s.log.Printf("OK SQL.SCAN: %v\n", fu.Filid)
					//dbpath  = 172.30.30.12:t002  (t002 = Name database)
					//dbipaddr = 172.30.32.12
					//dbport = 3050
					//make -> "sysdba:sysdba@localhost:3055/employee"
					//split := strings.Split(fu.Dbpath, ":")
					split := strings.SplitN(fu.Dbpath, ":", 2)
					fu.SqlRequest = s.makeSqlRequest(s.cfg.DBReqUSER, s.cfg.DBReqPASSWORD, split[0], strconv.Itoa(fu.Dbport), split[1])

					//установка таймеров из конфига (первый запуск сервера)
					if fu.Ismain > 0 {
						fu.TimeoutCTX = s.cfg.TimeoutCTXZBD
						fu.TimeoutTCP = s.cfg.TimeoutTCPZBD
					} else {
						fu.TimeoutTCP = s.cfg.TimeoutTCP
						fu.TimeoutCTX = s.cfg.TimeoutCTX
					}

					result = append(result, &fu)
				}
			}
		}
		s.ListDatabaseNodes.ListDBNode = result
		s.ListDatabaseNodes.TimeUpdate = time.Now()
	}
	fmt.Printf("[SQLReqList] Result: %v\n", result)
	for i, x := range result {
		fmt.Printf("[%d] %v\n", i, x)
	}
}

//формирует строку для sql проверки
func (s *Server) makeSqlRequest(user, password, host, port, dbs string) string {
	return fmt.Sprintf("%s:%s@%s:%s/%s", user, password, host, port, dbs)
}

func (s *Server) CheckboxSelectAndUncheck2(list []Element) {
	for _, x := range list {
		conv, err := strconv.ParseInt(x.IDS, 0, 16)
		if err != nil {
			s.log.Printf("Error convert string->int: %v\n", err)
			continue
		} else {
			for _, a := range s.ListDatabaseNodes.ListDBNode {
				if int(conv) == a.Filid {
					s.log.Printf("----:: %v %v\n", a.Element, x)
					a.Element.IDS = x.IDS
					a.Element.Status = x.Status
				}
			}
		}
	}
	for _, x := range s.ListDatabaseNodes.ListDBNode {
		fmt.Printf("element: %v\n", x)
	}
}

func (s *Server) CountChecked() (int, int) {
	var (
		check, uncheck int
	)
	for _, x := range s.ListDatabaseNodes.ListDBNode {
		if x.Element.Status {
			check++
		} else {
			uncheck++
		}
	}
	return check, uncheck
}

//функция просматривает и изменяет те сервера, которые надо исключить из проверки
func (s *Server) checkboxSelectAndUncheck(list []Element) {
	for _, x := range list {
		conv, err := strconv.ParseInt(x.IDS, 0, 16)
		if err != nil {
			s.log.Printf("Error convert string->int: %v\n", err)
			continue
		} else {
			for _, a := range s.ListDatabaseNodes.ListDBNode {
				if int(conv) == a.Filid {
					a.Element.IDS = x.IDS
					a.Element.Status = x.Status
				}
			}
		}
	}
}

//тестовая фукнция подсчитыват количество отобранных серверов
func (s *Server) countRL() {
	var (
		trued  int
		falsed int
	)
	for _, x := range s.ListDatabaseNodes.ListDBNode {
		if x.Element.Status {
			trued++
		} else {
			falsed++
		}
	}
	s.log.Printf("----> RESULT CHECK CHECK STATUS: [Trued ::%v] [Falsed :: %d]\n", trued, falsed)
}

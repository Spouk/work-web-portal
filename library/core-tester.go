package library

import (
	"fmt"
	"log"
	"os"
	"sync"
	"time"
)

type Tester struct {
	sw  *sync.WaitGroup
	log *log.Logger
}

func NewTester() *Tester {
	t := &Tester{
		sw:  &sync.WaitGroup{},
		log: log.New(os.Stdout, "[tester ] ", log.LstdFlags),
	}
	return t
}

type ResultWorker struct {
	IDS        string `json:"ids"`         //id воркеры = div#IDS блока на странице
	Status     string `json:"status"`      //текущий статус работы воркера
	Result     string `json:"result"`      //результаты работы воркера
	FromServer string `json:"from_server"` //от серверa
}

//count - количество воркеров, sendWorkStatus - внешний канал для передачи результата работы воркеров
//controlManager - канал для контроля работы менеджера
func (t *Tester) Manager(count int, sendWorkStatus chan ResultWorker, controlManager chan string) {
	t.log.Println("manager start")
	defer t.log.Println("manager end")
	command := make(chan string, count) //only send
	work := make(chan ResultWorker)     //only get
	workEndWork := make(chan bool)      //получение сообщения от воркера, о прекращении работы
	var countEndWorker = 0

	//запуск воркеров
	for i := 0; i < count; i++ {
		t.sw.Add(1)
		go t.worker(work, fmt.Sprintf("worker#%d", i), t.sw, i+2, 30, command)
	}

	//обработка в цикле
	for {
		select {
		//канал для получения сообщений от воркеров об окончании работы
		case _ = <-workEndWork:
			countEndWorker += 1

		//канал для получения ответов от воркеров
		case resWork := <-work:
			sendWorkStatus <- resWork
		//канал для внешнего управления менеджером
		case com := <-controlManager:
			switch com {
			case "exit":
				t.log.Printf("поступила команда Exit из контроля\n")
				//запускаю команду прекращения работы воркеров
				for i := 0; i < count; i++ {
					command <- "exit"
				}
				//жду завершения
				t.sw.Wait()
				//выход из менеджера
				return
			}
		default:
			//проверка завершенных воркеров
			if countEndWorker == count {
				t.log.Println("все воркеры закончил работу, возврат")
				return
			}
		}
	}
}

//воркер работает следующим образом получает канал, куда сливает все результаты работы, по результату работы завершается
//оповещая об этом os.stdout, завершая sw.Done(), или критическое завершение работы по команде
func (t *Tester) worker(work chan ResultWorker, name string, sw *sync.WaitGroup, tickerPeriod int, countIteration int, command chan string) {
	t.log.Printf("start %s\n", name)
	defer func() {
		sw.Done()
		t.log.Printf("end %s\n", name)
	}()
	//отправка сообщение о старте работы этого воркера
	work <- ResultWorker{
		IDS:    name,
		Status: "starting",
		Result: "",
	}
	ticker := time.NewTicker(time.Second * time.Duration(tickerPeriod))
	for {
		select {
		//управляющая команда
		case cc := <-command:
			switch cc {
			case "exit":
				t.log.Printf("FAST EXIT from %s\n", name)
				return
			}
			//тикер
		case tick := <-ticker.C:
			if countIteration > 0 {
				t.log.Printf("[%d] %s:%s\n", countIteration, name, tick.String())
				work <- ResultWorker{
					IDS:    name,
					Status: "working",
					Result: tick.String(),
				}
				countIteration -= 1
			} else {
				//передача структуры
				work <- ResultWorker{
					IDS:    name,
					Status: "the end",
					Result: tick.String(),
				}
				//завершение работы
				return
			}
		}
	}
}

func (s *Server) workerWC(work chan ResultWorker, tickerPeriod int, sw *sync.WaitGroup, name string, endChan chan bool) {
	defer func() {
		sw.Done()
		endChan <- true
		s.log.Printf("workerWS `%s` end \n", name)
	}()
	ticker := time.NewTicker(time.Second * time.Duration(tickerPeriod))
	var count = 10
	for {
		select {
		//тикер
		case tick := <-ticker.C:
			if count > 0 {
				s.log.Printf("[%d] %s:%s\n", count, name, tick.String())
				work <- ResultWorker{
					IDS:    name,
					Status: "working",
					Result: tick.String(),
				}
				count -= 1
			} else {
				//передача структуры
				work <- ResultWorker{
					IDS:    name,
					Status: "the end",
					Result: tick.String(),
				}
				//завершение работы
				return
			}
		}
	}
}



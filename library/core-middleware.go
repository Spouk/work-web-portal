package library

import (
	"context"
	"gitlab.com/Spouk/gotool/session"
	"net/http"
	"time"
)

func (s *Server) MiddlewareSession(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ses := s.Pool.Get().(*session.Session)
		ses.SetTEXT("info", "referer", []string{r.Referer()})
		ses.SetTEXT("info", "user_requesturi", r.RequestURI)
		ses.SetTEXT("info", "user_remoteaddr", r.RemoteAddr)
		ses.SetTEXT("info", "user_headers", r.Header)
		ses.SetTEXT("info", "user_cookies", r.Cookies())
		ses.SetTEXT("info", "user_referer", r.Referer())
		ses.SetTEXT("info", "user_useragent", r.UserAgent())
		ses.SetTEXT("info", "user_timeconnect", time.Now().String())
		//передача в контекст сессии
		r = r.WithContext(context.WithValue(r.Context(), "session", ses))
		//передача контекста далее
		next.ServeHTTP(w, r)
	})

}

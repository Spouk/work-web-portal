//----------------------------------------------------------------------
// 1 получаю список бд из filials
// 2 делаю сопоставление base and parent
// 3 рекурсивно сканирую базовую директорию,  раскидываю все файлы по филиалам
// 4 подсчитываю среднее значение периода обновления  по файлам (analizator)
// 5
//-----------------------------------------------------------------------
package library

import (
	"crypto/sha1"
	"database/sql"
	"encoding/gob"
	"encoding/hex"
	"fmt"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	layout_timechecked = "2006-01-02 15:04:05"
	layout             = "2006-01-02"
	logprefix          = "[backup-database-checker] "
)

//структура для дампа динамическая
type FilialDump struct {
	Filid  int
	Period int //период обновления бэкапа, в часах если прод, в сек если разработка
	//Prefix     string
	//Dbpath     string
	//SingleFile bool //выставляется флаг, если len(FilesAll <= 1), выяснить среднее армифметическое не представляется возможным
	//moved from files

	//ErrorPeriod     string //строка ошибка, в случае, если период равен 0
	//NoFiles         bool   //флаг, когда не файлов бэкапов по филиалу совсем
	//TimeExpired     bool   //флаг, если бэкап протух
	//TimeLastChecked string //время последней проверки
	//CheckMsg        string //строка проверки
}

//структура для филиала
type Filial struct {
	Filid      int
	Prefix     string
	Dbpath     string
	SingleFile bool //выставляется флаг, если len(FilesAll <= 1), выяснить среднее армифметическое не представляется возможным
	//moved from files
	Period          int            //период обновления бэкапа, в часах если прод, в сек если разработка
	ErrorPeriod     string         //строка ошибка, в случае, если период равен 0
	NoFiles         bool           //флаг, когда не файлов бэкапов по филиалу совсем
	TimeExpired     bool           //флаг, если бэкап протух
	TimeLastChecked string         //время последней проверки
	CheckMsg        string         //строка проверки
	FilesAll        []*profileFile //весь список файлов принадлежащий к одной базе по префиксу и filid
	FileLast        *profileFile   //последний файл-бэкап по временной метке
}

//структура для парсинга ответа от базы данных по филиалу
type FilialDatabase struct {
	Filid  int
	Dbpath string
}

//профиль файла бэкапа
type profileFile struct {
	Filid    int    //filials.filid
	Prefix   string //префикс бд filials.dbpath = ip:dbname_alias, dbname_alias = префикс для текущей базы
	Name     string //полное имя файла
	datemake string //временная метка
	datetime int64  //временная метка в time формате time.Unixnano для манипуляций надо обратно конвертировать, сделал для корректного сохранения в gob-дампах
	Sizer    int64  //размер файла в байтах
	Types    string //тип ОС, где собирался бэкап (L-unix*, W-Windows*)
	hastMark string //хэш метка уникальная
	//datetime time.Time //временная метка в time формате
	//Period          int       //период обновления бэкапа, в часах если прод, в сек если разработка
	//ErrorPeriod     string    //строка ошибка, в случае, если период равен 0
	//TimeExpired     bool      //флаг, если бэкап протух
	//TimeLastChecked string    //время последней проверки
	//FromProfile     bool      //флаг, что данные периода прочитаны из профиля
	//PeriodProfile   int       //период из профиля
	//CheckMsg        string    //строка проверки
}

//основный инстанс
type BackupChecker struct {
	sync.RWMutex
	sw      *sync.WaitGroup
	Log     *log.Logger
	c       *ConfigStruct
	logfile *os.File
	//StockFiles    map[string]*profileFile //1 файл по одному префиксу
	//StockFiles    map[int]*profileFile // filials.filid
	//StockTMPFIles []profileFile        //общий сток всех файлов выбранных по регспекам
	//listProfiles  map[string]Profile     //карта под профили из конфигов и анализатора
	//listTotal     map[int][]*profileFile
	linuxRegex    *regexp.Regexp
	winRegex      *regexp.Regexp
	linuxRegexFB2 *regexp.Regexp
	winRegexFB2   *regexp.Regexp
	//update
	dirRegex *regexp.Regexp

	Stocker         map[int]*Filial     //все филиалы по выборке из filials key = filid
	StockFilialDump map[int]*FilialDump //полученное из дампа
	StockFilials    []*Filial           //все филиалы по выборке из filials key = filid; связка с файлами по StockFilials.Filid = StockFiles.Filid = GOB дамп и рабочие
	StockFiles      []*profileFile      //все файлы, выбранными регспеками рекурсивным проходом по директориии бэкапчиков, списки файлов при каждой проверке проверяеются на дубликаты и новые файлы добапвляются
	StockDBSFilials []*Filial           //все филиалы по выборке из filials key = filid; связка с файлами по StockFilials.Filid = StockFiles.Filid

	//	debug variables
	DebCountDirs    int            //количество найденных директорий
	DebCountFiles   int            //количество найденных файлов подпадающих по регспек-фильтрам
	DebDirFoundMap  map[int]string //найденные директории
	DebDirFoundList []string       //найденные директории в виде списка
}

//создание нового инстанса
func NewBackupChecker(config *ConfigStruct) *BackupChecker {
	//создаю новый инстанс
	bc := &BackupChecker{
		sw:              new(sync.WaitGroup),
		Log:             log.New(os.Stdout, logprefix, log.Lshortfile|log.LstdFlags),
		c:               config,
		Stocker:         make(map[int]*Filial),
		StockFilialDump: make(map[int]*FilialDump),
	}

	//чтение/создание лога
	if _, err := os.Stat(bc.c.LogFile); os.IsNotExist(err) {
		fmt.Printf("Err: %v %v\nLogfile: %v\n", err, os.IsNotExist(err), bc.c.LogFile)
		fh, errf := os.OpenFile(bc.c.LogFile, os.O_CREATE|os.O_APPEND, os.ModePerm)
		if errf != nil {
			panic(errf)
		}
		bc.logfile = fh
	} else {
		fh, errf := os.OpenFile(bc.c.LogFile, os.O_APPEND|os.O_RDWR, os.ModePerm)
		if errf != nil {
			panic(errf)
		}
		bc.logfile = fh
	}

	//прекомпиляция регспеков
	bc.linuxRegex = regexp.MustCompile(`t\d\d\d-\w\w\d-\d\d\d\d\d\d\d\d.7z`)
	bc.winRegex = regexp.MustCompile(`t\d\d\d-\w\w\d-\d\d\d\d\d\d\d\d[wW].7z`)
	bc.linuxRegexFB2 = regexp.MustCompile(`t\d\d\d-\d\d\d\d\d\d\d\d.7z`)
	bc.winRegexFB2 = regexp.MustCompile(`t\d\d\d-\d\d\d\d\d\d\d\d[wW].7z`)
	bc.dirRegex = regexp.MustCompile(`t\d{3}`) //входящий поток привожу к нижнему регистру

	////получаю данные из базы данных
	//bc.GetListDatabaseListFromBD(config)
	//bc.ShowInfoDebug(1)
	//
	////провожу проверку соответствие бд из базы и текущим конфигом
	//bc.checkerFilials()
	//bc.ShowInfoDebug(2)

	////читаю/создаю дамп
	//if err := bc.ConfigDinamicLoader(config.BackupConfigPathFile); err != nil {
	//	bc.Log.Panic(err)
	//}
	////bc.ShowInfoDebug(3)

	////парсю файлы
	//bc.MakeListFilesFromDirectory()
	//
	////заполняю структуроу филиала + добавляю период проверки
	//bc.AnalizatorProfilesWithDir()
	//bc.ShowInfoDebug(4)
	//
	////сортирует и отьирает последний файл бэкап
	//bc.SortEndFiles()
	//bc.ShowInfoDebug(5)
	//
	////запуск первичной проверки для сбора первичных данных для формирования структуры
	//bc.Runner()

	//формирую структуры данных
	bc.Step1FindFiles()

	bc.ShowFilials(bc.Stocker)

	//загружаю дамп и обновляю данные по периодам если дамп есть
	if err := bc.Step2ReadDumpConfig(bc.c.BackupConfigPathFile); err != nil {
		bc.Log.Fatal(err)
	}

	//сохраняю конфиг
	if err := bc.SaveToGob(config.BackupConfigPathFile, bc.StockFilialDump); err != nil {
		bc.Log.Printf("[SaveToGob] ERROR: %v\n", err)
	}

	//возврат инстанса
	return bc
}

//----------------------------------------------------------------------
// функция показ результатов выполнение функций
// по аргументу показывает результат из базового инстанса
//-----------------------------------------------------------------------
func (b *BackupChecker) ShowInfoDebug(f int) {
	switch f {
	//получение списка из бд
	case 1:
		fmt.Printf("[start] ===stage 1===\n")
		for a, b := range b.StockDBSFilials {
			fmt.Printf("* [%d]  %v]\n", a, b)
		}
		fmt.Printf("[end] ===stage 1===\n")

	//проведение сопоставления списка между списком из бд и рабочим списком
	case 2:

		fmt.Printf("[start] ===stage 2===\n")
		var count = 0
		for _, f := range b.Stocker {
			fmt.Printf("[%v] [%v] %v\n", count, f.Filid, f)
			count++
		}
		fmt.Printf("[end] ===stage 2===\n")

	//создание/открытие дампа конфига
	case 3:
		fmt.Printf("[start] ===stage 3===\n")
		//show dumper
		fmt.Printf("---DUMP [StockFilialDump]---")
		for _, x := range b.StockFilialDump {
			fmt.Printf("[%v] [%v] %v\n", x.Filid, x.Period, x)
		}
		fmt.Printf("---DUMP [Stocker]---")
		for _, x := range b.Stocker {
			fmt.Printf("[%v] [%v] %v\n", x.Filid, x.Period, x)
		}

		fmt.Printf("[end] ===stage 3===\n")

	//AnalizatorProfilesWithDir анализатора, подсчет среднего времени между
	case 4:

		fmt.Printf("[start] ===stage 4===\n")
		for a, b := range b.Stocker {
			fmt.Printf("[%v] [%v] [%v] %v\n", a, b.Filid, b.Period, b)
		}
		fmt.Printf("[end] ===stage 4===\n")

	//выбирает последний файл бэкаапа
	case 5:

		fmt.Printf("[start] ===stage 5===\n")
		for a, b := range b.Stocker {
			fmt.Printf("[%v] [%v] [%v] %v\n", a, b.Filid, b.FileLast, b)
		}
		fmt.Printf("[end] ===stage 5===\n")

	}

}

//----------------------------------------------------------------------
// [dinamic-config] получаю список баз данных из бд и формирую список profiles
//-----------------------------------------------------------------------
func (bc *BackupChecker) GetListDatabaseListFromBD(c *ConfigStruct) {
	var (
		stocker []*Filial
	)
	dumpList := make(map[int]*FilialDump)

	conn, err := sql.Open("firebirdsql", c.BackupConfigDBList)
	if err != nil {
		bc.Log.Fatal(err)
	}
	//делаю выборку списка бд из таблицы filials
	qlist, err := conn.Query(`select filid, dbpath from filials where filid > 0`)
	if err != nil {
		bc.Log.Println(err)
	} else {
		for qlist.Next() {
			var tmp FilialDatabase
			err = qlist.Scan(&tmp.Filid, &tmp.Dbpath)
			if err != nil {
				bc.Log.Printf("[error-scan] %v\n", err)
			} else {
				stocker = append(stocker, &Filial{
					Filid:  tmp.Filid,
					Prefix: strings.Split(tmp.Dbpath, ":")[1],
					Dbpath: tmp.Dbpath,
				})
				//создаю структуру для дампа
				dumpList[tmp.Filid] = &FilialDump{
					Filid:  tmp.Filid,
					Period: 0,
				}
			}
		}
		bc.StockDBSFilials = stocker
		bc.StockFilialDump = dumpList

	}
}

//----------------------------------------------------------------------
// 		//проверяю соответствие филиалов из БД имеющимся
//		//если база отсутствует в bc.StockFilials то добавляю
//		//если база лишняято удаляю
//-----------------------------------------------------------------------
func (bc *BackupChecker) checkerFilials() {
	//аспект поиска избыточных, подлежащих удалению филиалов из базового списка
	fu := func(filid int) bool {
		for _, j := range bc.StockDBSFilials {
			if j.Filid == filid {
				return true
			}
		}
		return false
	}
	var NotFound []int
	for _, x := range bc.Stocker {
		//такого филиала уже в списке нет, надо удалить из базового конфига
		if fu(x.Filid) == false {
			NotFound = append(NotFound, x.Filid)
		}
	}
	//собственно удаление
	var result = make(map[int]*Filial)
	flag := false
	for _, x := range bc.Stocker {
		for _, j := range NotFound {
			if j == x.Filid {
				flag = true
				break
			}
		}
		if flag {
			flag = false
			continue
		} else {
			result[x.Filid] = x
		}
	}
	bc.Stocker = result

	//обратная проверка, когда ищущтся отсутствующие филиалы в базовом списке
	du := func(filid int) bool {
		for _, j := range bc.Stocker {
			if j.Filid == filid {
				return true
			}
		}
		return false
	}
	for _, x := range bc.StockDBSFilials {
		//найден отсутствующий элемент, который надо добавить
		if du(x.Filid) == false {
			//сразу добавляю в базовый список
			bc.Stocker[x.Filid] = x
		}
	}
}

//----------------------------------------------------------------------
// [1]  формирует рекурсивно структуру файлов + выстраивает структура на основе директории
//-----------------------------------------------------------------------
func (b *BackupChecker) Step1FindFiles() {

	//определение выходных переменных
	//maper := map[int]*Filial{}
	var numTmp int
	var prefixTmp string

	//debug:: каждый запуск обнуление счетчиков
	b.DebCountDirs = 0
	b.DebCountFiles = 0
	b.DebDirFoundMap = make(map[int]string)
	b.DebDirFoundList = []string{}

	//функция имени директории на предмет соответствия паттерну T/t000
	_ = filepath.Walk(b.c.WorkDir, func(path string, info os.FileInfo, err error) error {
		// временные переменные
		var datetime time.Time
		var pp profileFile

		//перевод имени в верхний регистр
		upperInfoName := strings.ToLower(info.Name())

		//найдена директория по регспеку
		if info.IsDir() && b.dirRegex.MatchString(upperInfoName) {
			b.Log.Printf("----found dir %v\n", upperInfoName)

			//deb:: увеличиваю счетчик
			b.DebCountDirs += 1
			b.DebDirFoundList = append(b.DebDirFoundList, upperInfoName)

			//разбор на составные элементы имени директории
			ru := strings.Split(upperInfoName, "t")[1]
			if num, err := strconv.Atoi(ru); err != nil {
				fmt.Printf("error: %v\n", err)
			} else {
				//проверяю на наличие подобной структуры
				if b.Stocker[num] == nil {
					fmt.Printf("--%d-- not found in Stocker\n", num)
					//создаю новую
					b.Stocker[num] = &Filial{
						Filid:  num,
						Prefix: b.correctFilial(num),
					}
					fmt.Printf("добавлена НОВАЯ ДИРЕКТОРИЯ: %v\n", b.Stocker[num])
				} else {
					//обнуляю список файлов оставляю период
					b.Stocker[num].FilesAll = []*profileFile{}
					b.Stocker[num].FileLast = nil
					b.Stocker[num].SingleFile = false
				}
				//debug save
				b.DebDirFoundMap[num] = upperInfoName

				//save tmp values
				numTmp = num
				prefixTmp = b.Stocker[num].Prefix
			}
		} else {
			// [1] = regex
			//шаблон winRegexFB2  + конвертация временной метки в название в нормальный тип
			if b.winRegexFB2.MatchString(upperInfoName) || b.linuxRegexFB2.MatchString(upperInfoName) {

				b.Log.Printf("----found FILE fb2 ::: %v\n", info.Name())

				//deb:: увеличиваю счетчик
				b.DebCountFiles += 1

				//t001-20210102.7z t001-20210102W.7z
				//x[:4], x[5:13], x[13:14], x)
				mark := info.Name()[5:13]
				conv := strings.Join([]string{mark[:4], mark[4:6], mark[6:]}, "-")
				tt, err := time.Parse(layout, conv)
				if err != nil {
					log.Println(err)
				} else {
					datetime = tt
				}
				tt.UnixNano()

				//window backup
				if b.winRegexFB2.MatchString(upperInfoName) {
					pp = profileFile{
						Filid:    numTmp,
						Prefix:   prefixTmp,
						Name:     upperInfoName,
						datemake: mark,
						datetime: datetime.UnixNano(),
						Sizer:    info.Size(),
						Types:    "windows",
					}
				}
				//linux backup
				if b.linuxRegexFB2.MatchString(upperInfoName) {
					pp = profileFile{
						Filid:    numTmp,
						Prefix:   prefixTmp,
						Name:     upperInfoName,
						datemake: mark,
						datetime: datetime.UnixNano(),
						Sizer:    info.Size(),
						Types:    "linux",
					}
				}

				if b.Stocker[numTmp] != nil && b.findDublicate(pp.Name, pp.Filid) == false {
					b.Stocker[numTmp].FilesAll = append(b.Stocker[numTmp].FilesAll, &pp)
				} else {
					fmt.Printf("[ERROR-WARNING] найден пустой филиал %v\n", numTmp)
				}

				//if maper[numTmp] != nil  {
				//	maper[numTmp].FilesAll = append(maper[numTmp].FilesAll, &pp)
				//} else {
				//	fmt.Printf("[ERROR-WARNING] найден пустой филиал %v\n", numTmp)
				//}
				//if maper[numTmp] != nil  && b.findDublicate(pp.Name, pp.Filid) == false {
				//	maper[numTmp].FilesAll = append(maper[numTmp].FilesAll, &pp)
				//} else {
				//	fmt.Printf("[ERROR-WARNING] найден пустой филиал %v\n", numTmp)
				//}
			}

			// [2] = regex
			//конвертация временной метки
			if b.winRegex.MatchString(upperInfoName) || b.linuxRegex.MatchString(upperInfoName) {

				b.Log.Printf("----found FILE fb3 ::: %v\n", info.Name())

				//deb:: увеличиваю счетчик
				b.DebCountDirs += 1

				mark := upperInfoName[9:17]
				conv := strings.Join([]string{mark[:4], mark[4:6], mark[6:]}, "-")
				tt, err := time.Parse(layout, conv)
				if err != nil {
					log.Println(err)
				} else {
					datetime = tt
				}
				//window backup
				if b.winRegex.MatchString(upperInfoName) {
					pp = profileFile{
						Filid:    numTmp,
						Prefix:   prefixTmp,
						Name:     upperInfoName,
						datemake: upperInfoName[9:17],
						datetime: datetime.UnixNano(),
						Sizer:    info.Size(),
						Types:    "windows",
					}
				}
				//linux backup
				if b.linuxRegex.MatchString(upperInfoName) {
					pp = profileFile{
						Filid:    numTmp,
						Prefix:   prefixTmp,
						Name:     upperInfoName,
						datemake: upperInfoName[9:17],
						datetime: datetime.UnixNano(),
						Sizer:    info.Size(),
						Types:    "linux",
					}
				}

				if b.Stocker[numTmp] != nil && b.findDublicate(pp.Name, pp.Filid) == false {
					b.Stocker[numTmp].FilesAll = append(b.Stocker[numTmp].FilesAll, &pp)
				} else {
					fmt.Printf("[ERROR-WARNING] найден пустой филиал %v\n", numTmp)
				}

				//if maper[numTmp] != nil  {
				//	maper[numTmp].FilesAll = append(maper[numTmp].FilesAll, &pp)
				//} else {
				//	fmt.Printf("[ERROR-WARNING] найден пустой филиал %v\n", numTmp)
				//}

				//if maper[numTmp] != nil  && b.findDublicate(pp.Name, pp.Filid) == false {
				//	maper[numTmp].FilesAll = append(maper[numTmp].FilesAll, &pp)
				//} else {
				//	fmt.Printf("[ERROR-WARNING] найден пустой филиал %v\n", numTmp)
				//}
			}
		}
		return nil
	})
	//b.Stocker = maper
}

//----------------------------------------------------------------------
// [2] читаю дамп конфига и обновляю периоды  старыми значениями
//-----------------------------------------------------------------------
func (b *BackupChecker) Step2ReadDumpConfig(filename string) error {
	//первый старт сервера, проверяю по пути наличие дампа, если есть загружаю, если нет создаю и дамплю пустой конфиг
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		//файла нет создаю и делаю первый дамп
		//почему выставлять os.O_WRONLY - смотрим код ядра linux
		///usr/local/go/src/syscall/zerrors_linux_amd64.go:660
		//O_RDONLY                         = 0x0
		//O_RDWR                           = 0x2
		//O_WRONLY                         = 0x1
		//флаги разные
		var fu *os.File = nil
		if fh, errf := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, os.ModePerm); errf != nil {
			b.Log.Printf("[FATALERROR][OPENFILE] ошибка создания дампа файла конфига `%v` :: `%v` :: `%v`\n", errf, filename, b.c.BackupConfigPathFile)
			if fh, errf1 := os.Create(filename); errf1 != nil {
				b.Log.Printf("[FATALERROR][CREATE] ошибка создания дампа файла конфига `%v` :: `%v` :: `%v`\n", errf, filename, b.c.BackupConfigPathFile)
				return errf
			} else {
				fu = fh
			}
		} else {
			fu = fh
		}
		b.Log.Printf("успешно создан  дамп конфига `%s`\n", filename)
		defer fu.Close()
		d := gob.NewEncoder(fu)
		if err := d.Encode(&b.StockFilialDump); err != nil {
			b.Log.Printf("[ERROR DUMP->FILE]: %v\n", err)
			return err
		}
		//возврат из функции
		return nil

	} else {
		//дамп есть, загружаю его
		fh, errf := os.OpenFile(filename, os.O_APPEND|os.O_RDONLY, os.ModePerm)
		if errf != nil {
			b.Log.Printf("[FATALERROR] ошибка открытия дампа конфига, причина -  `%v`\n", errf)
			return errf
		}
		defer fh.Close()
		d := gob.NewDecoder(fh)
		m := map[int]*FilialDump{}
		if err = d.Decode(&m); err != nil {
			b.Log.Printf("Error: %v\n", err)
			return err
		}
		b.StockFilialDump = m
		b.Log.Printf("[ConfigDinamicLoader][SUCCESS] успешная загрузка дампа конфига!\n")

		//делаю обновление данных по периодам
		//беру значения полученные из дампа ис
		//перезаписываю в текущем конфиге
		if len(b.StockFilialDump) > 0 {
			for _, x := range b.StockFilialDump {
				for _, j := range b.Stocker {
					if j.Filid == x.Filid {
						j.Period = x.Period
					}
				}
			}
		}
	}
	return nil
}

//----------------------------------------------------------------------
// [3] сортирую файлы + высчитываю период +  отбираю последний и присваиваю его FilesLast
//--------------------------------------------------------------------
func (b *BackupChecker) Step3SortFiles() {
	for _, x := range b.Stocker {

		//один файл - бэкап
		if len(x.FilesAll) == 1 {
			//выставляю флаг одиночног файла
			x.SingleFile = true
			//присваиваю последний файл
			x.FileLast = x.FilesAll[0]
			x.Period = 0
			x.ErrorPeriod = fmt.Sprintf("ситуация по префиксу %s, количество файлов = 1\n", x.FilesAll[0].Prefix)
			//на следующую итерацию
			continue
		}

		// если файлов = 0
		if len(x.FilesAll) == 0 {
			//выставляю флаг отсутствия файлов
			x.NoFiles = true
			x.Period = -1
			x.ErrorPeriod = "[ВНИМАНИЕ] по данному филиалу отсутствуют бэкапы совсем!"
			continue
		}

		//корректные случаи, количество файлов > 1
		var tmp []int64
		//обнуляю старые значения, если были
		x.NoFiles = false
		x.ErrorPeriod = ""

		for _, a := range x.FilesAll {
			tmp = append(tmp, a.datetime)
		}
		// сортирую по убыванию
		sort.Slice(x.FilesAll, func(i, j int) bool {
			return x.FilesAll[i].datetime > x.FilesAll[j].datetime
		})

		//присваиваю последний файл
		x.FileLast = x.FilesAll[0]

		//проверка на наличие периода отличного от нуля  если поставить 0 то при проверке запишется среднее значение
		if x.Period > 0 {
			fmt.Printf("[load from dump old value] STATUS: x.Period > 0  [%v] [%v] \n", x.Filid, x.Period)
			continue
		} else {
			fmt.Printf("[make new period] STATUS: x.period <= 0 [%v] [%v] \n", x.Filid, x.Period)
			//получаю среднее в часах
			x.Period = int(time.Unix(0, x.FilesAll[0].datetime).Sub(time.Unix(0, x.FilesAll[len(x.FilesAll)-1].datetime)).Hours() / float64(len(x.FilesAll)))
		}
	}

}

//------------------------------------- ---------------------------------
// [4] подсчет протукхлости бэкапа по FilesLast
//-----------------------------------------------------------------------
func (b *BackupChecker) Step4CheckExpireFile() {
	fu := func(f *profileFile, period int) bool {
		fTime := time.Unix(0, f.datetime)
		//получаю текущую дату
		tt := time.Now()
		tnow := time.Date(tt.Year(), tt.Month(), tt.Day(), 0, 0, 0, 0, time.UTC)
		//добавляю период в часах ко времени создания файла
		fTime = fTime.Add(time.Hour * time.Duration(period))
		tmpFtime := time.Date(fTime.Year(), fTime.Month(), fTime.Day(), 0, 0, 0, 0, time.UTC)
		if tnow.Unix() > tmpFtime.Unix() {
			return true
		} else {
			return false
		}
	}
	for _, f := range b.Stocker {
		if f.FileLast != nil {
			if fu(f.FileLast, f.Period) {
				//expired
				f.TimeExpired = true
				f.TimeLastChecked = time.Now().Format(layout_timechecked)
			} else {
				//ok
				f.TimeExpired = false
				f.TimeLastChecked = time.Now().Format(layout_timechecked)
			}
		}
	}
}

//----------------------------------------------------------------------
// [5] рунер
//-----------------------------------------------------------------------
func (b *BackupChecker) Runner() {
	b.Step1FindFiles()
	err := b.Step2ReadDumpConfig(b.c.BackupConfigPathFile)
	if err != nil {
		b.Log.Fatal(err)
	}
	b.Step3SortFiles()
	b.Step4CheckExpireFile()
	b.Log.Printf("--done `Runner` execute--\n")
}

//----------------------------------------------------------------------
// [6] функция запуска  из обработчика вебсокета
//-----------------------------------------------------------------------
func (bc *BackupChecker) WebSingleCheck(chanWork chan interface{}) {
	//результиирующая структура
	//res := BackupWSResult{
	//	Command: "result",
	//	Result:  []profileFile{},
	//}
	res := BackupWSResultFilial{
		Command:      "result",
		Result:       bc.Stocker,
		FilterResult: false,
	}
	//запускаю цикл проверки бэкапов
	bc.Runner()

	////формирую ответ для вебсокета
	//res.Result = bc.StockTMPFIles

	//передаю в вебсокет результат проверки
	chanWork <- res

	return
}

//----------------------------------------------------------------------
// filter - фильтрация данных
//-----------------------------------------------------------------------
func (b *BackupChecker) FilterData(chanWork chan interface{}, flag bool) {
	result := make(map[int]*Filial)
	if flag {
		for k, v := range b.Stocker {
			if v.TimeExpired {
				result[k] = v
			}
		}
	} else {
		result = b.Stocker
	}
	res := BackupWSResultFilial{
		Command:      "result",
		Result:       result,
		FilterCount:  len(result),
		FilterResult: true,
	}

	chanWork <- res
}

//----------------------------------------------------------------------
// [support] поиск дубликата файла в списке по filid и имени файла
//-----------------------------------------------------------------------
func (b *BackupChecker) findDublicate(filename string, filid int) bool {
	for g, x := range b.Stocker {
		if filid == g {
			for _, t := range x.FilesAll {
				if t.Name == filename {
					return true
				}
			}
		}
	}
	return false
}

//// рекурсивный обход входной директории для формирования списка файлов, с сортировкой
//_ = filepath.Walk(bc.c.WorkDir, func (path string, info os.FileInfo, err error) error {
//	if info.IsDir() == false {
//
//		//переменные для функции, обнуляются после каждой итерации
//		var datetime time.Time
//		var pp profileFile
//
//		// [1] = regex
//		//шаблон winRegexFB2  + конвертация временной метки в название в нормальный тип
//		if bc.winRegexFB2.MatchString(info.Name()) || bc.linuxRegexFB2.MatchString(info.Name()) {
//			//t001-20210102.7z t001-20210102W.7z
//			//x[:4], x[5:13], x[13:14], x)
//			mark := info.Name()[5:13]
//			conv := strings.Join([]string{mark[:4], mark[4:6], mark[6:]}, "-")
//			tt, err := time.Parse(layout, conv)
//			if err != nil {
//				log.Println(err)
//			} else {
//				datetime = tt
//			}
//			tt.UnixNano()
//			fmt.Printf("DATETIME: %v  `%v`\n", tt.String(), tt.UnixNano())
//			//сохраняю оригинальное имя файла для отбора дубликатов и статистики
//			pp.datemake = info.Name()
//
//			//проверка связи префикса и filid
//			if bc.getFilidByPrefix(info.Name()[:4]) == -1 {
//				bc.Log.Printf("[WARNING] prefix `%s` не нашел в Stocker Filid!!!!! вернул -1\n", info.Name()[:4])
//			}
//
//			//window backup
//			if bc.winRegexFB2.MatchString(info.Name()) {
//				pp = profileFile{
//					Filid:    bc.getFilidByPrefix(info.Name()[:4]),
//					Prefix:   info.Name()[:4],
//					Name:     info.Name(),
//					datemake: mark,
//					datetime: datetime.UnixNano(),
//					Sizer:    info.Size(),
//					Types:    "windows",
//					hastMark: bc.hashFromFilename(info.Name()),
//				}
//			}
//			//linux backup
//			if bc.linuxRegexFB2.MatchString(info.Name()) {
//				pp = profileFile{
//					Filid:    bc.getFilidByPrefix(info.Name()[:4]),
//					Prefix:   info.Name()[:4],
//					Name:     info.Name(),
//					datemake: mark,
//					datetime: datetime.UnixNano(),
//					Sizer:    info.Size(),
//					Types:    "linux",
//					hastMark: bc.hashFromFilename(info.Name()),
//				}
//			}
//			if bc.c.Debug {
//				fmt.Printf("[MakeListFilesFromDirectory] pp = `%v`\n", pp)
//			}
//
//			//сохраняю результат
//			if pp.Filid != -1 && bc.findDublicateHash(pp.hastMark) == false {
//				bc.Stocker[pp.Filid].FilesAll = append(bc.Stocker[pp.Filid].FilesAll, &pp)
//			} else {
//				//какая то лажа с файлом или корректностью выборки из filials (глянуть sql условия выборки)
//				bc.Log.Printf("[WARNING] файл имеет filid = -1  `%v`\n", pp)
//			}
//
//		}
//
//		// [2] = regex
//		//конвертация временной метки
//		if bc.winRegex.MatchString(info.Name()) || bc.linuxRegex.MatchString(info.Name()) {
//			mark := info.Name()[9:17]
//			conv := strings.Join([]string{mark[:4], mark[4:6], mark[6:]}, "-")
//			tt, err := time.Parse(layout, conv)
//			if err != nil {
//				log.Println(err)
//			} else {
//				datetime = tt
//			}
//			//window backup
//			if bc.winRegex.MatchString(info.Name()) {
//				pp = profileFile{
//					Filid:    bc.getFilidByPrefix(info.Name()[:4]),
//					Prefix:   info.Name()[:4],
//					Name:     info.Name(),
//					datemake: info.Name()[9:17],
//					datetime: datetime.UnixNano(),
//					Sizer:    info.Size(),
//					Types:    "windows",
//					hastMark: bc.hashFromFilename(info.Name()),
//				}
//			}
//			//linux backup
//			if bc.linuxRegex.MatchString(info.Name()) {
//				pp = profileFile{
//					Filid:    bc.getFilidByPrefix(info.Name()[:4]),
//					Prefix:   info.Name()[:4],
//					Name:     info.Name(),
//					datemake: info.Name()[9:17],
//					datetime: datetime.UnixNano(),
//					Sizer:    info.Size(),
//					Types:    "linux",
//					hastMark: bc.hashFromFilename(info.Name()),
//				}
//			}
//			if bc.c.Debug {
//				fmt.Printf("[MakeListFilesFromDirectory] pp = `%v`\n", pp)
//			}
//
//			//сохраняю результат
//			if pp.Filid != -1 && bc.findDublicateHash(pp.hastMark) == false {
//				bc.Stocker[pp.Filid].FilesAll = append(bc.Stocker[pp.Filid].FilesAll, &pp)
//			} else {
//				//какая то лажа с файлом или корректностью выборки из filials (глянуть sql условия выборки)
//				bc.Log.Printf("[WARNING] файл имеет filid = -1  `%v`\n", pp)
//			}
//
//		}
//	}
//
//	return nil
//})
//}

//----------------------------------------------------------------------
// [1] формирования общего списка всех файлов, подпадающих под шаблоны
//-----------------------------------------------------------------------
//func (bc *BackupChecker) MakeListFilesFromDirectory() {
//
//	// рекурсивный обход входной директории для формирования списка файлов, с сортировкой
//	_ = filepath.Walk(bc.c.WorkDir, func(path string, info os.FileInfo, err error) error {
//		if info.IsDir() == false {
//
//			//переменные для функции, обнуляются после каждой итерации
//			var datetime time.Time
//			var pp profileFile
//
//			// [1] = regex
//			//шаблон winRegexFB2  + конвертация временной метки в название в нормальный тип
//			if bc.winRegexFB2.MatchString(info.Name()) || bc.linuxRegexFB2.MatchString(info.Name()) {
//				//t001-20210102.7z t001-20210102W.7z
//				//x[:4], x[5:13], x[13:14], x)
//				mark := info.Name()[5:13]
//				conv := strings.Join([]string{mark[:4], mark[4:6], mark[6:]}, "-")
//				tt, err := time.Parse(layout, conv)
//				if err != nil {
//					log.Println(err)
//				} else {
//					datetime = tt
//				}
//				tt.UnixNano()
//				fmt.Printf("DATETIME: %v  `%v`\n", tt.String(), tt.UnixNano())
//				//сохраняю оригинальное имя файла для отбора дубликатов и статистики
//				pp.datemake = info.Name()
//
//				//проверка связи префикса и filid
//				if bc.getFilidByPrefix(info.Name()[:4]) == -1 {
//					bc.Log.Printf("[WARNING] prefix `%s` не нашел в Stocker Filid!!!!! вернул -1\n", info.Name()[:4])
//				}
//
//				//window backup
//				if bc.winRegexFB2.MatchString(info.Name()) {
//					pp = profileFile{
//						Filid:    bc.getFilidByPrefix(info.Name()[:4]),
//						Prefix:   info.Name()[:4],
//						Name:     info.Name(),
//						datemake: mark,
//						datetime: datetime.UnixNano(),
//						Sizer:    info.Size(),
//						Types:    "windows",
//						hastMark: bc.hashFromFilename(info.Name()),
//					}
//				}
//				//linux backup
//				if bc.linuxRegexFB2.MatchString(info.Name()) {
//					pp = profileFile{
//						Filid:    bc.getFilidByPrefix(info.Name()[:4]),
//						Prefix:   info.Name()[:4],
//						Name:     info.Name(),
//						datemake: mark,
//						datetime: datetime.UnixNano(),
//						Sizer:    info.Size(),
//						Types:    "linux",
//						hastMark: bc.hashFromFilename(info.Name()),
//					}
//				}
//				if bc.c.Debug {
//					fmt.Printf("[MakeListFilesFromDirectory] pp = `%v`\n", pp)
//				}
//
//				//сохраняю результат
//				if pp.Filid != -1 && bc.findDublicateHash(pp.hastMark) == false {
//					bc.Stocker[pp.Filid].FilesAll = append(bc.Stocker[pp.Filid].FilesAll, &pp)
//				} else {
//					//какая то лажа с файлом или корректностью выборки из filials (глянуть sql условия выборки)
//					bc.Log.Printf("[WARNING] файл имеет filid = -1  `%v`\n", pp)
//				}
//
//			}
//
//			// [2] = regex
//			//конвертация временной метки
//			if bc.winRegex.MatchString(info.Name()) || bc.linuxRegex.MatchString(info.Name()) {
//				mark := info.Name()[9:17]
//				conv := strings.Join([]string{mark[:4], mark[4:6], mark[6:]}, "-")
//				tt, err := time.Parse(layout, conv)
//				if err != nil {
//					log.Println(err)
//				} else {
//					datetime = tt
//				}
//				//window backup
//				if bc.winRegex.MatchString(info.Name()) {
//					pp = profileFile{
//						Filid:    bc.getFilidByPrefix(info.Name()[:4]),
//						Prefix:   info.Name()[:4],
//						Name:     info.Name(),
//						datemake: info.Name()[9:17],
//						datetime: datetime.UnixNano(),
//						Sizer:    info.Size(),
//						Types:    "windows",
//						hastMark: bc.hashFromFilename(info.Name()),
//					}
//				}
//				//linux backup
//				if bc.linuxRegex.MatchString(info.Name()) {
//					pp = profileFile{
//						Filid:    bc.getFilidByPrefix(info.Name()[:4]),
//						Prefix:   info.Name()[:4],
//						Name:     info.Name(),
//						datemake: info.Name()[9:17],
//						datetime: datetime.UnixNano(),
//						Sizer:    info.Size(),
//						Types:    "linux",
//						hastMark: bc.hashFromFilename(info.Name()),
//					}
//				}
//				if bc.c.Debug {
//					fmt.Printf("[MakeListFilesFromDirectory] pp = `%v`\n", pp)
//				}
//
//				//сохраняю результат
//				if pp.Filid != -1 && bc.findDublicateHash(pp.hastMark) == false {
//					bc.Stocker[pp.Filid].FilesAll = append(bc.Stocker[pp.Filid].FilesAll, &pp)
//				} else {
//					//какая то лажа с файлом или корректностью выборки из filials (глянуть sql условия выборки)
//					bc.Log.Printf("[WARNING] файл имеет filid = -1  `%v`\n", pp)
//				}
//
//			}
//		}
//
//		return nil
//	})
//}

//----------------------------------------------------------------------
// [2] анализатор
// профиль приоритет перед анализатором
//-----------------------------------------------------------------------
func (bc *BackupChecker) AnalizatorProfilesWithDir() {
	for filid, x := range bc.Stocker {
		bc.Log.Printf("[%d] %v %v\n", filid, len(x.FilesAll), x.FilesAll)
	}

	for filid, x := range bc.Stocker {

		//один файл - бэкап
		if len(x.FilesAll) == 1 {
			//выставляю флаг одиночног файла
			x.SingleFile = true
			if bc.c.Debug {
				bc.Log.Printf("Найден список с одним файлом: %v : %v\n", x.Prefix, x)
			}
			//на следующую итерацию
			continue
		}

		// если файлов = 0
		if len(x.FilesAll) == 0 {
			//выставляю флаг отсутствия файлов
			x.NoFiles = true
			x.Period = -1
			x.ErrorPeriod = "[ВНИМАНИЕ] по данному филиалу отсутствуют бэкапы совсем!"
			if bc.c.Debug {
				bc.Log.Printf("Найден список без файлов: %v : %v\n", x.Prefix, x)
			}
			continue
		}

		//корректные случаи, количество файлов > 1
		var tmp []int64
		//обнуляю старые значения, если были
		x.NoFiles = false
		x.ErrorPeriod = ""

		for _, a := range x.FilesAll {
			bc.Log.Printf("[%d] File: %v\n ", filid, a)
			tmp = append(tmp, a.datetime)
		}
		if bc.c.Debug {
			bc.Log.Printf("до сортировки: %v %v\n", filid, tmp)
		}

		// сортирую по убыванию
		sort.Slice(x.FilesAll, func(i, j int) bool {
			return x.FilesAll[i].datetime > x.FilesAll[j].datetime
		})

		fmt.Printf("XXX---------- PeriodReal: [%d] [%d] [%v] \n", bc.MedianaSlice(x.FilesAll), x.Filid, len(x.FilesAll))

		//проверка на наличие периода отличного от нуля
		//если поставить 0 то при проверке запишется среднее значение
		fmt.Printf("STATUS: [%v] [%v] \n", x.Filid, x.Period)
		if x.Period > 0 {
			fmt.Printf("STATUS: x.Period > 0  [%v] [%v] \n", x.Filid, x.Period)
			continue
		} else {
			fmt.Printf("STATUS: x.period <= 0 [%v] [%v] \n", x.Filid, x.Period)
			//получаю среднее в часах
			x.Period = int(time.Unix(0, x.FilesAll[0].datetime).Sub(time.Unix(0, x.FilesAll[len(x.FilesAll)-1].datetime)).Hours() / float64(len(x.FilesAll)))

		}

		////сортировка
		//sort.Slice(tmp, func(i, j int) bool { return tmp[i] > tmp[j] })
		//if bc.c.Debug {
		//	bc.Log.Printf("после сортировки: %v %v\n", filid, tmp)
		//}
		////обратная конвертация
		//sortTimes := []time.Time{}
		//for _, j := range tmp {
		//	if bc.c.Debug {
		//		bc.Log.Printf("[AnalizatorProfilesWithDir] %v\n", time.Unix(0, j))
		//	}
		//	sortTimes = append(sortTimes, time.Unix(0, j))
		//}

		////эксперимент с подсчетом среднего арфиметического по другому приниципу
		////формирую список разниц часовой между файлами
		//res := []int{}
		//f := sortTimes[0]
		//for i, b := range sortTimes[1:] {
		//	res = append(res, int(f.Sub(b).Hours()))
		//	if len(sortTimes[i:]) >= 1 {
		//		f = b
		//	}
		//}
		//if bc.c.Debug {
		//	fmt.Printf("[%s] список для медианы: %v\n", filid, res)
		//}
		////высчитываю среднее арифметическое
		//summ := 0
		//for _, j := range res {
		//	summ += j
		//}
		//mediana := summ / len(res)

		////обновляю все файлы по префиксу в списке UPD: не нужно, обновление переменных коснется только филиала, исключая файлы
		//x.Period = mediana

		//for _, t := range x.FilesAll {
		//	//t.Period = int(tmpResPeriod)
		//	x.Period = mediana
		//	t.Period = mediana
		//	//добавляю коррекцию
		//	t.Period += bc.c.Correction
		//	if bc.c.Debug {
		//		fmt.Printf("[AnalizatorProfilesWithDir] [%s] %v\n", filid, t.Period)
		//	}
	}
}

//for prefix, x := range bc.listTotal {
//	//если 1 элемент =  ошибка
//	if len(x) == 1 {
//		if bc.c.Debug {
//			fmt.Printf("Найден список с одним файлом: %v : %v\n", prefix, x)
//		}
//		//ищу в профилях из конфига
//		var flag = false
//		for _, g := range bc.c.Databases {
//			if g.Prefix == prefix {
//				flag = true
//				for _, t := range x {
//					t.Period = g.Period
//					t.IP = g.IP
//					t.FromProfile = true
//				}
//			}
//		}
//		if flag == false {
//			bc.writeLog(fmt.Sprintf("[AnalizatorProfilesWithDir] ситуация по префиксу %s, количество файлов = 1, в профилях из конфига ничего нет по этому префиксу\n", prefix))
//			//x[0].ErrorPeriod = fmt.Suprintf("[AnalizatorProfilesWithDir] ситуация по префиксу %s, количество файлов = 1, в профилях из конфига ничего нет по этому префиксу\n", prefix)
//		}
//		////добавляю единственное значение в общий итоговый список
//		//bc.StockFiles[prefix] = x[0]
//		//продолжим
//		continue
//	}
//	//корректные случаи, количество файлов > 1
//	var tmp []int64
//	for _, x := range x {
//		tmp = append(tmp, x.datetime.UnixNano())
//	}
//	if bc.c.Debug {
//		fmt.Printf("до сортировки: %v %v\n", prefix, tmp)
//	}
//	//сортировка
//	sort.Slice(tmp, func(i, j int) bool { return tmp[i] > tmp[j] })
//	if bc.c.Debug {
//		fmt.Printf("после сортировки: %v %v\n", prefix, tmp)
//	}
//	//обратная конвертация
//	sortTimes := []time.Time{}
//	for _, x := range tmp {
//		if bc.c.Debug {
//			fmt.Printf("[AnalizatorProfilesWithDir] %v\n", time.Unix(0, x))
//		}
//		sortTimes = append(sortTimes, time.Unix(0, x))
//	}
//
//	//эксперимент с подсчетом среднего арфиметического по другому приниципу
//	//формирую список разниц часовой между файлами
//	res := []int{}
//	f := sortTimes[0]
//	for i, x := range sortTimes[1:] {
//		res = append(res, int(f.Sub(x).Hours()))
//		if len(sortTimes[i:]) >= 1 {
//			f = x
//		}
//	}
//	if bc.c.Debug {
//		fmt.Printf("[%s] список для медианы: %v\n", prefix, res)
//	}
//	//высчитываю среднее арифметическое
//	summ := 0
//	for _, x := range res {
//		summ += x
//	}
//	mediana := summ / len(res)
//
//	//обновляю все файлы по префиксу в списке
//	for _, t := range x {
//		//t.Period = int(tmpResPeriod)
//		t.Period = mediana
//		//добавляю коррекцию
//		t.Period += bc.c.Correction
//		if bc.c.Debug {
//			fmt.Printf("[AnalizatorProfilesWithDir] [%s] %v\n", prefix, t.Period)
//		}
//	}
//
//	//ищу в профилях из конфига, если есть заполняю
//	for _, g := range bc.c.Databases {
//		if g.Prefix == prefix {
//			for _, t := range x {
//				t.PeriodProfile = g.Period
//				t.IP = g.IP
//				t.FromProfile = true
//			}
//		}
//	}
//}
//}

//----------------------------------------------------------------------
// [3] отбор наипозднейших файлов по времени создания по метке
//-----------------------------------------------------------------------
func (b *BackupChecker) SortEndFiles() {
	for _, j := range b.Stocker {
		//без файлов
		if len(j.FilesAll) == 0 {
			continue
		}
		//одиночный файл
		if len(j.FilesAll) == 1 {
			j.Period = 0
			j.ErrorPeriod = fmt.Sprintf("ситуация по префиксу %s, количество файлов = 1\n", j.FilesAll[0].Prefix)
		}
		j.FileLast = j.FilesAll[0]

		//файлов > 1 , берем верхний из слайса, т.к. слайс отсортирован по убыванию времени создания
		if len(j.FilesAll) >= 2 {
			j.FileLast = j.FilesAll[0]
		}

	}
}

//----------------------------------------------------------------------
// [4] проверка конечного списка файлов на протухлость
//-----------------------------------------------------------------------
func (b *BackupChecker) checkExpiredEndList() {
	for _, f := range b.Stocker {
		if f.FileLast != nil {
			if b.checkFileExpired(*f.FileLast, f.Period) {
				//expired
				f.TimeExpired = true
				f.TimeLastChecked = time.Now().Format(layout_timechecked)
			} else {
				//ok
				f.TimeExpired = false
				f.TimeLastChecked = time.Now().Format(layout_timechecked)
			}
		}
	}

	//for p, f := range b.Stocker {
	//	if f.FileLast != nil {
	//		fTime := time.Unix(0, f.FileLast.datetime)
	//
	//		//получаю текущую дату
	//		tt := time.Now()
	//		tnow := time.Date(tt.Year(), tt.Month(), tt.Day(), 0, 0, 0, 0, time.UTC)
	//
	//		//добавляю период в часах ко времени создания файла
	//		fTime = fTime.Add(time.Hour * time.Duration(f.Period))
	//
	//		//отладка, если надо
	//		//if b.c.Debug {
	//		//	b.Log.Printf("[debug][%v] %v %v %v\n", f.FileLast.Name, tnow.Format(layout), tnow.Unix() > fTime.Unix(), fTime.Format(layout))
	//		//}
	//		tmpFtime := time.Date(fTime.Year(), fTime.Month(), fTime.Day(), 0, 0, 0, 0, time.UTC)
	//
	//		fmt.Printf("{CHECK-%v} %v   %v [ %v ]\n", f.Prefix, tnow.String(), tmpFtime.String(), fTime.String())
	//		f.CheckMsg = fmt.Sprintf("{CHECK-%v} %v   %v [ %v ]\n", f.Prefix, tnow.Format(layout_timechecked), tmpFtime.Format(layout_timechecked), fTime.Format(layout_timechecked))
	//
	//		fmt.Printf("Now: %v    tmpFTime: %v\n", tnow.String(), tmpFtime.String())
	//
	//		//сравниваю
	//		if tnow.Unix() > tmpFtime.Unix() {
	//			//ошибка, период обновления бэкапа истек а нового файла нет, пишу в лог + файл репорт проверки
	//			f.TimeExpired = true
	//			f.TimeLastChecked = time.Now().Format(layout_timechecked)
	//			b.writeLog(fmt.Sprintf("[%s]  %v  %v", p, "бэкап протух", f.Period))
	//			//}
	//		} else {
	//			f.TimeLastChecked = time.Now().Format(layout_timechecked)
	//		}
	//		fmt.Printf("--------- RESULT CHECKTIME:    Filid: %d PEriod: %d\nTnow: %s\ntmpFtime: %s\nfileTime+period: %s\n",
	//			f.Filid, f.Period, tnow.String(),tmpFtime.String(), fTime.String())
	//	}
	//}

	//for _, x := range b.Stocker {
	//	fmt.Printf("LASTFILE: [%d] %v\n", x.Filid, x.FileLast)
	//}

}

//враппер для записи в лог
func (bc *BackupChecker) writeLog(msg string) {
	_, _ = bc.logfile.WriteString(
		fmt.Sprintf("%s [%v] %s\n", logprefix, time.Now().Format("2006-01-02 15:04"), msg))
	bc.Log.Print(msg)
}

//общий цикл-этапов проверки
//func (b *BackupChecker) Runner() {
//	//[1]
//	b.MakeListFilesFromDirectory()
//	//[2]
//	b.AnalizatorProfilesWithDir()
//	//[3]
//	b.SortEndFiles()
//	//[4]
//	b.checkExpiredEndList()
//
//	b.ShowFilidAndPeriod()
//
//	b.writeLog(fmt.Sprintf("проверка бэкапов завершена `%v`\n", time.Now().String()))
//}

//----------------------------------------------------------------------
// реализация проверки по таймеру :: общий цикл по заданному периоду
// chanCommand : канал для передачи данных в обработчик
//-----------------------------------------------------------------------
func (b *BackupChecker) TimerChecker(chanCommand chan string) {
	b.writeLog("[TimerChecker] стартует...\n")
	defer func() {
		b.writeLog("[TimerChecker] окончание работы...\n")
	}()
	var period time.Duration
	if b.c.Production {
		period = time.Hour * time.Duration(b.c.CheckPeriod)
	} else {
		period = time.Second * time.Duration(b.c.CheckPeriod)
	}
	timer := time.NewTicker(period)
	for {
		select {
		case <-timer.C:
			b.Runner()
		case comm := <-chanCommand:
			b.writeLog(comm)
		}
	}
}

//----------------------------------------------------------------------
// gob functions
//-----------------------------------------------------------------------
func (bc *BackupChecker) SaveToGob(filename string, data interface{}) error {
	//провожу перенос активных данных из Stocker в Dump перед сохранением
	for _, x := range bc.Stocker {
		for _, l := range bc.StockFilialDump {
			if x.Filid == l.Filid {
				l.Period = x.Period
			}
		}
	}

	var f *os.File
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		ff, errf := os.OpenFile(filename, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, os.ModePerm)
		if errf != nil {
			bc.Log.Printf("[FATALERROR][savetogob] ошибка сохранения дампа файла конфига `%v`\n", err)
			return err
		}
		f = ff
	} else {
		fu, errf := os.OpenFile(filename, os.O_TRUNC|os.O_WRONLY, os.ModePerm)
		if errf != nil {
			bc.Log.Printf("[FATALERROR][savetogob] ошибка сохранения дампа файла конфига `%v`\n", errf)
			return errf
		}
		defer fu.Close()
		f = fu
	}
	d := gob.NewEncoder(f)
	if err := d.Encode(data); err != nil {
		bc.Log.Printf("[SaveToGob] ошибка encode причина : `%v`\n", err)
		return err
	}
	return nil
}
func (bc *BackupChecker) LoadFileGob(filename string) error {
	var data = make(map[int]*Filial)
	if f, err := os.Open(filename); err != nil {
		bc.Log.Printf("Error: %v\n", err)
		return err
	} else {
		defer f.Close()
		d := gob.NewDecoder(f)
		if err = d.Decode(&data); err != nil {
			bc.Log.Printf("Error: %v\n", err)
			return err
		}
	}
	bc.Stocker = data
	return nil
}

//функция по загрузке/выгрузке дампа конфига
func (b *BackupChecker) ConfigDinamicLoader(filename string) error {
	//первый старт сервера, проверяю по пути наличие дампа, если есть загружаю, если нет создаю и дамплю пустой конфиг
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		//файла нет создаю и делаю первый дамп
		//почему выставлять os.O_WRONLY - смотрим код ядра linux
		///usr/local/go/src/syscall/zerrors_linux_amd64.go:660
		//O_RDONLY                         = 0x0
		//O_RDWR                           = 0x2
		//O_WRONLY                         = 0x1
		//флаги разные
		var fu *os.File = nil
		if fh, errf := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, os.ModePerm); errf != nil {
			b.Log.Printf("[FATALERROR][OPENFILE] ошибка создания дампа файла конфига `%v` :: `%v` :: `%v`\n", errf, filename, b.c.BackupConfigPathFile)
			if fh, errf1 := os.Create(filename); errf1 != nil {
				b.Log.Printf("[FATALERROR][CREATE] ошибка создания дампа файла конфига `%v` :: `%v` :: `%v`\n", errf, filename, b.c.BackupConfigPathFile)
				return errf
			} else {
				fu = fh
			}
		} else {
			fu = fh
		}
		b.Log.Printf("успешно создан  дамп конфига `%s`\n", filename)
		defer fu.Close()
		d := gob.NewEncoder(fu)
		if err := d.Encode(&b.StockFilialDump); err != nil {
			b.Log.Printf("[ERROR DUMP->FILE]: %v\n", err)
			return err
		}
		//возврат из функции
		return nil

	} else {
		//дамп есть, загружаю его
		fh, errf := os.OpenFile(filename, os.O_APPEND|os.O_RDONLY, os.ModePerm)
		if errf != nil {
			b.Log.Printf("[FATALERROR] ошибка открытия дампа конфига, причина -  `%v`\n", errf)
			return errf
		}
		defer fh.Close()
		d := gob.NewDecoder(fh)
		m := map[int]*FilialDump{}
		if err = d.Decode(&m); err != nil {
			b.Log.Printf("Error: %v\n", err)
			return err
		}
		b.StockFilialDump = m
		b.Log.Printf("[ConfigDinamicLoader][SUCCESS] успешная загрузка дампа конфига!\n")

		//делаю обновление данных по периодам
		//беру значения полученные из дампа ис
		//перезаписываю в текущем конфиге
		if len(b.StockFilialDump) > 0 {
			for _, x := range b.StockFilialDump {
				for _, j := range b.Stocker {
					if j.Filid == x.Filid {
						j.Period = x.Period
					}
				}
			}
		}
	}
	return nil
}

//----------------------------------------------------------------------
// утилиты
//-----------------------------------------------------------------------
// генератор префикса
func (bc *BackupChecker) getPrefixbyFilid(filid int) (prefix string) {
	if filid < 100 {
		prefix = fmt.Sprintf("t0%d", filid)
	} else {
		prefix = fmt.Sprintf("t%d", filid)
	}
	return
}

func (bc *BackupChecker) getFilidByPrefix(prefix string) (filid int) {

	for k, v := range bc.Stocker {
		if v.Prefix == prefix {
			return k
		}
	}
	return -1
}
func (b *BackupChecker) ShowFilials(str map[int]*Filial) {
	fmt.Printf("STOCKER------- %v\n", b.Stocker)
	for x, a := range str {
		fmt.Printf("[ShowFilials] ----- [%d] [%d]  %v : %v\n", x, a.Period, a.Filid, a)
		if len(a.FilesAll) > 0 {
			for _, g := range a.FilesAll {
				fmt.Printf("File---- [%v] [%v] %v %v\n", g.Prefix, g.Filid, g.datemake, g.Name)
			}
		}
	}
}

//функция поиска дубликатов в списке файлов по филиалу

func (b *BackupChecker) findDublicateHash(hashMark string) bool {
	for _, x := range b.Stocker {
		for _, a := range x.FilesAll {
			if a.hastMark == hashMark {
				return true
			}
		}
	}
	return false
}

//----------------------------------------------------------------------
// беру список времени файлов
// сортирую
// высчитываю среднее арифметическое
// получаю результат
//-----------------------------------------------------------------------
func (b *BackupChecker) medianaTimeSlice(s []time.Time) float64 {
	// сортирую по убыванию
	sort.Slice(s, func(i, j int) bool {
		return s[i].After(s[j])
	})
	//усредненное среднее арифметическое
	//fmt.Printf("%v %v %v\n",s[0].Sub(s[len(s)-1]).Hours(), s[0].String(), s[len(s)-1].String())

	return s[0].Sub(s[len(s)-1]).Hours() / float64(len(s))
}

//генератор хэша из имени файла
func (b *BackupChecker) hashFromFilename(name string) string {
	h := sha1.New()
	h.Write([]byte(name))
	return hex.EncodeToString(h.Sum(nil))
}

//генератор рандомных заданной длины хэшей
func (b *BackupChecker) generateHashMark(l int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	rand.Seed(time.Now().UnixNano())
	res := make([]rune, l)
	for x := 0; x < l; x++ {
		res = append(res, letters[rand.Intn(len(letters))])
	}
	return string(res)
}

//----------------------------------------------------------------------
// конвертер и обновитель пакета от вебморды для изменения периодов проверки бд
//-----------------------------------------------------------------------
func (b *BackupChecker) UpdateConvertAndUpdater(fu []UpdateResult) {
	//изменяю сразу текущий конфиг Stocker
	for _, x := range fu {
		res, err := strconv.Atoi(strings.Split(x.Filid, "filid")[1])
		if err != nil {
			b.Log.Printf("[errror] %v\n", err)
		} else {
			newp, err := strconv.Atoi(x.NewPeriod)
			if err != nil {
				b.Log.Printf("[errror] %v\n", err)
			} else {
				b.Lock()
				b.Stocker[res].Period = newp
				b.Unlock()
			}
		}
	}
	b.ShowFilidAndPeriod()
	b.SaveToGob(b.c.BackupConfigPathFile, b.Stocker)
}
func (b *BackupChecker) ShowFilidAndPeriod() {
	for _, x := range b.Stocker {
		fmt.Printf("--- [%d]  [%d] \n", x.Filid, x.Period)
	}
}

//----------------------------------------------------------------------
// функция для тестирования среднего значения по слайсу
//-----------------------------------------------------------------------
func (b *BackupChecker) MedianaSlice(x []*profileFile) int {
	res := int(time.Unix(0, x[0].datetime).Sub(time.Unix(0, x[len(x)-1].datetime)).Hours() / float64(len(x)))
	return int(res)
}

//----------------------------------------------------------------------
// функция для проверки протухлости файла
//-----------------------------------------------------------------------
func (b *BackupChecker) checkFileExpired(f profileFile, period int) bool {
	fTime := time.Unix(0, f.datetime)
	fmt.Printf("FileTime: %v\n", fTime.String())
	//получаю текущую дату
	tt := time.Now()
	tnow := time.Date(tt.Year(), tt.Month(), tt.Day(), 0, 0, 0, 0, time.UTC)
	fmt.Printf("TimeNow: %v\n", tnow.String())
	//добавляю период в часах ко времени создания файла
	fTime = fTime.Add(time.Hour * time.Duration(period))
	fmt.Printf("Ftime+period: %v\n", fTime.String())
	tmpFtime := time.Date(fTime.Year(), fTime.Month(), fTime.Day(), 0, 0, 0, 0, time.UTC)
	fmt.Printf("Ftime+period/ZERO: %v\n", tmpFtime.String())
	if tnow.Unix() > tmpFtime.Unix() {
		fmt.Printf("EXPIRED \n")
		return true
	} else {
		fmt.Printf("OK\n")
		return false
	}

}

//----------------------------------------------------------------------
// проверка филиалов, где нет файлов
//-----------------------------------------------------------------------
func (b *BackupChecker) CheckerNoFiles() {
	for _, b := range b.Stocker {
		if len(b.FilesAll) == 0 {
			b.NoFiles = true
			b.Period = -1
			b.ErrorPeriod = "[ВНИМАНИЕ] по данному филиалу отсутствуют бэкапы совсем!"
		}
	}
}

func (b *BackupChecker) correctFilial(n int) (res string) {
	if n < 10 {
		res = fmt.Sprintf("00%v", n)
		return
	}
	if n < 100 {
		res = fmt.Sprintf("0%v", n)
		return
	}
	if n >= 100 && n < 1000 {
		res = fmt.Sprintf("%v", n)
		return
	}
	return
}

//----------------------------------------------------------------------
// debug functional
//-----------------------------------------------------------------------
func (b *BackupChecker) DebuggerFindFiles() {
	b.Step1FindFiles()
	//суммирую имеющиеся данные по общему стоку
	stat := make(map[int]int) //prefix: count files
	for k, v := range b.Stocker {
		stat[k] = len(v.FilesAll)
	}
	//вывод результата
	b.Log.Printf("Дебаггер\n---------------------\n%3v : %d\n%3v : %d\n", "файлов", b.DebCountFiles,
		"филиалов", b.DebCountDirs)
	for k, v := range stat {
		b.Log.Printf("[%v] %v\n", k, v)
	}
	b.Log.Printf("количество :%v\n", len(stat))

	for fil, name := range b.DebDirFoundMap {
		fmt.Printf("%3d %10s\n", fil, name)

	}
	for i, name := range b.DebDirFoundList {
		fmt.Printf("dir found -> %3d.  %10s\n", i, name)
	}
	for k, v := range b.Stocker {
		fmt.Printf("stocker=> %3d  %10s\n", k, v.Prefix)
	}

}

-- структура таблицы на 161 базе
create table FILIALSTATE
(
  FILIAL int not null,
  STATE int,
  COMMENT varchar(1024),
  constraint PK_FILIALSTATE primary key (FILIAL)
);

-- структура таблицы filial
create table FILIALS

(

  FILID int,

  SHORTNAME varchar,

  FULLNAME varchar,

  JID int,

  FCOLOR int,

  DBALIAS varchar,

  PRICEJID int,

  ISMAIN int,

  DBPATH varchar,

  DBIPADDR varchar,

  DBPORT int default 3050,

  CALLCENTRE int,

  DBPATH2 varchar,

  CONNECTTIMEOUT int,

  ARCHIVEBASE varchar,

  MEDIABASE varchar,

  FILIAL_GROUP_IGNORE int,

  DIS_IN_SCHEDULE int,

  DEFIMAGEPATH varchar,

  LPUCODE varchar,

  SPECLPUCODE int,

  TID int,

  TRMASK varchar,

  MAINPACSPID int,

  ER_PLACEID varchar,

  ER_AUTHTOKEN varchar,

  DIAGNOSTICFILIAL int,

  VIEWINWEB int,

  SHOWCASHREF int,

  ADDRID int,

  GRPID int,

  EXTLABCODE varchar,

  SITEGRPID int,

  MATAPPFILIALTYPE int ,

  MATAPPFILIAL int,

  ICONID int,

  INSTRUCTIONS varchar,

  INSTRUCTTITLE varchar,

  INTRUCTIONIPADDR varchar,

  SCHED_FILIAL_GROUP_ID int,

  WEBNAME varchar,

  BOTTOMGEN int,

  TOPGEN int,

  WSPATH varchar,

  MEDIAID varchar,

  LABSECRETKEY varchar,

  ONLINETYPE int,

  TIMEZONE int,

  VIEWPRICEINWEB int,

  WEBPRICENAME varchar,

  PACSCODE varchar,

  JNAME2013 varchar,

  constraint PK_FILIALS primary key (FILID)

);


